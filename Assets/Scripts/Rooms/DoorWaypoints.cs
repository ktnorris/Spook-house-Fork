﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorWaypoints : MonoBehaviour {

    RoomBase2 ourRoom;
    public GameObject connectedDoor;

	// Use this for initialization
	void Start () {
        GetComponent<MeshRenderer>().enabled = false;
        ourRoom = GetComponentInParent<RoomBase2>();
        this.GetComponent<Collider>().enabled = false;
        this.GetComponent<Collider>().enabled = true;

	}

    /// <summary>
    /// Directs guests and adds exits to the room.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        
       if(other.tag == "guest")
        {
            other.GetComponent<BaseGuest>().GoalChange(ourRoom.QueryForExit(this.gameObject.transform.parent.gameObject, other.gameObject));
            if (!ourRoom.Exits.Contains(this.transform.parent.gameObject))
            {
                ourRoom.guestsInRoom.Add(other.gameObject);
            }
        }

       if(other.tag == "door")
        {
            connectedDoor = other.gameObject;
        }
    }


    

    /// <summary>
    /// Gets rid of the room as a exit
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit(Collider other)
    {
       if (other.tag == "guest")
        {
            if (ourRoom.Exits.Contains(this.transform.parent.gameObject))
            {
                ourRoom.guestsInRoom.Remove(other.gameObject);
            }
        }
    }


    public void ResetAdjDoors()
    {
        if(connectedDoor != null)
        {
            connectedDoor.GetComponentInParent<RoomBase2>().DisconnectAnExit(connectedDoor);
            connectedDoor.transform.GetChild(0).gameObject.SetActive(true);
        }
    }
}
