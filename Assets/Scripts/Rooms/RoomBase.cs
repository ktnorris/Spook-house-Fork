﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomBase : MonoBehaviour {

    public float doorCount;
    Dictionary<GameObject, Vector3> ConnectedRooms;

    private void Start()
    {
        Init();
    }

    /// <summary>
    /// Initializes some values and stuff
    /// </summary>
    void Init()
    {
        ConnectedRooms = new Dictionary<GameObject, Vector3>();
    }
    

    /// <summary>
    /// The function that is actually called from below, it supplies a vector 3 for guests to navigate too.
    /// </summary>
    /// <param name="startPoint">Which object is triggering this change</param>
    /// <returns>The next position for a guest to track too</returns>
    public Vector3 QueryForNextWaypoint(GameObject startPoint, GameObject guest)
    {
        if (!ConnectedRooms.ContainsKey(startPoint))
        {
            
                int[] emeter = guest.GetComponent<EntertainmentMeter>().MazeExit();
                guest.GetComponent<BaseGuest>().ReportScore(emeter[1], emeter[0]);
                guest.GetComponent<EntertainmentMeter>().enabled = false;
            
            return GameObject.FindGameObjectWithTag("Atlas").GetComponent<GameTimeKeeping>().KillPosition.transform.position;
        }else
        {
            //Debug.Log("Getting next postion");
            if (!ConnectedRooms.ContainsValue(guest.GetComponent<BaseGuest>().destination) && guest.GetComponent<BaseGuest>().destination == GameObject.FindGameObjectWithTag("Atlas").GetComponent<GameTimeKeeping>().KillPosition.transform.position)
            {
                //Debug.Log("fit for changing");
                List<GameObject> randyList = BuildDictionaryArray(startPoint);

                int randy = Random.Range(0, randyList.Count);
                if (randyList.Count > 0)
                {
                    //Debug.Log(randyList[randy]);
                    //Debug.Log(ConnectedRooms[randyList[randy]]);
                    return ConnectedRooms[randyList[randy]];
                }else
                {
                    return GameObject.FindGameObjectWithTag("Atlas").GetComponent<GameTimeKeeping>().KillPosition.transform.position;
                }
            }

            return guest.GetComponent<BaseGuest>().destination;
        }
    }

    /// <summary>
    /// Adds an exit point for the room to direct the guest too
    /// </summary>
    /// <param name="door"></param>
    /// <param name="roomTrans"></param>
    public void AddPotentialExit(GameObject door, Vector3 roomTrans)
    {
        ConnectedRooms.Add(door, roomTrans);
    }

    /// <summary>
    /// Removes an exit from the list
    /// </summary>
    /// <param name="key">the gameObjet we're removing</param>
    public void RemovePotentialExit(GameObject key)
    {
        ConnectedRooms.Remove(key);
    }

    /// <summary>
    /// If an exit even exists for the given door
    /// </summary>
    /// <param name="key">The door we're dealing with</param>
    /// <returns></returns>
    public bool hasExit(GameObject key)
    {
        return ConnectedRooms.ContainsKey(key);
    }

    public Vector3 getTransform(GameObject key)
    {
        return ConnectedRooms[key];
    }

    /// <summary>
    /// To be run Whenever we need something iterable or random.
    /// </summary>
    /// <param name="omit">any gamobject to omit from the list</param>
    /// <returns></returns>
    private List<GameObject> BuildDictionaryArray(GameObject omit)
    {
        List<GameObject> returnArray = new List<GameObject>();
        foreach(KeyValuePair<GameObject, Vector3> roomPair in ConnectedRooms)
        {
            returnArray.Add(roomPair.Key);
        }


        returnArray.Remove(omit);

        return returnArray;
    }
}
