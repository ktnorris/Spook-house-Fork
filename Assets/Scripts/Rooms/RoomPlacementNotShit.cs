﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RoomPlacementNotShit : AttractionPlaceV2 {

    public bool isConnected;
    public int doorCount;
    public List<GameObject> connectedDoors;

    //Navmesh obstacles
    NavMeshObstacle[] obstacles;

    //Guests in the collider at the time of placement
    List<GameObject> killList;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        killList = new List<GameObject>();
        obstacles = GetComponentsInChildren<NavMeshObstacle>();
        foreach (NavMeshObstacle obs in obstacles)
        {
            obs.enabled = false;
        }

        doorCount = GetComponentsInChildren<DoorContact>().Length;
    }

    // Update is called once per frame
    override protected void Update () {
        base.Update();
	}


    protected override bool CanPlace()
    {
        
        if(connectedDoors.Count > 0)
        {
            isConnected = true;
        }else
        {
            isConnected = false;
        }

        if (connectedDoors.Count >= doorCount)
        {
            isConnected = false;
        }
        base.collisionFlag = !isConnected;
        return base.CanPlace();
    }

    /// <summary>
    /// Drop the room sets stuff in the room script?
    /// </summary>
    void Drop()
    {
        foreach (GameObject toKill in killList)
        {
            if (toKill != null)
            {
                toKill.GetComponent<BaseGuest>().KillForNoGoodReason();
            }
        }

        

        RoomBase2 room = gameObject.AddComponent<RoomBase2>();
        List<GameObject> exits = new List<GameObject>();

        DoorContact[] doors = GetComponentsInChildren<DoorContact>();
        for (int x = 0; x < doors.Length; x++)
        {
            //Debug.Log(!connectedDoors.Contains(doors[x].gameObject) + doors[x].gameObject.name);
            if (!connectedDoors.Contains(doors[x].gameObject.transform.parent.gameObject))
            {
                exits.Add(doors[x].gameObject.transform.parent.gameObject);
            }
            doors[x].gameObject.AddComponent<DoorWaypoints>();
            doors[x].disableExit();
            Destroy(doors[x]);
        }

        room.Init(exits);
        foreach (NavMeshObstacle obs in obstacles)
        {
            
            obs.enabled = true;
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "guest")
        {
            Debug.Log("Adding to kill list");
            killList.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "guest")
        {
            Debug.Log("Removing from kill list");
            killList.Remove(other.gameObject);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Room")
        {
            base.collisionFlag = true;
        }
    }

    protected override void Lock()
    {
        Drop();
        base.Lock();
    }
}
