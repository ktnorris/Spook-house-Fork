﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RoomBase2 : MonoBehaviour {

    public List<GameObject> Exits;
    public List<Vector3> PlacePoints;
    public List<GameObject> connectedExits;

    public Vector3 offset;


    //--------------------------------------------------------

    public List<GameObject> guestsInRoom;
    public List<GameObject> towersInRoom;

    Color[] colorsOrig;
    /// <summary>
    /// Initializes the lists
    /// </summary>
    public void Init (List<GameObject> e) {
        Exits = new List<GameObject>();
        PlacePoints = new List<Vector3>();
        connectedExits = new List<GameObject>();

        //----------------------------------
        //sell stuff init

        guestsInRoom = new List<GameObject>();
        towersInRoom = new List<GameObject>();

        foreach(GameObject exit in e)
        {
            AddToExitList(exit);
        }

        
        Renderer[] rendlist = GetComponentsInChildren<Renderer>();
        colorsOrig = new Color[rendlist.Length];
        for (int x = 0; x < rendlist.Length; x++)
        {
            colorsOrig[x] = rendlist[x].material.color;
        }
	}

    /// <summary>
    /// Adds the exit to the potential exits
    /// </summary>
    /// <param name="Exit">The position of the door</param>
    public void AddToExitList(GameObject Exit)
    {
        updatePlacePoints(Exit.transform.position);
        Exits.Add(Exit);
    }

    public void ConnectAnExit(GameObject exit)
    {
        RemoveFromExitList(exit);
        connectedExits.Add(exit);
    }

    public void DisconnectAnExit(GameObject exit)
    {
        connectedExits.Remove(exit);
        AddToExitList(exit);
    }

    /// <summary>
    /// Updates the 
    /// </summary>
    /// <param name="doorPos"></param>
    private void updatePlacePoints(Vector3 doorPos)
    {
        Vector3 diff;
        diff = (doorPos - transform.position);
        Vector3 dir = new Vector3(1, 0, 1);
        dir.x *= offset.x;
        dir.z *= offset.y;
        diff.x *= dir.x;
        diff.z *= dir.z;
        PlacePoints.Add(diff);
    }

    public void RemoveFromExitList(GameObject doorPos)
    {
        removePlacementPoint(doorPos.transform.position);
        Exits.Remove(doorPos);
    }
    
    private void removePlacementPoint(Vector3 doorPos)
    {
        Vector3 diff;
        diff = new Vector3((doorPos.x - transform.position.x) * offset.x, 0, (doorPos.z - transform.position.z) * offset.z);
        PlacePoints.Remove(diff);
    } 


    /// <summary>
    /// Returns the placement vector list so that rooms can know where they can be placed.
    /// </summary>
    /// <returns></returns>
    public List<Vector3> returnPlacementVectors()
    {
        return PlacePoints;
    }


    /// <summary>
    /// When a door collider detects a guest this will be called
    /// </summary>
    /// <param name="startPoint">Which gamobject is detecting the guest</param>
    /// <param name="guest">the guest in question</param>
    /// <returns>Returns the next point for the guest to walk to</returns>
    public Vector3 QueryForExit(GameObject startPoint, GameObject guest)
    {
        if (Exits.Contains(startPoint))
        {
            return hitExit(guest);
        }
        else
        {
            return hitEntrance();
        }
    }

    /// <summary>
    /// If the door was in the exits list that means it is time for the guest to leave! this function grabs the score from the entertainment meter
    /// and then passes it to reportScore(). Finally it sets a timer for the guest to disappear.
    /// </summary>
    /// <param name="guest">The guest that just hit the exit</param>
    /// <returns>Where it should go until it dies</returns>
    Vector3 hitExit(GameObject guest)
    {
        EntertainmentMeter emeter = guest.GetComponent<EntertainmentMeter>();
        BaseGuest gScript = guest.GetComponent<BaseGuest>();

        int[] scoreArray = emeter.MazeExit();

        gScript.ReportScore(scoreArray[1], scoreArray[0]);
        StartCoroutine(gScript.KillTimer(2));


        return GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().gTime.KillPosition.transform.position ;

    }

    /// <summary>
    /// When a guest hits an entrance or the first collider of a room, this function tells them to go to an available exit.
    /// But it priortizes connected exits! This makes it so that player aren't penalized for using rooms with many exit points.
    /// </summary>
    /// <returns></returns>
    Vector3 hitEntrance()
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        if (connectedExits.Count > 0)
        {
            int randy = Mathf.RoundToInt(Random.Range(0f, connectedExits.Count - 1));
            return connectedExits[ randy].transform.position;
        }
        else
        {
            int randy = Mathf.RoundToInt(Random.Range(0f, Exits.Count -1 ));
            return Exits[randy].transform.position;
        }
    }

    //------------------------------------------------------------------------------------------------------------
    //All this stuff only happens when we're selling a room!
    //------------------------------------------------------------------------------------------------------------


    private void OnMouseOver()
    {
        if (GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().SellState)
        {
            
            if (CanSell())
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    Debug.Log("Hovering over this thing to sell it");
                    SellRoom();
                }
            }
        }
    }

    bool Colored;

    private void Update()
    {
        if (GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().SellState)
        {
            if (!Colored)
            {
                CanSell();
                Colored = true;
            }
        }else if (!GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().SellState)
        {
            if (Colored)
            {
                resetColor();
                Colored = false;
            }
        }
    }


    bool CanSell()
    {
        Renderer[] rendlist = gameObject.GetComponentsInChildren<Renderer>();

        if(connectedExits.Count > 0)
        {
            foreach(Renderer rend in rendlist)
            {
                if (rend.gameObject.name.Substring(0, 4) != "Path")
                {
                    rend.material.color = Color.red;
                }
            }
            return false;
        }else
        {
            foreach(Renderer rend in rendlist)
            {
                if (rend.gameObject.name.Substring(0, 4) != "Path")
                {
                    rend.material.color = Color.green;
                }
            }
            return true;
        }
    }

    void resetColor()
    {
        Renderer[] rendlist = gameObject.GetComponentsInChildren<Renderer>();

        for(int x = 0; x < rendlist.Length; x++)
        {
            if (rendlist[x].gameObject.name.Substring(0,4) != "Path")
            {
                rendlist[x].material.color = Color.white;
            }
        }
    }


    void SellRoom()
    {
        float totalVal = 0;
        GameScoring gScoring = GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().gScoring;
        totalVal += GameObject.FindGameObjectWithTag("RoomPlaceCanvas").GetComponent<RoomPlacementPanel>().shuffleCost * .25f;

        foreach(GameObject guest in this.guestsInRoom)
        {
            if (guest != null)
            {
                int[] mazeExit = guest.GetComponent<EntertainmentMeter>().MazeExit();
                totalVal += mazeExit[1];
                gScoring.AddReputation(mazeExit[0]);
                guest.GetComponent<BaseGuest>().StopAllCoroutines();
                guest.GetComponent<BaseGuest>().KillForNoGoodReason();
            }
        }

        foreach(GameObject tower in towersInRoom)
        {
            for (int x = 0; x < tower.GetComponent<ActiveUpgrade>().upgradeCosts.Length; x++)
            {
                float[] tierTracker = tower.GetComponent<ActiveUpgrade>().tierTracker;
                if (tierTracker[0] + tierTracker[1] + tierTracker[2] >= x + 1)
                {
                    totalVal += tower.GetComponent<ActiveUpgrade>().upgradeCosts[x].costM * .25f;
                   
                }

                totalVal += (tower.GetComponent<AttractionBase>().BaseStats.towerCosts.costM * .25f);
            }

            Destroy(tower);
        }


        DoorWaypoints[] doors = gameObject.GetComponentsInChildren<DoorWaypoints>();
        foreach (DoorWaypoints door in doors)
        {
            door.ResetAdjDoors();
        }

        GameObject moneyviewer = Resources.Load("SpawnFolder/MoneyCanvas") as GameObject;
        GameObject moneyThingy = Instantiate(moneyviewer, transform.position, Quaternion.identity);
        ShowMoney money = moneyThingy.AddComponent<ShowMoney>();
        money.Start();
        

        totalVal = Mathf.Round(totalVal);
        money.moneyAmount = totalVal;
        StartCoroutine(money.TextTrigger(totalVal));
        gScoring.AddMoney(totalVal);
        Destroy(gameObject);


    }


}
