﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    Atlas atlas;

    public float movespeed;

    public float zoomSpeed;

    public GameObject Bounds;

    Vector3 currentPos;

	// Use this for initialization
	void Start () {
        atlas = GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>();
        currentPos = transform.position;
	}

    // Update is called once per frame
    void Update() {

        Vector3 nextPos = currentPos;

        if (Input.GetKey(atlas.inputSettings.cameraUp))
        {
            nextPos.z -= movespeed * Time.deltaTime;
            withinBounds(nextPos);
        }

        if (Input.GetKey(atlas.inputSettings.camerDown))
        {
            nextPos.z += movespeed * Time.deltaTime;
            withinBounds(nextPos);
        }

        if (Input.GetKey(atlas.inputSettings.cameraRight))
        {
            nextPos.x -= movespeed * Time.deltaTime;
            withinBounds(nextPos);
        }

        if (Input.GetKey(atlas.inputSettings.cameraLeft))
        {
            nextPos.x += movespeed * Time.deltaTime;
            withinBounds(nextPos);
        }
		
	}


    void withinBounds(Vector3 pos)
    {
        int x = 0;

        switch (x)
        {
            case 0:
                if(pos.x < Bounds.transform.position.x - Bounds.transform.localScale.x || pos.x > Bounds.transform.position.x + Bounds.transform.localScale.x)
                {
                    Debug.Log("outside of x coords");
                    pos.x = currentPos.x;
                }
                goto case 1;
            case 1:
                if(pos.z < Bounds.transform.position.z - Bounds.transform.localScale.z || pos.x > Bounds.transform.position.z + Bounds.transform.localScale.z)
                {
                    pos.z = currentPos.z;
                }
                break;
        }

        transform.Translate(pos);
    }
}
