﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This script should be attached to wall clusters, which make up the sides of rooms. Each wall cluster should have two rooms assigned to it, except for ones touching the outside.

public class WallClusterScript : MonoBehaviour
{

    /// <summary>
    /// The first room that the wall cluster forms. Assign in inspector.
    /// </summary>
    public GameObject room1;

    /// <summary>
    /// The second room that the wall cluster forms. Assign in inspector.
    /// </summary>
    public GameObject room2;

    /// <summary>
    /// List of the walls attached to this wall cluster.
    /// </summary>
    List<MeshRenderer> listOfWalls = new List<MeshRenderer>();

    /// <summary>
    /// All the clusters on the rooms that this wall cluster is attached to.
    /// </summary>
    private List<GameObject> tempClusters = new List<GameObject>();

    /// <summary>
    /// True if this wall cluster has a door.
    /// </summary>
    public bool hasDoor;


    /// <summary>
    /// The door attached to this wall cluster.
    /// </summary>
    public DoorToggle thisClusterDoor;

    /// <summary>
    /// True when door is open, false when door is closed.
    /// </summary>
    public bool doorOpened = false;

    // Use this for initialization
    void Start()
    {

        GameObject holder = transform.parent.gameObject;
        for (int i = 0; i < holder.transform.childCount; i++)
        {
            
            //Checks if each wall cluster shares a room with our wall cluster
            if (holder.transform.GetChild(i).GetComponent<WallClusterScript>().room1 == gameObject.GetComponent<WallClusterScript>().room1 ||
                holder.transform.GetChild(i).GetComponent<WallClusterScript>().room1 == gameObject.GetComponent<WallClusterScript>().room2 ||
                holder.transform.GetChild(i).GetComponent<WallClusterScript>().room2 == gameObject.GetComponent<WallClusterScript>().room1 ||
                holder.transform.GetChild(i).GetComponent<WallClusterScript>().room2 == gameObject.GetComponent<WallClusterScript>().room2)
            {
                tempClusters.Add(holder.transform.GetChild(i).gameObject);
            }
        }


    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "wall")
        {
            listOfWalls.AddRange(col.gameObject.GetComponentsInChildren<MeshRenderer>());
        }
        if (col.tag == "door")
        {
            hasDoor = true;
            thisClusterDoor = col.gameObject.GetComponentInChildren<DoorToggle>();
        }
    }

    /// <summary>
    /// Hides all wall Clusters attached to room.
    /// </summary>
    void HideWallsOnRoom()
    {
        //Debug.Log("HideWallsOnRoom() Triggered");
        foreach (MeshRenderer child in listOfWalls)
        {//checks children in the array we made at the top

            MeshRenderer[] rabbitHole = child.gameObject.GetComponentsInChildren<MeshRenderer>();//creates a new array using the children of the child of the last array
            foreach (MeshRenderer child2 in rabbitHole)
            {//accesses new array

                if (child2.tag != "base")
                {//won't hide the base
                    child2.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;//hides everything else
                }
            }
        }
    }

    void ShowWallsOnRoom()
    {
        foreach (MeshRenderer child in listOfWalls)//checks children in the array we made at the top
        {
            MeshRenderer[] rabbitHole = child.gameObject.GetComponentsInChildren<MeshRenderer>();//creates a new array using the children of the child of the last array
            foreach (MeshRenderer child2 in rabbitHole)//accesses new array
            {
                child2.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;//shows everything
            }
        }
    }

    /*
    // Update is called once per frame
    void Update()
    {

        if (thisClusterDoor != null)
        {
            if (!doorOpened)
            {

                if (thisClusterDoor.open)//checks to see if the door attached to this wall cluster is open
                {
                    HideWallsOnRoom();//hides the walls attached to the wall clusters on room1 and room2
                    doorOpened = true;
                    foreach (GameObject others in tempClusters)//iterates trough each wall cluster
                    {
                        others.GetComponent<WallClusterScript>().HideWallsOnRoom();//hides walls using RoomStuff script on other wall clusters
                    }
                }
            }
            else if (!thisClusterDoor.open)
            {
                ShowWallsOnRoom();//shows the walls attached to the wall clusters on room1 and room2
                doorOpened = false;
                foreach (GameObject others in tempClusters)//iterates trough each wall cluster
                {
                    others.GetComponent<WallClusterScript>().ShowWallsOnRoom();//shows walls using RoomStuff script on other wall clusters
                }
            }
        }
    }
    */
}
