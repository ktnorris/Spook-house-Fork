﻿using UnityEngine;
using System.Collections;

public class TransparentWalls : MonoBehaviour {

   
    void Start()
    {
        AlphaToggle(0);
    }
	
	
    public void AlphaToggle(float lvl)
    {
        Material mat;
        Color32 c;

        Renderer[] rendList = GetComponentsInChildren<Renderer>();
        for(int x = 0; x < rendList.Length; x++)
        {
            if (rendList[x].gameObject.tag != "base")
            {
                mat = rendList[x].material;

                c = mat.color;

                c.a = (byte)lvl;

                ///I copy pasted most of this I have no clue what it does
                mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                mat.SetInt("_ZWrite", 0);
                mat.DisableKeyword("_ALPHATEST_ON");
                mat.EnableKeyword("_ALPHABLEND_ON");
                mat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                mat.renderQueue = 3000;
                mat.SetFloat("_Mode", 3f);
                mat.SetColor("_Color", c);
            }
        }
    }
}
