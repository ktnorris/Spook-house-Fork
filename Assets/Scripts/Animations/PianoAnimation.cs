﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PianoAnimation : SpecialAbility {

    Animator pianoAnim;
    float LickWait;
    float IdleWait;
    bool inLoop;

    public void Start()
    {
        pianoAnim = GetComponentInChildren<Animator>();
        LickWait = GetComponent<ActiveAttraction>().ActiveVals.coolDown + (GetComponent<ActiveAttraction>().ActiveVals.upTime / 2);
        IdleWait = 15;
        //StartCoroutine(Idling());
    }

    public override void Init()
    {
        //unused
    }

    public override void OnHover()
    {
        //unused
    }

    public override void Trigger(GameObject guest)
    {
        if (!inLoop)
        {
            SetClicked();
        }
    }

    void SetClicked()
    {
        inLoop = true;
        pianoAnim.SetTrigger("Clicked");
        StartCoroutine(Lick());
    }


    IEnumerator Lick()
    {
        yield return new WaitForSeconds(LickWait);
        pianoAnim.SetTrigger("Reset");
        inLoop = false;
    }


    IEnumerator Idling()
    {
        yield return new WaitForSeconds(IdleWait);
        pianoAnim.SetTrigger("Idling");
        IdleWait = UnityEngine.Random.Range(IdleWait - 3, IdleWait + 3);
        StartCoroutine(Idling());
    }
}
