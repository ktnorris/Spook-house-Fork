﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoffinAnimation : SpecialAbility {

    Animator thisAnim;

    public override void Init()
    {
    }

    public override void OnHover()
    {
    }

    public override void Trigger(GameObject guest)
    {
        SetClicked();
    }

    // Use this for initialization
    void Start () {
        thisAnim = GetComponent<Animator>();
	}
	
    void SetClicked()
    {
        thisAnim.SetTrigger("clicked");

    }

}
