﻿using UnityEngine;
using System.Collections;

public class LighthingDayNight : MonoBehaviour {

    public float changeVal;

    public Vector3 rotationaxis;

    private GameObject miscCode;
	// Use this for initialization
	void Start () {
        miscCode = GameObject.FindGameObjectWithTag("Atlas");

        float timeframe = 12 * miscCode.GetComponent<GameTimeKeeping>().hourLength;
        changeVal = 180 / timeframe;
        Debug.Log("done calculating time");
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(rotationaxis * changeVal * Time.deltaTime);
	
	}
}
