﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/// <summary>
/// The class which tracks game winning and ending conditions
/// </summary>
[Serializable]
public class GameManagerBase : MonoBehaviour {

    //public static GameManagerBase Instance;

    //---------------------------------------------------------------------------------------------------------------------------------
    //A series of delegate and struct declarations for the purpose of making this class react to many different win conditions
    //---------------------------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// A struct for any conditions that could end the game
    /// </summary>
    [Serializable]
    public struct EndConditions
    {
        public float Money;
        public float SoulShards;
        public float FreakOuts;
        public float Days;
        public float Rep;

        public EndConditions(float money, float soulShards, float freakOuts, float days, float rep)
        {
            this.Money = money;
            this.SoulShards = soulShards;
            this.FreakOuts = freakOuts;
            this.Days = days;
            this.Rep = rep;

        }
    }
    /// <summary>
    /// A delegate that declares a method to be run on completion of end conditions
    /// </summary>
    public delegate void End();
    /// <summary>
    /// A delegate that dictates how states should be checked.
    /// </summary>
    /// <param name="state1">The first set of state variables</param>
    /// <param name="state2">The second set of state variables</param>
    /// <param name="endMethod">The callback method to run if it finds similarities</param>
    public delegate void StateCheck(EndConditions state1, EndConditions state2, End endMethod);


    //--------------------------------------------------------------------------------------------------------------------------------
    //Variable declarations for the delegate methods listed above
    //--------------------------------------------------------------------------------------------------------------------------------
    
    /// <summary>
    /// What method to use when checking for a win state
    /// </summary>
    public StateCheck stateCheckerWin;
    /// <summary>
    /// What method to use when checking for a lose state
    /// </summary>
    public StateCheck stateCheckerLoss;
    /// <summary>
    /// What method to use when a win state is achieved.
    /// </summary>
    public End win;
    /// <summary>
    /// What method to use when a loss state is achieved
    /// </summary>
    public End lose;


    //---------------------------------------------------------------------------------------------------------------------------
    //Here we declare the different structs to be used for the win and loss conditions
    //---------------------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// Our win conditions
    /// </summary>
    public EndConditions winConditions;
    /// <summary>
    /// Our loss conditions
    /// </summary>
    public EndConditions loseConditions;
    /// <summary>
    /// The current state of the game
    /// </summary>
    public EndConditions currentState;


    //-------------------------------------------------------------------------------------------------------------------------
    //Contract stuff
    //-------------------------------------------------------------------------------------------------------------------------
    public float contractLimit;
    public List<ContractBase> MainContract = new List<ContractBase>();
    public List<ContractBase> ActiveContracts = new List<ContractBase>();
    public List<ContractBase> QueuedContracts = new List<ContractBase>();
    public ContractViewer ContractViewer;

    //Dictionary of Unlockable Objects;
    private Dictionary<String, GameObject> Unlockables;
    [Serializable]
    public struct unlockable
    {
        public string ItemName;
        public GameObject toUnlock;
    }

    public unlockable[] unlockablesInspector;

    private List<String> UnlockedItems;

    //--------------------------------------------------------------------------------------------------------------------------
    //Methods are defined here
    //--------------------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// Sets default win states and methods for debugging purposes.
    /// </summary>
    public virtual void Init()
    {
        if (stateCheckerWin == null)
        {
            stateCheckerWin = this.DefaultCompareStates;
        }

        if (stateCheckerLoss == null)
        {
            stateCheckerLoss = this.DefaultCompareStates;
        }

        if (win == null)
        {
            win = this.defaultWin;
        }

        if(lose == null)
        {
            lose = this.defaultLose;
        }

        UnlockedItems = new List<string>();
        Unlockables = new Dictionary<string, GameObject>();
        for(int x = 0; x < unlockablesInspector.Length; x++)
        {
            Unlockables.Add(unlockablesInspector[x].ItemName, unlockablesInspector[x].toUnlock);
        }
        

        UpdateUnlocked();

        UpdateState(0, 0, 0, 0, 3);
    }

    /// <summary>
    /// Method to be called whenever you need to set win conditiions.
    /// </summary>
    /// <param name="winCheck"></param>
    /// <param name="lossCheck"></param>
    /// <param name="winMethod"></param>
    /// <param name="lossMethod"></param>
    public void SetDelegates(StateCheck winCheck, StateCheck lossCheck, End winMethod, End lossMethod)
    {
        stateCheckerWin = winCheck;
        stateCheckerLoss = lossCheck;
        win = winMethod;
        lose = lossMethod;
    }

    /// <summary>
    /// Any updates to the state of the game, if no change indicate a 0! 
    /// </summary>
    /// <param name="money">Any monetary change</param>
    /// <param name="soulShards">any soulShard change</param>
    /// <param name="freakOuts">any freakOut change</param>
    /// <param name="time">any time change</param>
    /// <param name="rep">any reputation change</param>
	public void UpdateState (float money, float soulShards, float freakOuts, float days, float rep) {

        this.currentState.Money += money;
        this.currentState.SoulShards += soulShards;
        this.currentState.FreakOuts += freakOuts;
        this.currentState.Days += days;
        this.currentState.Rep = rep;

        
        stateCheckerWin(this.winConditions, this.currentState, this.win);
        stateCheckerLoss(this.loseConditions, this.currentState, this.lose);

        
	
	}


    //-----------------------------------------------------------------------------------------------------------------------------------
    //These methods are to be used for debugging and example purposes not for actual game use
    //-----------------------------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// Default state checker compares all states all the time
    /// </summary>
    /// <param name="state1">The first set of conditions</param>
    /// <param name="state2">The second set of conditions</param>
    /// <param name="endMethod">The method delegate to be called if conditions are met</param>
    void DefaultCompareStates(EndConditions state1, EndConditions state2, End endMethod)
    {
        int Case = 1;

        switch (Case)
        {
            case 1:
                if (state1.Money == state2.Money) endMethod();
                goto case 2;
            case 2:
                if (state1.SoulShards == state2.SoulShards) endMethod();
                goto case 3;
            case 3:
                if (state1.FreakOuts == state2.FreakOuts) endMethod();
                goto case 4;
            case 4:
                if (state1.Days == state2.Days) endMethod();
                goto case 5;
            case 5:
                if (state1.Rep == state2.Rep) endMethod();
                break;
        }
    }

    /// <summary>
    /// A default win state
    /// </summary>
    void defaultWin()
    {
        Debug.Log("You won");
    }

    /// <summary>
    /// A default lose state
    /// </summary>
    void defaultLose()
    {
        Debug.Log("You lost");
    }

    //--------------------------------------------------------------------------------------------------
    //Some contract functions down here
    //-------------------------------------------------------------------------------------------------

    public void AddActiveContract(ContractBase toAdd, bool main, List<ContractBase> listToAdd)
    {
        if (main)
        {
            if (MainContract.Count > 0)
            {
                RemoveActiveContract(MainContract[0], MainContract);
            }
            MainContract.Add(toAdd);
            UpdateContractViewer();
        }else if(listToAdd.Count < contractLimit)
        {
            listToAdd.Add(toAdd);
            UpdateContractViewer();
        }
    }

    public void RemoveActiveContract(ContractBase toRemove, List<ContractBase> removeFrom)
    {
        removeFrom.Remove(toRemove);
        UpdateContractViewer();
    }


    public void UpdateContractViewer()
    {
        ContractViewer.UpdateList(MainContract, ContractViewer.mainContracts);
        ContractViewer.UpdateList(ActiveContracts, ContractViewer.sideContracts);
        ContractViewer.UpdateList(QueuedContracts, ContractViewer.awaitingContracts);
    }


    //----------------------------------------------------------------------------------------------------
    //Stuff to do with unlocks below here
    //----------------------------------------------------------------------------------------------------

    public bool CheckUnlockState(string Name)
    {
        if (Unlockables.ContainsKey(Name))
        {
            return Unlockables[Name].activeInHierarchy;
        }

        return false;
    }


    public void UnlockItem(string Name)
    {
        if (Unlockables.ContainsKey(Name))
        {
            Unlockables[Name].SetActive(true);
            UnlockedItems.Add(Name);
        }
    }

    public void UpdateUnlocked()
    {
        foreach (KeyValuePair<String, GameObject> pair in Unlockables)
        {
            if (pair.Value.activeInHierarchy)
            {
                UnlockedItems.Add(pair.Key);
            }
        }
    }

    public List<string> getUnlockedItems()
    {
        return this.UnlockedItems;
    }

    
}
