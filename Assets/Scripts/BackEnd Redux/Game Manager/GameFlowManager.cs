﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class manages the guest count and how it changes to respond to reputation.
/// </summary>
public class GameFlowManager : GameScoring {

    /// <summary>
    /// The array holding information for how many guests should be spawned given a specific hour.
    /// </summary>
    public float[] GuestArray = new float[24];

    /// <summary>
    /// The algorithm the game uses to determine how and where guests are spawned.
    /// </summary>
    /// <param name="seconds">The time allotment for this phase</param>
    /// <param name="guestCount">How many guests are to be spawned over the phase</param>
    /// <param name="spawnPosition">Where guests will be spawned</param>
    public delegate IEnumerator PhaseType(float seconds, float guestCount, Vector3 spawnPosition);

    public PhaseType currentPhaseAlg;

    /// <summary>
    /// The different guest types that can be called upon to be instantiated
    /// </summary>
    public string[] guestType;


    //---------------------------------------------------------------------------------------------------------------------------------
    //Upkeep and initialazation functions
    //---------------------------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// Exists for testing
    /// </summary>
    void Start()
    {
        Init();
    }

    /// <summary>
    /// Simply initializes some default values doesn't do anything cool.
    /// </summary>
    public override void Init()
    {
        base.Init();

        /*
        for(int x = 0; x < GuestArray.Length; x++)
        {
            if(x < 12)
            {
                GuestArray[x] = 0;
            }
            else
            {
                GuestArray[x] = x % 12;
            }
        }
        */

        currentPhaseAlg = Linear;

    }

    /// <summary>
    /// Calculates the changes in guest numbers according to 
    /// </summary>
    public void UpdateFlow()
    {
        float starMod = CalcStarMod();

        for(int x = 0; x < GuestArray.Length; x++)
        {
            GuestArray[x] *= starMod;
        }
    }

    float CalcStarMod()
    {
        float unedited = currentState.Rep;
        float calculated;
        if (unedited == 0)
        {
            calculated = .25f;
        }
        else
        {
            calculated = unedited / 3;
        }
        return calculated;
    }


    //-------------------------------------------------------------------------------------------------------------------------
    //Phasing Algorithms
    //-------------------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// A basic phasing algorithm that spreads random guest types equally over a number of seconds.
    /// </summary>
    /// <param name="seconds">The time it will take to complete the phase</param>
    /// <param name="guestCount">the number of guests it will spawn</param>
    /// <param name="position">where it will spawn said guests</param>
    IEnumerator Linear(float seconds, float guestCount, Vector3 position)
    {
        float guestAdjusted = Mathf.Round(guestCount);

        //Debug.Log(guestAdjusted);

        if (guestAdjusted != 0)
        {
            float ratio = seconds / guestAdjusted;
            //Debug.Log(ratio);
            float positionalAdjustment = .5f;
            //Debug.Log(positionalAdjustment);

            for (int x = 1; x < guestCount + 1; x++)
            {
                //Debug.Log("delay = " + (x * ratio - ratio * positionalAdjustment).ToString());
                yield return new WaitForSeconds(x * ratio - ratio * positionalAdjustment);
                Instantiate(Resources.Load<GameObject>("SpawnFolder/" + RandomGuestPick()), position, Quaternion.identity);
            }


        }
    }


    string RandomGuestPick()
    {
        int randyRounded = Mathf.RoundToInt(Random.Range(0, guestType.Length - 1));
        return guestType[randyRounded];
    }

}
