﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Handles The game's timekeeping elements 
/// </summary>
public class GameTimeKeeping : GameFlowManager {


    //-------------------------------------------------------------------------------------------------------------------
    //State variables
    //-------------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// Which day the timekeeper is currently on.
    /// </summary>
    public int day;
    /// <summary>
    /// The current hour
    /// </summary>
    public int hour;

    


    //------------------------------------------------------------------------------------------------------------------
    //Measurements
    //------------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// The hour that ends the day officially.
    /// </summary>
    public int cycleEnd;
    /// <summary>
    /// The length in seconds that an hour lasts.
    /// </summary>
    public int hourLength;


    /// <summary>
    /// Where guests will spawn
    /// </summary>
    public GameObject SpawnPos;

    /// <summary>
    /// Where the guests will go to die!
    /// </summary>
    public GameObject KillPosition;

    // Use this for initialization
    void Start () {
        Init();
        StartCoroutine(HourDelay());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Called whenever it is time for the game to continue forward an hour.
    /// </summary>
    void IncrementHour()
    {
         hour++;

        if(hour == 23)
        {
            hour = 0;
        }

        if(hour == cycleEnd)
        {
            EndDay();
        }

        StartCoroutine(currentPhaseAlg(hourLength, GuestArray[hour], SpawnPos.transform.position));
    }

    /// <summary>
    /// All functions that should happen at the end of a day.
    /// </summary>
    void EndDay()
    {
        day++;
        this.PurgeReviews(day - this.reviewBuffer);
        this.UpdateState(0, 0, 0, 1, this.currentState.Rep);
        UpdateFlow();
    }

    /// <summary>
    /// Returns the current hour in a convienient string form.
    /// </summary>
    string WhatHour()
    {
        if(hour > 12)
        {
            return (hour % 24).ToString() + "PM";
        }

        if(hour < 12)
        {
            return (hour % 12).ToString() + "AM";
        }

        if(hour == 24)
        {
            return "12AM";
        }

        if(hour == 12)
        {
            return "12PM";
        }

        return "Not valid time";
    }

    /// <summary>
    /// The time between hours
    /// </summary>
    /// <returns>A coroutine</returns>
    IEnumerator HourDelay()
    {
        yield return new WaitForSeconds(hourLength);
        IncrementHour();
        StartCoroutine(HourDelay());
    }
}
