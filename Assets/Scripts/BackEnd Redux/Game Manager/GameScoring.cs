﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A class that updates the score values in the game manager.
/// </summary>
public class GameScoring : GameManagerBase {

    public float guestCount = 0;
    public float AvgRep = 0;
    private Queue<review> reviewStack;
    /// <summary>
    /// The time for which reviews are held
    /// </summary>
    public float reviewBuffer;

    public struct review
    {
        public float repVal;
        public float day;

        public review(float rep, float curDay)
        {
            repVal = rep;
            day = curDay;
        }
    }


    public override void Init()
    {
        reviewStack = new Queue<review>();
        base.Init();
    }

    /// <summary>
    /// Reports any money changes to the game manager
    /// </summary>
    /// <param name="toAdd">Amount of money to change</param>
    public void AddMoney(float toAdd)
    {
        this.UpdateState(toAdd, 0, 0, 0, currentState.Rep);
    }

    /// <summary>
    /// Reports soul shard changes to the game manager
    /// </summary>
    /// <param name="toAdd">Amount of soul shards to change</param>
    public void AddSoulShards(float toAdd)
    {
        this.UpdateState(0, toAdd, 0, 0, currentState.Rep);
    }

    /// <summary>
    /// Reports any changes to freak out numbers to the game manager.
    /// </summary>
    /// <param name="toAdd">The number of points to change</param>
    public void AddFreakOut(float toAdd)
    {
        this.UpdateState(0, 0, toAdd, 0, currentState.Rep);
    }

    /// <summary>
    /// Reports changes in the reputation score.
    /// </summary>
    /// <param name="toAdd">Reputation score to report</param>
    public void AddReputation(float toAdd)
    {
        reviewStack.Enqueue(new review(toAdd, currentState.Days));
        ComputeReputationAverage();

        guestCount += 1;
    }

    /// <summary>
    /// Adds reputation without averaging it
    /// </summary>
    /// <param name="toAdd">The amount of starts being added</param>
    public void AddRepFlat(float toAdd)
    {
        UpdateState(0, 0, 0, 0, toAdd);
    }

    /// <summary>
    /// Calculates reputation score changes and passes up the average
    /// </summary>
    /// <param name="toAdd">The reputation being added</param>
    private void ComputeReputationAverage()
    {
        float total = 0;

        if (reviewStack.Count < 1)
        {
            foreach (review guestReview in reviewStack)
            {
                total += guestReview.repVal;
            }
        }else
        {
            total = 0;
        }

        float roundedAvg = 0;

        if (reviewStack.Count > 0) { 
            roundedAvg = Mathf.Round(total / reviewStack.Count);
        }
        else
        {
            roundedAvg = Mathf.Round(total / 1);
        }
        AvgRep = roundedAvg;

        UpdateState(0, 0, 0, 0, roundedAvg);
    }

    public void PurgeReviews(float minDay)
    {
        for (int x = 0; x < reviewStack.Count; x++)
        {
            if(reviewStack.Peek().day > minDay)
            {
                break;
            }else
            {
                reviewStack.Dequeue();
            }
        }

        ComputeReputationAverage();
        Debug.Log("I just purged reviews!");
    }
}
