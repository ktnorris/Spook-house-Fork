﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FootprintGenerator : MonoBehaviour {

    public Vector3 gridScale;
    public GameObject footprint;
    public GameObject lineGroup;
    public bool redraw;

    // Use this for initialization
    private void Start()
    {
        redraw = false;
    }

    // Update is called once per frame
    /// <summary>
    /// This creates the footprint and draws the lines
    /// </summary>
    void Update()
    {
        if (redraw)
        {
            if (footprint == null)
            {
                footprint = GameObject.CreatePrimitive(PrimitiveType.Plane);
                footprint.transform.SetParent(gameObject.transform);
                footprint.transform.localPosition = Vector3.zero;
                footprint.transform.localScale = Vector3.one;
                footprint.GetComponent<Renderer>().material = Resources.Load("Materials/Kerry's Mat/FootPrint Material") as Material;
                footprint.tag = "Grid";
            }

            if (lineGroup == null)
            {
                lineGroup = new GameObject("LineGroup");
                lineGroup.transform.SetParent(transform);
                lineGroup.transform.localPosition = Vector3.zero;
                
            }

            
            redraw = false;
        }

        footprint.transform.localScale = gridScale;
        if (lineGroup.transform.childCount < 1)
        {
            GenerateLines();
        }
    }


    /// <summary>
    /// Goes through a four loop and generates a bunch of lines then passes the row, column info to find coordinates to figure out where to draw the line.
    /// </summary>
    void GenerateLines()
    {
        for(float r = 0; r < (gridScale.z * 10) + 1; r++)
        {
            GameObject newLine = new GameObject("Line [" + r.ToString() + " Row]");
            newLine.transform.SetParent(lineGroup.transform);
            LineRenderer line = newLine.AddComponent<LineRenderer>();
            line.material = Resources.Load("Materials/Kerry's Mat/GridMaterial") as Material;
            Vector3[] linePoints = FindLineCoords(false, r);
            line.SetPositions(linePoints);
            LayerDecider(r, newLine);
            line.SetWidth(.1f, .1f);
        }

        for (float c = 0; c < (gridScale.x * 10) + 1; c++)
        {
            GameObject newLine = new GameObject("Line [" + c.ToString() + " Column]");
            newLine.transform.SetParent(lineGroup.transform);
            LineRenderer line = newLine.AddComponent<LineRenderer>();
            line.material = Resources.Load("Materials/Kerry's Mat/GridMaterial") as Material;
            Vector3[] linePoints = FindLineCoords(true, c);
            line.SetPositions(linePoints);
            LayerDecider(c, newLine);
            line.SetWidth(.1f, .1f);
        }

    }

    /// <summary>
    /// Does some black magic to get the lines drawn in the right place
    /// </summary>
    /// <param name="vert">If we're drawing a line on the z-axis</param>
    /// <param name="col">The particular line we're drawing</param>
    /// <returns></returns>
    Vector3[] FindLineCoords(bool vert, float col)
    {

        Vector3[] coords = new Vector3[2];

        if (vert)
        {
            Vector3 orig = new Vector3((col + transform.position.x) - ((gridScale.x * 10) /2), .1f + footprint.transform.position.y, footprint.transform.position.z + footprint.transform.localScale.z * 5);
            Vector3 end = new Vector3((col + transform.position.x) - ((gridScale.x * 10) /2), .1f + footprint.transform.position.y, footprint.transform.position.z - footprint.transform.localScale.z * 5);
            coords[0] = orig;
            coords[1] = end;
        }else
        {
            Vector3 orig = new Vector3(footprint.transform.position.x + footprint.transform.localScale.x * 5, .1f + footprint.transform.position.y, (col + transform.position.z) - ((gridScale.z * 10) /2));
            Vector3 end = new Vector3(footprint.transform.position.x - footprint.transform.localScale.x * 5, .1f + footprint.transform.position.y, (col + transform.position.z) - ((gridScale.z * 10) /2));
            coords[0] = orig;
            coords[1] = end;
        }


        return coords;
    }


    void LayerDecider(float axis, GameObject me)
    {

        int Case = 1;
        switch (Case)
        {
            case 1:
                me.layer = LayerMask.NameToLayer("Grid X 1");
                goto case 3;
            case 3:
                if (axis % 5 == 0) me.layer = LayerMask.NameToLayer("Grid X 5");
                break;
        }
    }

}
