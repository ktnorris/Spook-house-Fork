﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Grid : MonoBehaviour
{

	public int gridWidth;
	public int gridHeight;

	public GameObject tilePrefab;

	public bool redraw = false;

	void Start ()
	{
		redraw = false;
	}

	//When the Scene changes
	void Update ()
	{
		if (redraw) {
			DestroyExtras ();
			InstantiateGrid ();
			redraw = false;

		}
	}


	void InstantiateGrid ()
	{

		for (int h = 0; h < gridHeight; h++) {
			for (int w = 0; w < gridWidth; w++) {
				GameObject currentTile;
				float x = this.transform.position.x;
				float y = this.transform.position.y;
				float z = this.transform.position.z;
				currentTile = Instantiate (tilePrefab, new Vector3 (x + w, y + h, z), Quaternion.identity) as GameObject;
				currentTile.transform.SetParent (this.transform);
			}
		}
	}

	void DestroyExtras ()
	{

		foreach (Transform child in transform) {

			DestroyImmediate (child.gameObject, true);
		}
	}
}
