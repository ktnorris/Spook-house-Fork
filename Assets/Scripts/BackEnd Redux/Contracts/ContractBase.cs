﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ContractBase : MonoBehaviour {


    //-----------------------------------------------------------------------------------------------------------------------------
    /// <summary>
    /// The values required to win.
    /// </summary>
    public GameManagerBase.EndConditions winState;
    /// <summary>
    /// The values required to lose
    /// </summary>
    public GameManagerBase.EndConditions loseState;

    /// <summary>
    /// Options that can be displayed
    /// </summary>
    public List<Dropdown.OptionData> optionText;

    public string contractName;

    /// <summary>
    /// What the contract states to the player
    /// </summary>
    public string descriptionTxt;

    /// <summary>
    /// Personal details of the contract giver
    /// </summary>
    public string contractGiverName;

    /// <summary>
    /// Where to load the character sprite
    /// </summary>
    public Sprite characterSpritePath;

    /// <summary>
    /// How the game knows if you win
    /// </summary>
    /// <param name="winState">The conditions to be met for a win</param>
    /// <param name="currentState">What the current state is</param>
    /// <param name="callBack">The method to call back when the goal is met</param>
    public abstract void WinCheck(GameManagerBase.EndConditions winState, GameManagerBase.EndConditions currentState, GameManagerBase.End callBack);
    /// <summary>
    /// How the game checks if you fail to complete the objective
    /// </summary>
    /// <param name="lossState">What the conditions to lose are</param>
    /// <param name="currentState">What the current conditions are</param>
    /// <param name="callBack">The callback method to complete fail state effects</param>
    public abstract void LossCheck(GameManagerBase.EndConditions lossState, GameManagerBase.EndConditions currentState, GameManagerBase.End callBack);
    /// <summary>
    /// What happens when the goal is met
    /// </summary>
    public abstract void GoalComplete();
    /// <summary>
    /// What happens if the goal is not met
    /// </summary>
    public abstract void GoalFail();

    /// <summary>
    /// What happens if the player accepts this contract
    /// </summary>
    public abstract void AcceptContract();
    /// <summary>
    /// What happens if the player chooses to decline (if that's even possible)
    /// </summary>
    public abstract void DeclineContract();
    /// <summary>
    /// Updates the contract panel in the top-left corner of the screen with information about the current conditions of the contract
    /// </summary>
    public abstract void UpdateOptionText();
}
