﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStart : ContractBase {

    GameManagerBase gManager;

    void Start()
    {
        if (GameObject.FindGameObjectWithTag("Contract Panel") != null)
        {
            Time.timeScale = 0;
            //Debug.Log(Time.timeScale);
            GameObject.FindGameObjectWithTag("Contract Panel").transform.parent.gameObject.SetActive(true);
            GameObject.FindGameObjectWithTag("Contract Panel").GetComponent<ContractPanel>().currentContract = this;
            GameObject.FindGameObjectWithTag("Contract Panel").GetComponent<ContractPanel>().SetInfo(base.descriptionTxt, base.contractGiverName, base.characterSpritePath);

            
        }

    }

    public override void AcceptContract()
    {
        gManager = GetComponent<GameManagerBase>();
        gManager.winConditions = this.winState;
        gManager.loseConditions = this.loseState;
        gManager.SetDelegates(this.WinCheck, this.LossCheck, this.GoalComplete, this.GoalFail);
        Time.timeScale = 1;
        GameObject.FindGameObjectWithTag("Contract Panel").transform.parent.gameObject.SetActive(false);
        gManager.AddActiveContract(this, true, gManager.MainContract);
        UpdateOptionText();

    }

    public override void DeclineContract()
    {
        Debug.Log("how did you manage to do this?");
    }

    public override void GoalComplete()
    {
        Debug.Log("Wow you won, way to go!");
        gManager.currentState.Money += 50;
        //AddNextContract

    }

    public override void GoalFail()
    {
        Debug.Log("You fucking suck");
    }

    public override void LossCheck(GameManagerBase.EndConditions lossState, GameManagerBase.EndConditions currentState, GameManagerBase.End callBack)
    {
        UpdateOptionText();

        //Nothing to see here
    }

    public override void WinCheck(GameManagerBase.EndConditions winState, GameManagerBase.EndConditions currentState, GameManagerBase.End callBack)
    {
        UpdateOptionText();
        if (currentState.Rep > 0)//FIX THIS
        {
            GoalComplete();
        }
    }


    public override void UpdateOptionText()
    {
        //Nothing to see here
    }
}
