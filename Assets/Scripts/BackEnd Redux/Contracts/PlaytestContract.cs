﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This contract will appear at the start of the game and check to see if the player has earned 10 fright points for a win, 5 freak outs for a lose;
/// </summary>
public class PlaytestContract : ContractBase {


    GameManagerBase gManager;

    // Use this for initialization
    void Start()
    {
        if (GameObject.FindGameObjectWithTag("Contract Panel") != null)
        {
            Time.timeScale = 0;
            //Debug.Log(Time.timeScale);
            GameObject.FindGameObjectWithTag("Contract Panel").transform.parent.gameObject.SetActive(true);
            GameObject.FindGameObjectWithTag("Contract Panel").GetComponent<ContractPanel>().currentContract = this;
            GameObject.FindGameObjectWithTag("Contract Panel").GetComponent<ContractPanel>().SetInfo(base.descriptionTxt, base.contractGiverName, base.characterSpritePath);
        }
        
    }

    public override void GoalComplete()
    {
        Debug.Log("Wow you won, way to go!");
        gManager.UnlockItem("Coffin");
    }

    public override void GoalFail()
    {
        Debug.Log("Awwww you lost");
    }

    public override void LossCheck(GameManagerBase.EndConditions lossState, GameManagerBase.EndConditions currentState, GameManagerBase.End callBack)
    {
        UpdateOptionText();

        if(currentState.Days > lossState.Days)
        {
            callBack();
        }
    }

    public override void WinCheck(GameManagerBase.EndConditions winState, GameManagerBase.EndConditions currentState, GameManagerBase.End callBack)
    {
        UpdateOptionText();
        if(currentState.SoulShards >= winState.SoulShards)
        {
            callBack();
        }
    }

    public override void AcceptContract()
    {
        gManager = GetComponent<GameManagerBase>();
        gManager.winConditions = this.winState;
        gManager.loseConditions = this.loseState;
        gManager.SetDelegates(this.WinCheck, this.LossCheck, this.GoalComplete, this.GoalFail);
        Time.timeScale = 1;
        GameObject.FindGameObjectWithTag("Contract Panel").transform.parent.gameObject.SetActive(false);
        gManager.AddActiveContract(this, true, gManager.MainContract);
        UpdateOptionText();
    }

    public override void DeclineContract()
    {
        Debug.Log("how did you manage to do this?");
    }

    public override void UpdateOptionText()
    {
        this.optionText[0].text = gManager.currentState.SoulShards.ToString() + "/" + gManager.winConditions.SoulShards + " Soul Shards";
        this.optionText[1].text = gManager.currentState.Days.ToString() + " Days Remaining";
        gManager.UpdateContractViewer();
    }

    
}
