﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// This class's purpose is to hold references to All major scripts and GameObjects in one place for easy access, Try not to clutter it up with unneccesary information!
/// </summary>
[Serializable]
public class Atlas : MonoBehaviour {


    /// <summary>
    /// The interactable grid space
    /// </summary>
    public GameObject footPrint;

    /// <summary>
    /// The game manager *handles win loss conditions
    /// </summary>
    public GameManagerBase gManager;

    /// <summary>
    /// The scoring manager *handles score updates
    /// </summary>
    public GameScoring gScoring;

    /// <summary>
    /// The guest flow manager *handles guest flow
    /// </summary>
    public GameFlowManager gFlow;

    /// <summary>
    /// The Time keeper *handles hour and day increments
    /// </summary>
    public GameTimeKeeping gTime;

    /// <summary>
    /// A 3D position that represents the spawn point for guests.
    /// </summary>
    public Vector3 spawnPoint;
    /// <summary>
    /// If an object is being dragged by the player;
    /// </summary>
    public GameObject HeldObject;

    /// <summary>
    /// A layerMask showing no grid pieces.
    /// </summary>
    public LayerMask NoGrid;
    /// <summary>
    /// Shows all the grid
    /// </summary>
    public LayerMask AllGrid;
    /// <summary>
    /// Shows a 5x5 grid
    /// </summary>
    public LayerMask FiveGrid;

    public bool SellState;

    /// <summary>
    /// A structure that tracks what inputs should be triggered by what.
    /// </summary>
    [Serializable]
    public struct Inputs
    {
        public KeyCode cameraUp;
        public KeyCode camerDown;
        public KeyCode cameraRight;
        public KeyCode cameraLeft;
        public KeyCode rotateClockwise;
        public KeyCode rotateCounterClockWise;
    }

    /// <summary>
    /// The inputs variable
    /// </summary>
    public Inputs inputSettings;

    private void Start()
    {
        inputSettings.cameraUp = KeyCode.W;
        inputSettings.camerDown = KeyCode.S;
        inputSettings.cameraRight = KeyCode.D;
        inputSettings.cameraLeft = KeyCode.A;
        inputSettings.rotateClockwise = KeyCode.E;
        inputSettings.rotateCounterClockWise = KeyCode.Q;
    }

    /// <summary>
    /// Sets the held object to do stuff
    /// </summary>
    /// <param name="toHold"></param>
    public void SetHeldObject(GameObject toHold)
    {
        this.HeldObject = toHold;

        if(toHold == null)
        {
            Camera.main.cullingMask = this.NoGrid;
        }else if (toHold.tag == "Tower")
        {
            Camera.main.cullingMask = this.AllGrid;
        }else if(toHold.tag == "Room")
        {
            Camera.main.cullingMask = this.FiveGrid;
        }


    }
}
