﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

abstract public class Contract : MonoBehaviour {

    public string name;
    public enum State {Active, Rejected, Failure, Success};
    public int deadline;
    public State state;
    public int goalBaseline;

    [Serializable]
     public struct Goals
    {
        public string type;
        public int amount;
        public int timespan;

        public Goals (string typ, int amt, int time)
        {
            type = typ;
            amount = amt;
            timespan = time;

        }
    }

    [Serializable]
    public struct Rewards
    {
        public string type;
        public int amount;

        public Rewards (string typ, int amt)
        {
            type = typ;
            amount = amt;

        }
    } 

    public Goals goalList;
    public Rewards rewardList;


    protected void ActivateContract (){
        TimeLimit();
        state = State.Active;

    }

    protected void TimeLimit(){
        int currentday = TimeKeeper.Instance.numDays; //Get what the current day is
        deadline = currentday + goalList.timespan; //Set the deadline to be x amount of days from now

    } 

    protected void CheckTime(){
        if (TimeKeeper.Instance.numDays == deadline){
            if (state == State.Active){
                state = State.Failure;
            }
        }
     
    }

    //void function for seeing if goals have been met

    protected void CheckGoals(){
        
        if (goalList.type == "FP"){
            goalBaseline = (int)Scoring.Instance.FrightPoints;
            if (goalBaseline == goalList.amount){
                RewardGive();
                state = State.Success;
            }
            
        }
        

    } 

    //void function for giving rewards (tangible if minor, complete game if major)

    protected void RewardGive(){
        if (rewardList.type == "money"){
            Scoring.Instance.DollaDollaBills += rewardList.amount;
        }
    
    }

    
}
