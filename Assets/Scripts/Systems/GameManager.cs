﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Button = UnityEngine.UI.Button;
using UnityEngine.UI;
using Blur = UnityStandardAssets.ImageEffects.Blur;

public class GameManager : Singleton<GameManager> {
      public static GameManager Instance;

    /// <summary>
    /// The total number of fright points earned - used for the win state condition
    /// </summary>
    private float frightPoints;

    /// <summary>
    /// The total number of freak outs earned - used for the lose state condition
    /// </summary>
    private float freakPoints;

    /// <summary>
    /// The number of fright points needed to win
    /// </summary>
    public float frightWin;

    /// <summary>
    /// The number of freak out points needed to lose
    /// </summary>
    public float freakLose;

    /// <summary>
    /// The game menu we disable after the game is finished
    /// </summary>
    public GameObject disableMenuOnEnd;

    /// <summary>
    /// A gameobject holding menu elements that should be enabled on win/loss
    /// </summary>
    public GameObject toEnableOnEnd;

    /// <summary>
    /// Tracks if the game is in the pathing state or not for our convience
    /// </summary>
    public bool pathState;

    public int timeLimit;


    public GameObject shopButton;
    public GameObject shopMenu;


    /// <summary>
    /// Buttons that control the speed of play
    /// </summary>
    public List<GameObject> playButtons;

    public Text frightText;

    public Text dayText;


    void Start()
    {
        UpdateGameManager(0, 0);
    }
	
	/// <summary>
    /// Called to update the scores within the game manager
    /// </summary>
    /// <param name="fright">The number of fright points to add</param>
    /// <param name="freak">The number of freak out points to add</param>
	public void UpdateGameManager (float fright, float freak)
    {
        frightPoints += fright;
        frightText.text = frightPoints.ToString() + "/" + frightWin.ToString() + " FP";
        freakPoints += freak;
        dayText.text = " Day: " + gameObject.GetComponent<TimeKeeper>().numDays + "/" + timeLimit.ToString();

        if(frightPoints >= frightWin)
        {
            GameEnd(true);
            
        }

        if(gameObject.GetComponent<TimeKeeper>().numDays >= timeLimit)
        {
            GameEnd(false);
        }else if(freakPoints > freakLose)
        {
            GameEnd(false);
        }
    }


    /// <summary>
    /// This is only called externally by the pathing button.
    /// </summary>
    public void PathingToggle()
    {
        pathState = !pathState;
        gameObject.GetComponent<Pathing>().redrawTiles(pathState);
        if (Time.timeScale > 0)
        {
            // Time.timeScale = 0;
            /* for (int x = 0; x < playButtons.Count; x++)
            {
                shopMenu.SetActive(false);
                shopButton.GetComponent<Button>().interactable = false;
                playButtons[x].GetComponent<Button>().interactable = false;
            }
        }
        else
        {
            Time.timeScale = 1;
            for (int x = 0; x < playButtons.Count; x++)
            {
                playButtons[x].GetComponent<Button>().interactable = true;
                shopButton.GetComponent<Button>().interactable = true;
            } */
        }

        //enable pathing script here.
       
    }

    /// <summary>
    /// Called when the game should end
    /// </summary>
    /// <param name="win">Wheter or not the game was won or lost</param>
    void GameEnd(bool win)
    {
        ForcePause();
        Camera.main.GetComponent<Blur>().enabled = true;

        if (win)
        {
            Win();
        }
        else
        {
            Loss();
        }

    }

    /// <summary>
    /// Forcibly pauses everything in the scene
    /// </summary>
    void ForcePause()
    {
        Time.timeScale = 0;
        disableMenuOnEnd.SetActive(false);
        //Do other neato effects here
    }

    /// <summary>
    /// Where any and all win specific effects will be called
    /// </summary>
    void Win()
    {
        toEnableOnEnd.transform.GetChild(0).gameObject.SetActive(true);
    }

    /// <summary>
    /// Where all loss condition effects are called
    /// </summary>
    void Loss()
    {
        toEnableOnEnd.transform.GetChild(1).gameObject.SetActive(true);
    }
}
