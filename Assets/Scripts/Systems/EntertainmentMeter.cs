﻿using UnityEngine;
using System.Collections;

public class EntertainmentMeter : MonoBehaviour
{

	/// <summary>
    /// The Entry Fee set to this guest this scales the entertainment meter also
    /// </summary>
	public int EntryFee = 1;
    /// <summary>
    /// The range that the entertainment meter is measured along
    /// </summary>
	private int Range = 10;

	/// <summary>
    /// The time in-between pulls from guest
    /// </summary>
	public int tickRate;

	/// <summary>
    /// The amount of fear pulled from the guest
    /// </summary>
	public float FearVal;

	/// <summary>
    /// The current fluctuating entertainment value
    /// </summary>
	private int EntertainVal;
    /// <summary>
    /// The start rating coming from this guest
    /// </summary>
	private int starRating;
    /// <summary>
    /// The amount of money tipped from the guest
    /// </summary>
	private float tip;

    /// <summary>
    /// The particle system that "collects" the particles released by guests when they enter critState.
    /// </summary>
    public GameObject particlesCollect;

    /// <summary>
    /// The empty gameObject that we are parenting our particles collect particle system to upon instantiation.
    /// </summary>
    public GameObject particlesHere;

    bool inCrit;


    // Use this for initialization
    public void Start ()
    {
        particlesCollect = Resources.Load("Darkness Inverse") as GameObject;
        particlesHere = GameObject.Find("ParticlesHere");

        if (EntryFee == 0) {
			EntryFee = 1;
		}

		if (tickRate == 0) {
			tickRate = 3;
		}
		tip = 0;

		EntertainVal = 0;
		Range = EntryFee * Range;

        //Here we add the report the entry fee to the score system.
        //gameObject.GetComponent<BaseGuest>().ReportScore(EntryFee) ;
		EntertainmentMeter emeter = gameObject.GetComponent<EntertainmentMeter> ();
		//Debug.Log (emeter.EntryFee);

		//begins the tick and recording process
		StartCoroutine (pullFearVal ());
	
	}

	//Calculate the fear meter with this
	int FearMeter ()
	{
		float maxFear = gameObject.GetComponent<BaseGuest> ().frightValueMax;
		int increment = (int)((FearVal / maxFear) * 10);
		//Debug.Log (increment + "current increment by which to change");
		return increment;
	}
	
	// This coroutine is called to update the fright value
	IEnumerator pullFearVal ()
	{
       
		BaseGuest gscript = gameObject.GetComponent<BaseGuest> ();
		this.FearVal = gscript.frightValue;
		//Debug.Log (FearVal + "Current fear value as reported by emeter");
		this.UpdateMeter ();
		yield return new WaitForSeconds (tickRate);
		StartCoroutine (pullFearVal ());

	}

	//Updates the Entertainment value
	void UpdateMeter ()
	{
		BaseGuest gscript = gameObject.GetComponent<BaseGuest> ();
		int critcutoff = Range * 4 / 5;
		bool critfear = gscript.critState;
		int changeVal = FearMeter ();

        //Debug.Log(changeVal + " Change Value");

        tip = Mathf.Min (tip += changeVal, 100);
       // Debug.Log("Current tip " + tip);
		

		//Debug.Log (EntertainVal + "- current entertainment value");
		if (EntertainVal < critcutoff) { 
			EntertainVal += changeVal;
		} else if (critfear) {
			EntertainVal += changeVal;
			//Debug.Log (EntertainVal + "- entertainment val after computations");
		}
	}


    /// <summary>
    /// Script gets called when guest enters critState - collects the particles released.
    /// </summary>
    /// <returns></returns>
    public IEnumerator InvokeSoulShards(float amount)
    {
        Debug.Log("Getting soul shards");
        GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().gScoring.AddSoulShards(amount);
        GetComponentInChildren<ParticleSystem>().Play();
        yield return new WaitForSeconds(1.0f);
        //GameObject parts = Instantiate(particlesCollect, particlesHere.transform, false) as GameObject;
        yield return new WaitForSeconds(3.0f);
        //Destroy(parts);
        StartCoroutine(SoulShardTickRate(tickRate, amount));

    }

    public IEnumerator SoulShardTickRate(float time, float initAmount)
    {
        yield return new WaitForSeconds(time);
        if (GetComponent<BaseGuest>().critState)
        {
            if (initAmount <= 2)
            {
                StartCoroutine(InvokeSoulShards(initAmount * 2));
            }
        }
    }

    //This calculates money made off tips and star ratings of the customers
    public int[] MazeExit ()
	{
		float preRating = (float)EntertainVal / (float)Range;
		//Debug.Log (preRating + " - prerating");
		if (preRating > .8) {
			starRating = 5;
		} else if (preRating > .6) {
			starRating = 4;
		} else if (preRating > .4) {
			starRating = 3;
		} else if (preRating > .2) {
			starRating = 2;
		} else if (preRating > 0) {
			starRating = 1;
		} else {
			starRating = 0;
		}

        //Debug.Log (starRating + " - the current star rating");
        //Debug.Log(tip / 100);
		tip = Mathf.Round((EntryFee * (tip / 100)));
		Debug.Log (tip + " - final tip");

		int[] startip = new int[2];
		startip [0] = starRating;
		startip [1] =(int) tip;

		return startip;


	}
}
