﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Scoring : Singleton<Scoring> {
      public static Scoring Instance;

	public float FrightPoints;
	public float DollaDollaBills;
    public float freakOutPoints;
	public GameObject Dollars;
	public GameObject FP;
    public GameObject FO;
	public int StarRatingAvg;
	public ArrayList StarValArray;
	public GameObject Stars;
    public GameObject codeHandler;


	void Start ()
	{
		if (DollaDollaBills == 0) {
			DollaDollaBills = 20;
		}
		if (FrightPoints == 0) {
			FrightPoints = 0;
		}

		Stars = GameObject.FindGameObjectWithTag ("Stars");
		StarRatingAvg = 0;
		StarValArray = new ArrayList ();
		UpdateScore ();
	}


	public void AddDollarsandCompute (float frightVal)
	{
		DollaDollaBills += Mathf.Round (frightVal);
		UpdateScore ();
	}

	public void AddFPandCompute (float frightPoints)
	{
		FrightPoints += Mathf.Round (frightPoints);
        codeHandler.GetComponent<GameManager>().UpdateGameManager(frightPoints, 0);
		UpdateScore ();
	}

	public void AddStarCount (int rating)
	{
		StarValArray.Add (rating);
        UpdateScore();

	}


    public void AddFOpoint(float toAdd)
    {
        freakOutPoints += Mathf.Round(toAdd);
        codeHandler.GetComponent<GameManager>().UpdateGameManager(0, toAdd);
        UpdateScore();
    } 


	public void ResetStars(){
		StarRatingAvg = 0;
		StarValArray = new ArrayList ();
        UpdateScore();
	}

	void UpdateScore ()
	{
		Text Dollartxt = Dollars.GetComponent<Text>();
		Text FPtxt = FP.GetComponent<Text>();
		Dollartxt.text = "$" + DollaDollaBills;
		FPtxt.text = "" + FrightPoints;

		int StarValTotal = 0;
		if (StarValArray.Count > 0) {
			for (int x = 0; x < StarValArray.Count; x++) {
				if (x == StarValArray.Count - 1) {
					StarValTotal += (int)StarValArray [x];
					StarRatingAvg = StarValTotal / StarValArray.Count;
				} else {
					StarValTotal += (int)StarValArray [x];
				}
			}
		}

		StarHandler starscript = Stars.GetComponent<StarHandler> ();
		starscript.UpdateStarSprite (StarRatingAvg);
	}


    public void InvokeFrightPoint()
    {
        StartCoroutine(FrightPointAddDelay(2, 1));
    }

    /// <summary>
    /// Amount of time it takes to add fright points to the gui
    /// </summary>
    /// <param name="waitTime">How long it waits</param>
    /// <param name="frightPoints">How many to points to add</param>
    /// <returns></returns>
    public IEnumerator FrightPointAddDelay(float waitTime, float frightPoints)
    {
        yield return new WaitForSeconds(waitTime);
        AddFPandCompute(frightPoints);
    }
}
