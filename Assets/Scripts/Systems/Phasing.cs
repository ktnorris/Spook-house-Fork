﻿using UnityEngine;
using System.Collections;

public class Phasing : MonoBehaviour
{

	//Variables for phase attributes

	/// <summary>
	/// A list of all possible guest types.
	/// </summary>
	public string[] guestList;
	/// <summary>
	/// The type of the phase. Can be linear, quadratic etc.
	/// </summary>
	public string phaseType;
	/// <summary>
	/// The length of the phase in seconds
	/// </summary>
	public int phaseLength;

	//Variables for function
	/// <summary>
	/// Where the guests are spawned from
	/// </summary>
	public Spawner assosiatedSpawn;
	/// <summary>
	/// A boolean that registers wheter the script is mid-phrase
	/// </summary>
	public bool phasing;
    /// <summary>
    /// The amount of time in seconds that the phase is allowed to last
    /// </summary>
    public float timeLimit;


	// Use this for initialization
	void Start ()
	{
		
	}

	// Is called before running a phase, sets phase type to linear and phase length to 15
	public void DefaultPhaseKit ()
	{
		phaseType = "Linear";
		phaseLength = 15;
	}

	//Sets phase type to ramp (increases over time) and sets length to 20 seconds
	public void RampingPhaseKit ()
	{
		phaseType = "Ramp";
		phaseLength = 20;
    
	}

	//If the spawner is active
	//There is no phase running
	//then begin a phase
    /// <summary>
    /// This runphase is outdated runphase time should be called instead
    /// </summary>
	public void RunPhase ()
	{
		
		if (assosiatedSpawn.gameObject.activeInHierarchy) {
			if (!phasing) {
				//if the phase type is linear that a for loop runs until 15 and starts a spawn delay for each iteration.
				if (phaseType == "Linear") {
					for (int x = 0; x < phaseLength; x++) {
						//Creates a new coroutine that waits for X seconds and spawns a guest
						//must use Start Coroutine to initiate
						StartCoroutine (PhaseDelay (x));
					}
				}


				if (phaseType == "Ramp") {
					for (int x = 0; x < phaseLength; x++) {
						if (x < 5 || x >= 15) {
							StartCoroutine (PhaseDelay (x));
						}
						if (x >= 5 && x < 15) {
							StartCoroutine (PhaseDelay (x));
						}
					}
                        
				}

				//example of a phase we could write
				if (phaseType == "Quad") {
					QuadraticSpawner ();
				}

				phasing = true;
			}
		}
	}


    /// <summary>
    /// This run phase will take place over a specific number of seconds and fully complete by the end of the time limit.
    /// takes a specific length a type and a number of guests to be spawned
    /// Current phaseTypes = Linear ...
    /// </summary>
    /// <param name="phaseLengthSeconds">The number of seconds that the phase runs over</param>
    /// <param name="phaseTyping">The type of the phase</param>
    /// <param name="GuestCount">The number of guests spawned over the phase</param>
    public void RunPhaseTimed(float phaseLengthSeconds, string phaseTyping, int GuestCount)
    {
        if(phaseTyping == "Linear")
        {
            //run stuff
            float ratio = phaseLengthSeconds / GuestCount;
            for(float x = 0; x < GuestCount; x++)
            {
                StartCoroutine(PhaseDelay((int) (x * ratio)));
            }
        }
        else
        {
            Debug.Log("not a valid phase type please try again");
        }
    }

	//example external phase call
	void QuadraticSpawner ()
	{
		//bleh
	}

	//A coroutine that spawns a random guest after WaitTime seconds
	IEnumerator PhaseDelay (int WaitTime)
	{
		//creates a random index
		int randomGuest = RandomGuest ();

		//If the spawner is active
		if (assosiatedSpawn.isActiveAndEnabled) {
			//wait function
			yield return new WaitForSeconds (WaitTime);
			//calls the spawner to create a guest out of the guestlist
			assosiatedSpawn.Spawn (guestList [RandomGuest ()]);
			//assosiatedSpawn.Spawn (guestList [RandomGuest ()]);
			//Checks to see if the phase has completed
			CheckForEndPhase (WaitTime + 1); //<- change this?
		}

	}

	//Creates a random int between 0 and guestList length
	int RandomGuest ()
	{
		int random = Random.Range (0, guestList.Length);
		return random;

	}


	//Compares the phaseNumber given to the phaselength and if they are equal ends the phase
	//phasing = false
	void CheckForEndPhase (int PhaseNumber)
	{
		if (PhaseNumber == phaseLength) {
			phasing = false;
		}
	}

	//clears all currently coroutines
	void EmergencyStop ()
	{
		StopAllCoroutines ();
		phasing = false;
	}
}