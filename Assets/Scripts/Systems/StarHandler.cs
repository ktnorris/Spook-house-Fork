﻿using UnityEngine;
using System.Collections;

public class StarHandler : MonoBehaviour
{

	// Use this for initialization
	
	// Update is called once per frame
	public void UpdateStarSprite (int count)
	{
		DeactivateChildren ();
		this.transform.GetChild (count).gameObject.SetActive (true);
	}

	public void DeactivateChildren ()
	{
		for (int x = 0; x < this.transform.childCount; x++) {
			transform.GetChild (x).gameObject.SetActive (false);
		}
	}
}
