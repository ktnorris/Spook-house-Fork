﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class AttractionBase : MonoBehaviour {

    [Serializable]
    public struct BaseAttraction
    {
        public float[] damage;
        public float[] damageCap;
        public float range;
        public LayerMask affectMask;
        public ParticleSystem rangeEffect;
        public ActiveUpgrade.costs towerCosts;

    }

    /// <summary>
    /// The Attraction's stats made nice and pretty
    /// </summary>
    //public AttractionStats attractionStats;
    //public AttractionComponents attractionComponents;
    //public AttractionStates attractionStates;
    public BaseAttraction BaseStats;

    void Start()
    {
        Init();
    }

	/// <summary>
    /// The Initializer funciton - runs all one time setup methods.
    /// </summary>
	public virtual void Init()
    {
        InitializeParticles(GetComponentsInChildren<ParticleSystem>());
    }

    /// <summary>
    /// The final Call whenever a guest is scared.
    /// </summary>
    /// <param name="guest">to scare</param>
    protected void ScareGuest(float scareValue, GameObject guest)
    {
        //Debug.Log(LineOfSightCheck(guest));
        if (LineOfSightCheck(guest))
        {

            ApplyEffects(guest);

            BaseGuest gScript = guest.GetComponent<BaseGuest>();

            float frightFill = BaseStats.damageCap[gScript.guestType - 1] - gScript.frightValue;
            frightFill = Mathf.Min(frightFill, scareValue);

            if (frightFill <= 0)
            {
                frightFill = 0;
            }
            gScript.ChangeFV(frightFill);
        }
    }

    /// <summary>
    /// Gathers and apply's any special effect behaviors on this attraction instance.
    /// </summary>
    /// <param name="guest">the guest to be effected</param>
    protected void ApplyEffects(GameObject guest)
    {
        SpecialAbility[] effectlist = GetComponents<SpecialAbility>();
        for (int x = 0; x < effectlist.Length; x++)
        {
            effectlist[x].Trigger(guest);
        }
    }


    public void InitEffects()
    {
        SpecialAbility[] effectlist = GetComponents<SpecialAbility>();
        foreach(SpecialAbility abilit in effectlist)
        {
            abilit.Init();
        }
    }

    /// <summary>
    /// Checks to see if the guest is in line of sight.
    /// </summary>
    /// <param name="guest">the guest in question</param>
    protected bool LineOfSightCheck(GameObject guest)
    {
        RaycastHit hit;

        Debug.DrawRay(transform.position, guest.transform.position - transform.position, Color.yellow, 10);
        if (Physics.Raycast(transform.position, guest.transform.position - transform.position, out hit, 100.0f, BaseStats.affectMask))
        {

            //Debug.Log(hit.collider.gameObject.name);
            if (hit.collider.gameObject.Equals(guest))
            {

                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Displays all hover information on the tower
    /// </summary>
    protected virtual void OnMouseEnter()
    {
        
            ToggleRange(true);
        
    }

    /// <summary>
    /// Hides all hover functions on the tower
    /// </summary>
    protected virtual void OnMouseExit()
    {
            ToggleRange(false);
    }

    /// <summary>
    /// Initializes the particle systems to be displayed (range system)
    /// </summary>
    protected void InitializeParticles(ParticleSystem[] partArray)
    {
        for(int x = 0; x < partArray.Length; x++)
        {
            if(partArray[x].name == "RangeSystem")
            {
                BaseStats.rangeEffect = partArray[x];
                BaseStats.rangeEffect.startLifetime = BaseStats.range;
            }
        }
    }

    /// <summary>
    /// Turns the range effect on and off
    /// </summary>
    /// <param name="on"></param>
    void ToggleRange(bool on)
    {
        if (on)
        {
            //Stuff turns on
            if (BaseStats.rangeEffect != null)
            {
                BaseStats.rangeEffect.Play();
            }
        }
        else
        {
            if(BaseStats.rangeEffect != null)
            {
                BaseStats.rangeEffect.Stop();
            }
        }
    }
	
	
}
