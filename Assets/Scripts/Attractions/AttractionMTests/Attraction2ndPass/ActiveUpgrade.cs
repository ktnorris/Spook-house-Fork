﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ActiveUpgrade : ActiveAttraction
{

    /// <summary>
    /// The gui element to inject info into.
    /// </summary>
    public GameObject towerPanel;
    
    /// <summary>
    /// The number of points left you can use to upgrade.
    /// </summary>
    public float upgradePoints;

    /// <summary>
    /// Keeps of where upgrades have been allocated.
    /// </summary>
    public float[] tierTracker;

    /// <summary>
    /// A struct that defines all of the possible cost variables.
    /// </summary>
    [Serializable]
    public struct costs
    {
        public float costM;
        public float costS;
        public float costR;
    }

    //Names of the upgrades that this thing has.
    public string up1Name;
    public string up2Name;
    public string up3Name;


    /// <summary>
    /// An array of costs for each upgrade tier.
    /// </summary>
    public costs[] upgradeCosts;

    private void Start()
    {
        Init();
    }


    public override void Init()
    {
        towerPanel = GameObject.FindGameObjectWithTag("TowerPanel");
        Debug.Log("I'm Initiating");
        if (tierTracker.Length == 0)
        {
            tierTracker = new float[3];
        }
        base.Init();
    }

    /// <summary>
    /// Triggers the tower
    /// </summary>
    public override void OnMouseDown()
    {
        base.OnMouseDown();
    }

    /// <summary>
    /// Anything to happen when the mouse 
    /// </summary>
    protected override void OnMouseEnter()
    {
        base.OnMouseEnter();
    }

    /// <summary>
    /// Any effect to play when the mouse leaves the gameobject
    /// </summary>
    protected override void OnMouseExit()
    {
        base.OnMouseExit();
    }

    /// <summary>
    /// Calls the base class and registers clicks as well!
    /// </summary>
    protected override void OnMouseOver()
    {

        if (GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().HeldObject == null)
        {
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                Injector();
            }
            base.OnMouseOver();
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    //Actual stuff happens down here

    /// <summary>
    /// The different paths that an upgrade can be applied to
    /// </summary>
    public enum upPath { Up1, Up2, Up3 };
    public upPath Path;

    public delegate void Up();

    //Methods that will be filled by a child class
    protected Up Upgrade1;
    protected Up Upgrade2;
    protected Up Upgrade3;



    public virtual void Injector()
    {
        //Debug.Log(towerPanel.ToString() + " Injecting towery goodnes like not really though");

        if (GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().HeldObject == null)
        {
            TowerPanel panelScript = towerPanel.GetComponent<TowerPanel>();

            panelScript.tower = gameObject;
            panelScript.towerTier = tierTracker;
            panelScript.UpgradeButtons[0].transform.GetChild(0).GetComponent<Text>().text = up1Name;
            panelScript.UpgradeButtons[1].transform.GetChild(0).GetComponent<Text>().text = up2Name;
            panelScript.UpgradeButtons[2].transform.GetChild(0).GetComponent<Text>().text = up3Name;

            panelScript.towerinfo.DamageTextCur.text = BaseStats.damage[0].ToString();
            panelScript.towerinfo.DamageCapCur.text = BaseStats.damageCap[0].ToString();
            panelScript.towerinfo.CooldownCur.text = ActiveVals.coolDown.ToString();
            panelScript.towerinfo.RangeCur.text = BaseStats.range.ToString();
            panelScript.towerinfo.DurationCur.text = ActiveVals.upTime.ToString();


            //towerPanel.GetComponent<TowerPanel>().UpdateTiers();
            towerPanel.transform.position = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            towerPanel.transform.parent.GetComponent<Canvas>().enabled = true;
            //Path = upPath.Up3;
            //Upgrade();
        }
    }

    /// <summary>
    /// Approximately how an upgrade should work
    /// </summary>
    public virtual void Upgrade()
    {
        int x = 1;

        if (CanAfford())
        {
            
            switch (x)
            {
                case 1:
                    if (Path == upPath.Up1)
                    {
                        //Debug.Log("Upgrade 1");
                        if (Upgrade1 != null)
                        {
                            this.tierTracker[0] += 1;
                            Upgrade1();
                        }
                        break;
                    }
                    goto case 2;
                case 2:
                    if (Path == upPath.Up2)
                    {
                        //Debug.Log("upgrade 2");
                        if (Upgrade2 != null)
                        {
                            this.tierTracker[1] += 1;
                            Upgrade2();
                        }
                        break;
                    }
                    goto case 3;
                case 3:
                    if (Path == upPath.Up3)
                    {
                        if (Upgrade3 != null)
                        {
                            this.tierTracker[2] += 1;
                            Upgrade3();
                            //Debug.Log("Upgrade 3");

                        }


                    }

                    break;
            }
        }
    }


    bool CanAfford()
    {
        GameScoring scoring = GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().gScoring;
		Debug.Log ("scoring: " + scoring);

        int TierSum = (int)(tierTracker[0] + tierTracker[1] + tierTracker[2]);

        if(upgradePoints == 0)
        {
            return false;
        }

        //This is just a really long if check to make sure you have all of the necesary currencies
        
        if(upgradeCosts[TierSum].costM <= scoring.currentState.Money && upgradeCosts[TierSum].costS <= scoring.currentState.SoulShards && upgradeCosts[TierSum].costR <= scoring.currentState.Rep)
        {
            upgradePoints -= 1;
            scoring.AddMoney(-upgradeCosts[TierSum].costM);
            scoring.AddSoulShards(-upgradeCosts[TierSum].costS);
            scoring.AddRepFlat(-upgradeCosts[TierSum].costR);
            return true;
        }
        return false;
    }


}
