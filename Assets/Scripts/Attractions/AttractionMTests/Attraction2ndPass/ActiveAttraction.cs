﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ActiveAttraction : AttractionBase {

    [Serializable]
    public struct ActiveStats
    {
        /// <summary>
        /// The trigger collider for the attraction
        /// </summary>
        public SphereCollider damageCollider;

        /// <summary>
        /// How long it takes for the tower to become usable post scare
        /// </summary>
        public float coolDown;

        /// <summary>
        /// How long the tower is active for after a click
        /// </summary>
        public float upTime;

        /// <summary>
        /// If the tower can be used or not
        /// </summary>
        public bool useable;


        
    }

    private Renderer[] rendlist;

    public ActiveStats ActiveVals;

    private void Start()
    {
        Init();
    }

    /// <summary>
    /// Do cool things if the mouse is over the thing
    /// </summary>
    protected override void OnMouseEnter()
    {
        if (GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().HeldObject == null)
        {
            base.OnMouseEnter();
        }
        
    }

    protected virtual void OnMouseOver()
    {

        if (GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().HeldObject == null)
        {
            if (ActiveVals.useable && rendlist[1].material.color != Color.green)
            {

                //Debug.Log(rendlist.Length);

                for (int x = 0; x < rendlist.Length; x++)
                {
                    rendlist[x].material.color = Color.green;
                }

            }
            else if (!ActiveVals.useable && rendlist[1].material.color != Color.white)
            {
                for (int x = 0; x < rendlist.Length; x++)
                {
                    rendlist[x].material.color = Color.white;
                }
            }
        }
    }

    /// <summary>
    /// Stop doing cool things cause the mouse isn't over this anymore
    /// </summary>
    protected override void OnMouseExit()
    {
        if (GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().HeldObject == null)
        {
            base.OnMouseExit();
            if (rendlist[1].material.color != Color.white)
            {
                for (int x = 0; x < rendlist.Length; x++)
                {
                    rendlist[x].material.color = Color.white;
                }

            }
        }
    }

    public override void Init()
    {
        ActiveVals.useable = true;
        if (gameObject.GetComponent<SphereCollider>() == null)
        {
            CreateColliders();
        }
        //Debug.Log(gameObject.transform.GetComponentInChildren<Renderer>());
		rendlist = gameObject.GetComponentsInChildren<Renderer>();
        base.Init();
        //any other init commands here
    }
	
    /// <summary>
    /// Creates a new Collider from scratch that will serve as our trigger zone
    /// </summary>
	protected void CreateColliders()
    {
        //Creates the damage collider here
        ActiveVals.damageCollider = gameObject.AddComponent<SphereCollider>();
        ActiveVals.damageCollider.radius = BaseStats.range;
        ActiveVals.damageCollider.isTrigger = true;
        ActiveVals.damageCollider.enabled = false;
    }

    /// <summary>
    /// What happens when you click a tower!
    /// </summary>
    public virtual void OnMouseDown()
    {

        if (GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().HeldObject == null)
        {
            if (ActiveVals.useable && gameObject.GetComponent<AttractionPlaceV2>() == null)
            {
                StartCoroutine(ActiveTime());
            }
        }
    }

    protected virtual IEnumerator ActiveTime()
    {
        ActiveVals.useable = false;
        ActiveVals.damageCollider.enabled = true;
        yield return new WaitForSeconds(ActiveVals.upTime);
        ActiveVals.damageCollider.enabled = false;
        yield return new WaitForSeconds(ActiveVals.coolDown);
        ActiveVals.useable = true;
    }

    /// <summary>
    /// What is run whenever a guest enters this trigger zone
    /// </summary>
    /// <param name="other">The guest collider</param>
    protected void OnTriggerEnter(Collider other)
    {
        if(other.tag == "guest" && ActiveVals.damageCollider.enabled)
        {
            ScareGuest(BaseStats.damage[other.gameObject.GetComponent<BaseGuest>().guestType-1], other.gameObject);
            //print(BaseStats.damage[other.gameObject.GetComponent<BaseGuest>().guestType - 1]);
            //print(other.gameObject.name);
        }
    }
}
