﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PassiveAttraction : AttractionBase {

    [Serializable]
    public struct PassiveVals
    {
        public SphereCollider passiveCollider;
    }

    public PassiveVals passiveStats;

	// Use this for initialization
	void Start () {
        CreatePassiveColliders();
        Init();
	}


    //Sets up the tower
    public override void Init()
    {
        CreatePassiveColliders();
        base.Init();
    }

    /// <summary>
    /// Creates trigger collider that matches the range of the tower
    /// </summary>
    void CreatePassiveColliders()
    {
        passiveStats.passiveCollider = gameObject.AddComponent<SphereCollider>();
        passiveStats.passiveCollider.isTrigger = true;
        passiveStats.passiveCollider.radius = BaseStats.range;

        GameObject passiveEffect = Resources.Load("SpawnFolder/PassiveTowerEffect 1") as GameObject;
        GameObject thisEffect = Instantiate(passiveEffect);
        //Debug.Log(thisEffect.ToString());
        thisEffect.transform.SetParent(gameObject.transform);
        thisEffect.transform.localPosition = Vector3.zero;
        thisEffect.GetComponent<ParticleSystem>().startLifetime = BaseStats.range * 2;
    }

    /// <summary>
    /// When the collider damages guests
    /// </summary>
    /// <param name="other">The collider brah</param>
    protected virtual void OnTriggerStay(Collider other)
    {
        if(other.tag == "guest")
        {
            BaseGuest guestScript = other.GetComponent<BaseGuest>();
            guestScript.ChangeFV(this.BaseStats.damage[guestScript.guestType - 1 ] * Time.deltaTime);
        }
    }



}
