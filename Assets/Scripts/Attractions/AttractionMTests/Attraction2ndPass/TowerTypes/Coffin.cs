﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Coffin : ActiveUpgrade {


    [Serializable]
    public struct CoffinUpgradeStats
    {
        public string[] upgradeText;
        public float[] coolDownPercentages;
        public float[] DamageUpgrades;
        public float[] SlowPercentage;
    }

    public CoffinUpgradeStats CoffinStats;

    // Use this for initialization
    void Start()
    {
        Init();
    }


    public override void Init()
    {
        base.up1Name = CoffinStats.upgradeText[0];
        base.up2Name = CoffinStats.upgradeText[1];
        base.up3Name = CoffinStats.upgradeText[2];
        base.Upgrade1 = DamageUp;
        base.Upgrade2 = CoolDown;
        base.Upgrade3 = Slow;
        base.Init();

        for (int x = 0; x < CoffinStats.coolDownPercentages.Length; x++)
        {
            CoffinStats.coolDownPercentages[x] = gameObject.GetComponent<ActiveAttraction>().ActiveVals.coolDown * (CoffinStats.coolDownPercentages[x]);
        }
    }

    private float tierSum;


    /// <summary>
    /// Triggers the tower
    /// </summary>
    public override void OnMouseDown()
    {
        base.OnMouseDown();
    }

    /// <summary>
    /// Anything to happen when the mouse 
    /// </summary>
    protected override void OnMouseEnter()
    {
        base.OnMouseEnter();
    }

    /// <summary>
    /// Any effect to play when the mouse leaves the gameobject
    /// </summary>
    protected override void OnMouseExit()
    {
        base.OnMouseExit();
    }

    /// <summary>
    /// Calls the base class and registers clicks as well!
    /// </summary>
    protected override void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {

            Injector();
            UpgradeButtonFill();
        }
        base.OnMouseOver();
    }


    //Actual upgrade stuff is going to be down here
    public void UpgradeButtonFill()
    {
        tierSum = tierTracker[0] + tierTracker[1] + tierTracker[2];
        GameObject[] Buttons = towerPanel.GetComponent<TowerPanel>().UpgradeButtons;


        //Buttons[0].GetComponent<UpgradeButtonHover>().GrabPanelSetVals(DamageUpgrade, towerPanel.GetComponent<TowerPanel>().towerinfo.DamageTextCur);
        //Buttons[2].GetComponent<UpgradeButtonHover>().GrabPanelSetVals(rangeUpUpgrade, towerPanel.GetComponent<TowerPanel>().towerinfo.RangeCur);
    }




    public void DamageUp()
    {
        tierSum = tierTracker[0];
        StatUp thisStatup;

        int x = (int)tierSum;

        switch (x)
        {

            case 1:
                thisStatup = gameObject.AddComponent<StatUp>();
                thisStatup.DamageChange = CoffinStats.DamageUpgrades[0];
                thisStatup.Init();
                break;
            case 2:
                thisStatup = gameObject.AddComponent<StatUp>();
                thisStatup.DamageChange = CoffinStats.DamageUpgrades[1];
                thisStatup.Init();
                break;
            case 3:
                thisStatup = gameObject.AddComponent<StatUp>();
                thisStatup.DamageChange = CoffinStats.DamageUpgrades[2];
                thisStatup.Init();
                break;
        }
    }


    public void CoolDown()
    {
        tierSum = tierTracker[1];
        int x = (int)tierSum;

        CoolDownReduction reductionS;

        switch (x)
        {
            case 1:
                reductionS = gameObject.AddComponent<CoolDownReduction>();
                reductionS.reductionAmt = CoffinStats.coolDownPercentages[0];
                reductionS.Init();
                break;
            case 2:
                reductionS = gameObject.AddComponent<CoolDownReduction>();
                reductionS.reductionAmt = CoffinStats.coolDownPercentages[1];
                reductionS.Init();
                break;
            case 3:
                reductionS = gameObject.AddComponent<CoolDownReduction>();
                reductionS.reductionAmt = CoffinStats.coolDownPercentages[2];
                reductionS.Init();
                break;
        }
    }


    public void Slow()
    {
        tierSum = tierTracker[2];
        int x = (int)tierSum;

        SlowBehavoir slow;

        switch (x)
        {
            case 1:
                slow = gameObject.AddComponent<SlowBehavoir>();
                slow.slowDegree = CoffinStats.SlowPercentage[0];
                slow.slowTime = 2;
                break;
            case 2:
                slow = gameObject.GetComponent<SlowBehavoir>();
                slow.slowDegree = CoffinStats.SlowPercentage[1];
                break;
            case 3:
                slow = gameObject.GetComponent<SlowBehavoir>();
                slow.slowDegree = CoffinStats.SlowPercentage[2];
                break;
        }
    }
}
