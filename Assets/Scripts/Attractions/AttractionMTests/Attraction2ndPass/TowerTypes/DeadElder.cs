﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadElder : PassiveAttraction {

    private GameObject cleanupPanel;
    private Renderer[] rendlist;


    /// <summary>
    /// The standard init whatever stuff.
    /// </summary>
    public override void Init()
    {
        cleanupPanel = GameObject.FindGameObjectWithTag("Cleanup Panel");
        rendlist = gameObject.GetComponentsInChildren<Renderer>();
        base.Init();
    }

    /// <summary>
    /// Makes the elder look interactable.
    /// </summary>
    protected override void OnMouseEnter()
    {
        if (GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().HeldObject == null)
        {
            for (int x = 0; x < rendlist.Length; x++)
            {
                rendlist[x].material.color = Color.green;
            }
        }
    }

    /// <summary>
    /// Makes it the original color again.
    /// </summary>
    protected override void OnMouseExit()
    {
        if (GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().HeldObject == null)
        {
            for (int x = 0; x < rendlist.Length; x++)
            {
                rendlist[x].material.color = Color.white;
            }
        }
    }


    private void OnMouseOver()
    {
        if (GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().HeldObject == null)
        {
            if (Input.GetKey(KeyCode.Mouse1))
            {
                cleanupPanel.GetComponent<CleanupPanel>().tower = gameObject;
                cleanupPanel.GetComponent<CleanupPanel>().EnablePanel(true);
            }
        }
    }

}
