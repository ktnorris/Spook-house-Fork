﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Piano : ActiveUpgrade {

    
    
    [Serializable]
    public struct PianoUpgradeStats
    {
        public string[] upgradeText;
        public float[] coolDownPercentages;
        public float[] lingerLengths;
        public float[] lingerDamage;
        public float[] cushionPercentage;
    }

    public PianoUpgradeStats PianoStats;

	// Use this for initialization
	void Start () {
        Init();
	}


    public override void Init()
    {
        base.up1Name = PianoStats.upgradeText[0];
        base.up2Name = PianoStats.upgradeText[1];
        base.up3Name = PianoStats.upgradeText[2];
        base.Upgrade1 = Cushion;
        base.Upgrade2 = CoolDown;
        base.Upgrade3 = LingerTrigger;
        base.Init();

        for(int x = 0; x <PianoStats.coolDownPercentages.Length; x++)
        {
            PianoStats.coolDownPercentages[x] = gameObject.GetComponent<ActiveAttraction>().ActiveVals.coolDown * (PianoStats.coolDownPercentages[x]);
        }
    }

    private float tierSum;


    /// <summary>
    /// Triggers the tower
    /// </summary>
    public override void OnMouseDown()
    {
        base.OnMouseDown();
    }

    /// <summary>
    /// Anything to happen when the mouse 
    /// </summary>
    protected override void OnMouseEnter()
    {
        base.OnMouseEnter();
    }

    /// <summary>
    /// Any effect to play when the mouse leaves the gameobject
    /// </summary>
    protected override void OnMouseExit()
    {
        base.OnMouseExit();
    }

    /// <summary>
    /// Calls the base class and registers clicks as well!
    /// </summary>
    protected override void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            
            Injector();
            UpgradeButtonFill();
        }
        base.OnMouseOver();
    }


    //Actual upgrade stuff is going to be down here
    public void UpgradeButtonFill()
    {
        tierSum = tierTracker[0] + tierTracker[1] + tierTracker[2];
        GameObject[] Buttons = towerPanel.GetComponent<TowerPanel>().UpgradeButtons;


        //Buttons[0].GetComponent<UpgradeButtonHover>().GrabPanelSetVals(DamageUpgrade, towerPanel.GetComponent<TowerPanel>().towerinfo.DamageTextCur);
        //Buttons[2].GetComponent<UpgradeButtonHover>().GrabPanelSetVals(rangeUpUpgrade, towerPanel.GetComponent<TowerPanel>().towerinfo.RangeCur);
    }




    public void Cushion()
    {
        tierSum = tierTracker[0];
        Cushion thisCushion;

        int x = (int)tierSum;

        switch (x)
        {
                
            case 1:
                thisCushion = gameObject.AddComponent<Cushion>();
                thisCushion.firePercentage = PianoStats.cushionPercentage[0];
                break;
            case 2:
                thisCushion = gameObject.GetComponent<Cushion>();
                thisCushion.firePercentage = PianoStats.cushionPercentage[1];
                break;
            case 3:
                thisCushion = gameObject.GetComponent<Cushion>();
                thisCushion.firePercentage = PianoStats.cushionPercentage[2];
                break;
        }
    }


    public void CoolDown()
    {
        tierSum =  tierTracker[1];
        int x = (int)tierSum;

        CoolDownReduction reductionS;

        switch (x)
        {
            case 1:
                reductionS = gameObject.AddComponent<CoolDownReduction>();
                reductionS.reductionAmt = PianoStats.coolDownPercentages[0];
                reductionS.Init();
                break;
            case 2:
                reductionS = gameObject.AddComponent<CoolDownReduction>();
                reductionS.reductionAmt = PianoStats.coolDownPercentages[1];
                reductionS.Init();
                break;
            case 3:
                reductionS = gameObject.AddComponent<CoolDownReduction>();
                reductionS.reductionAmt = PianoStats.coolDownPercentages[2];
                reductionS.Init();
                break;
        }
    }


    public void LingerTrigger()
    {
        tierSum = tierTracker[2];
        int x = (int)tierSum;

        Linger lingerEffect;

        switch (x)
        {
            case 1:
                lingerEffect = gameObject.AddComponent<Linger>();
                lingerEffect.lingerDamage = PianoStats.lingerDamage[0];
                lingerEffect.lingerLength = PianoStats.lingerLengths[0];
                break;
            case 2:
                lingerEffect = gameObject.GetComponent<Linger>();
                lingerEffect.lingerDamage = PianoStats.lingerDamage[1];
                lingerEffect.lingerLength = PianoStats.lingerLengths[1];
                break;
            case 3:
                lingerEffect = gameObject.GetComponent<Linger>();
                lingerEffect.lingerDamage = PianoStats.lingerDamage[2];
                lingerEffect.lingerLength = PianoStats.lingerLengths[2];
                break;
        }
    }
}
