﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Pumpkin : ActiveUpgrade {

    [Serializable]
    public struct PumpkinUpgradeStats
    {
        public string[] upgradeText;
        public float[] RangeUpgradePercent;
        public float[] StunTimes;
        public float[] CritFloorPercentage;
    }

    public PumpkinUpgradeStats PumpkinStats;

    // Use this for initialization
    void Start()
    {
        for (int x = 0; x < PumpkinStats.RangeUpgradePercent.Length; x++)
        {
            PumpkinStats.RangeUpgradePercent[x] *= gameObject.GetComponent<AttractionBase>().BaseStats.range;
        }
        Init();
    }


    public override void Init()
    {
        base.up1Name = PumpkinStats.upgradeText[0];
        base.up2Name = PumpkinStats.upgradeText[1];
        base.up3Name = PumpkinStats.upgradeText[2];
        base.Upgrade1 = Range;
        base.Upgrade2 = CritZone;
        base.Upgrade3 = Stun;
        base.Init();

        
    }

    private float tierSum;


    /// <summary>
    /// Triggers the tower
    /// </summary>
    public override void OnMouseDown()
    {
        base.OnMouseDown();
    }

    /// <summary>
    /// Anything to happen when the mouse 
    /// </summary>
    protected override void OnMouseEnter()
    {
        base.OnMouseEnter();
    }

    /// <summary>
    /// Any effect to play when the mouse leaves the gameobject
    /// </summary>
    protected override void OnMouseExit()
    {
        base.OnMouseExit();
    }

    /// <summary>
    /// Calls the base class and registers clicks as well!
    /// </summary>
    protected override void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {

            Injector();
            UpgradeButtonFill();
        }
        base.OnMouseOver();
    }


    //Actual upgrade stuff is going to be down here
    public void UpgradeButtonFill()
    {
        tierSum = tierTracker[0] + tierTracker[1] + tierTracker[2];
        GameObject[] Buttons = towerPanel.GetComponent<TowerPanel>().UpgradeButtons;


        //Buttons[0].GetComponent<UpgradeButtonHover>().GrabPanelSetVals(DamageUpgrade, towerPanel.GetComponent<TowerPanel>().towerinfo.DamageTextCur);
        //Buttons[2].GetComponent<UpgradeButtonHover>().GrabPanelSetVals(rangeUpUpgrade, towerPanel.GetComponent<TowerPanel>().towerinfo.RangeCur);
    }




    public void Range()
    {
        tierSum = tierTracker[0];
        RangeUP RangeUP;

        int x = (int)tierSum;

        switch (x)
        {

            case 1:
                RangeUP = gameObject.AddComponent<RangeUP>();
                RangeUP.rangeChange = PumpkinStats.RangeUpgradePercent[0];
                RangeUP.Init();
                break;
            case 2:
                RangeUP = gameObject.AddComponent<RangeUP>();
                RangeUP.rangeChange = PumpkinStats.RangeUpgradePercent[1];
                RangeUP.Init();
                break;
            case 3:
                RangeUP = gameObject.AddComponent<RangeUP>();
                RangeUP.rangeChange = PumpkinStats.RangeUpgradePercent[2];
                RangeUP.Init();
                break;
        }
    }


    public void CritZone()
    {
        tierSum = tierTracker[1];
        int x = (int)tierSum;

        CritZone critFloorZone;

        switch (x)
        {
            case 1:
                critFloorZone = gameObject.AddComponent<CritZone>();
                critFloorZone.critZoneFloor = PumpkinStats.CritFloorPercentage[0];
                critFloorZone.waitTime = 2;
                critFloorZone.Init();
                break;
            case 2:
                critFloorZone = gameObject.GetComponent<CritZone>();
                critFloorZone.critZoneFloor = PumpkinStats.CritFloorPercentage[1];
                critFloorZone.Init();
                break;
            case 3:
                critFloorZone = gameObject.GetComponent<CritZone>();
                critFloorZone.critZoneFloor = PumpkinStats.CritFloorPercentage[2];
                critFloorZone.Init();
                break;
        }
    }


    public void Stun()
    {
        tierSum = tierTracker[2];
        int x = (int)tierSum;

        SlowBehavoir stun;

        switch (x)
        {
            case 1:
                stun = gameObject.AddComponent<SlowBehavoir>();
                stun.slowTime = PumpkinStats.StunTimes[0];
                stun.slowDegree = 0;
                break;
            case 2:
                stun = gameObject.GetComponent<SlowBehavoir>();
                stun.slowTime = PumpkinStats.StunTimes[1];
                break;
            case 3:
                stun = gameObject.GetComponent<SlowBehavoir>();
                stun.slowTime = PumpkinStats.StunTimes[2];
                break;
        }
    }
}
