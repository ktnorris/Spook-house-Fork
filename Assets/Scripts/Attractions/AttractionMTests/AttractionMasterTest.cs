﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AttractionMasterTest : MonoBehaviour {


    /// <summary>
	/// The name of the goddamn tower which SHOULD HAVE BEEN LIKE ONE OF THE FIRST THINGS
    /// IMPLEMENTED, CHRIST KERRY
	/// </summary>
    public string towerName;

    /// <summary>
    /// The Dps of the tower on each guest type.
    /// In the order of Child, Adult, Elder
    /// </summary>
    public float[] DPS;

    /// <summary>
    /// The Damage caps for each guest type.
    /// In the order of Child, Adult, Elder
    /// </summary>
    public float[] damageCap;

    /// <summary>
    /// The cost of the tower in dollars if applicable
    /// </summary>
    public int costDollars;

    /// <summary>
    /// The cost of the tower in Fright Points if applicable
    /// </summary>
    public int costFP;

    /// <summary>
    /// The cool down period in between uses.
    /// </summary>
    public float coolDown;

    /// <summary>
    /// How many tiles the tower radius should extend from it's origin.
    /// </summary>
    public float aoeRange;

    /// <summary>
    /// The type of tower being dealt with
    /// Active must be triggered
    /// Passive run automatically
    /// </summary>
    public bool active;

    /// <summary>
    /// A boolean that allows the tower to run automatically.
    /// </summary>
    public bool auto;

    /// <summary>
    /// The type of the tower.
    /// This can be either Offensive, Passive, or Roaming represented as 1,2,3 respectively
    /// </summary>
    public float towerType;

    /// <summary>
    /// The duration of the active tower's collider being enabled.
    /// </summary>
    public float activeDuration;

    /// <summary>
    /// Warmup period before a towr activates
    /// </summary>
    public float activeWarmup;

    /// <summary>
    /// Prevents a tower from functioning regularly, (stops its ability to scare if enabled)
    /// </summary>
    public bool damaged;

    /// <summary>
    /// The attraction's particle system script.
    /// </summary>
    public SpookVisual particleScript;

    /// <summary>
    /// The Collider used for detecting guests.
    /// </summary>
    public SphereCollider ourCollider;

    /// <summary>
    /// Tracks the collider that logs automatic activation
    /// </summary>
    public SphereCollider autoCollider;

    /// <summary>
    /// The attraction's cooldown timer script.
    /// </summary>
    public CooldownTimer cdTimer;

    /// <summary>
    /// A state tracker to see if the tower is done cooling down yet
    /// </summary>
    public bool CoolingDown;

    /// <summary>
    /// A state that stops the tower from activating unless placed already.
    /// </summary>
    public bool placed;

    /// <summary>
    /// The number of tiles taken up along the length axis of the attraction
    /// </summary>
    public float AttractionLength;

    /// <summary>
    /// The width of the attraction in tiles.
    /// </summary>
    public float AttractionWidth;

    /// <summary>
    /// The name of the room that this attraction is in.
    /// </summary>
    public string roomName;

    /// <summary>
    /// The room (gameObject) that this attraction is in.
    /// </summary>
    public GameObject room;

    /// <summary>
    /// The particle system on this attraction.
    /// </summary>
    public ParticleSystem thisPartSys;

    /// <summary>
    /// The particle system that shows the tower's range.
    /// </summary>
    public ParticleSystem rangeSys;

    /// <summary>
    /// The parent of all the rooms in the scene.
    /// </summary>
    public GameObject roomHolder;

    /// <summary>
    /// Temporary list that holds the particle effects affected wall clusters.
    /// </summary>
    List<GameObject> tempClusterList = new List<GameObject>();

    /// <summary>
    /// The animator for this particular object
    /// </summary>
    public Animator ourAnim;

    /// <summary>
    /// What layers our raycasts will hit
    /// </summary>
    public LayerMask mask;

    

    public ParticleSystem rangeEffect;


    public Renderer[] internalRendList;

    private ParticleSystem[] dummyArray;

    /// <summary>
    /// This should not determine any prefab values, only what kind of tower function should be called.
    /// </summary>
    void Start() {
        internalRendList = GetComponentsInChildren<Renderer>();
        particleScript = gameObject.GetComponentInChildren<SpookVisual>();//access the Attraction's particle system script
        cdTimer = gameObject.GetComponentInChildren<CooldownTimer>();//access the Attraction's cooldown script
        dummyArray = gameObject.GetComponentsInChildren<ParticleSystem>();

        for (int x = 0; x < dummyArray.Length; x++)
        {
            if (dummyArray[x].gameObject.name == "RangeSystem")
            {
                rangeEffect = dummyArray[x];
            }
        }

        //roomHolder = GameObject.Find("Rooms");


        //Instantiates the collider used for detecting guests

        SetAoeRange();



    }

    void OnMouseEnter()
    {
        if (placed) {
            //Debug.Log(placed);
            if (active && !auto)
            {
                for (int x = 0; x < internalRendList.Length; x++)
                {
                    if (internalRendList[x].gameObject.GetComponent<ParticleSystemRenderer>() == null)
                    {
                        internalRendList[x].material.shader = Shader.Find("Toon/Lit");
                    }
                }
            }

            rangeEffect.Play();
        }
    }


    void OnMouseExit()
    {
        if (active && !auto) {
            for (int x = 0; x < internalRendList.Length; x++)
            {
                if (internalRendList[x].gameObject.GetComponent<ParticleSystemRenderer>() == null)
                {
                    internalRendList[x].material.shader = Shader.Find("Standard");
                }
            }
        }
        rangeEffect.Stop();
    }

    /// <summary>
    /// Sets the collider to be a sphere collider and changes the size of the particle effect to match the range of the collider.
    /// </summary>
	public void SetAoeRange() {
        ourCollider = gameObject.AddComponent<SphereCollider>();
        ourCollider.radius = aoeRange;

        if (gameObject.GetComponentInChildren<ParticleSystem>() != null)
        {


            if (active)
            {
                for (int x = 0; x < dummyArray.Length; x++)
                {
                    if (dummyArray[x].gameObject.name == "DustSystem")
                    {
                        thisPartSys = dummyArray[x];
                    }
                }
                var sz = thisPartSys.sizeOverLifetime;
                sz.enabled = true;

                /*
                AnimationCurve adaptiveCurve = new AnimationCurve();
                adaptiveCurve.AddKey(0.0f, 0.0f);
                adaptiveCurve.AddKey(1.0f, 1.0f);
                sz.size = new ParticleSystem.MinMaxCurve(aoeRange * 2, adaptiveCurve);
                */

            }

            rangeEffect.startLifetime = aoeRange;




        }

        ourCollider.isTrigger = true;
        // ourCollider.enabled = false;

        if (active)
        {
            if (auto)
            {
                autoCollider = gameObject.AddComponent<SphereCollider>();
                autoCollider.radius = aoeRange;
                autoCollider.isTrigger = true;
                autoCollider.enabled = true;
                gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
                ourCollider.enabled = false;
            }
            else
            {
                ourCollider.enabled = false;

            }

        }
        else
        {
            //TODO have passive effect play
            gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
        }
    }

    /// <summary>
    /// Raises the trigger enter event if an active tower it deals its damage all at once ergo ONENTER.
    /// </summary>
    /// <param name="other">The collider attached to the object that is colliding</param>
    void OnTriggerEnter(Collider other)
    {

        if (placed)
        {
            if (active)
            {
                if (!damaged)
                {
                    if (ourCollider.enabled)
                    {
                        if (other.tag == "guest")
                        {
                            int thisGuest = other.GetComponent<TestGuest>().guestType;
                            ScareGuest(DPS[thisGuest - 1], other.gameObject);
                        }
                    }
                }
            }
        }
    }


    /// <summary>
    /// If a tower is passive then the OnTriggerStay function is called to apply fright per second to the guest
    /// </summary>
    /// <param name="other">Other.</param>
    void OnTriggerStay(Collider other)
    {
        if (placed)
        {
            if (!active)
            {
                if (!damaged)
                {
                    if (other.tag == "guest")
                    {
                        int guestType = other.GetComponent<BaseGuest>().guestType;
                        ScareGuest((DPS[guestType - 1] * Time.deltaTime), other.gameObject);
                    }
                }
            }

            if (autoCollider != null)
            {
                if (autoCollider.enabled)
                {
                    if (!damaged)
                    {
                        if (other.tag == "guest")
                        {

                            OnMouseDown();
                        }
                    }
                }
            }
        }
    }


    /// <summary>
    /// An on mouse down function that actively triggers a tower if and only if it is active
    /// </summary>
    void OnMouseDown()
    {
        if (placed)
        {
            if (active)
            {
                //Debug.Log("activating");
                if (!CoolingDown)
                {
                    if (ourAnim != null)
                    {
                        ourAnim.SetBool("Clicked", true);
                    }

                    StartCoroutine(ActiveTime());
                    if (towerName == "Piano")
                    {
                        SoundManager.Instance.Play2D(SoundManager.Instance.pianoplay, gameObject.transform.position, 1f);
                    }
                    else if (towerName == "Coffin")
                    {
                        Debug.Log("THINGS SHOULD BE HAPPENING");
                        SoundManager.Instance.Play2D(SoundManager.Instance.coffinshake, gameObject.transform.position, 1f);

                    }
                }
            }
        }
    }

    /// <summary>
    /// A coroutine that toggles the collider on and off.
    /// </summary>
    /// <returns></returns></returns>
    IEnumerator ActiveTime() {
        CoolingDown = true;
        yield return new WaitForSeconds(activeWarmup);
        if (ourAnim != null)
        {
            ourAnim.SetBool("Clicked", false);
        }

        if (auto)
        {
            autoCollider.enabled = false;
        }
        gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
        gameObject.GetComponent<BoxCollider>().enabled = false;
        ourCollider.enabled = true;
        thisPartSys.Play();
        yield return new WaitForSeconds(activeDuration);
        thisPartSys.Stop();
        ourCollider.enabled = false;
        gameObject.GetComponent<BoxCollider>().enabled = true;

        if (cdTimer != null)
        {
            StartCoroutine(cdTimer.CooldownStart());
        }
        yield return new WaitForSeconds(coolDown);
        gameObject.layer = LayerMask.NameToLayer("Default");
        Debug.Log("Ready for firing");
        CoolingDown = false;
        if (auto)
        {
            autoCollider.enabled = true;
        }
    }


    /// <summary>
    /// Scares the guest within the limits of the damage cap on this tower
    /// </summary>
    /// <param name="scareValue">Amount of fright to be applied</param>
    /// <param name="guest">The guest which it is applied to</param>
    public void ScareGuest(float scareValue, GameObject guest)
    {

        if (SightCheck(guest)){

            TestGuest gScript = guest.GetComponent<TestGuest>();

        float frightFill = damageCap[gScript.guestType - 1] - gScript.frightValue;
        frightFill = Mathf.Min(frightFill, scareValue);

        if (frightFill <= 0)
        {
            frightFill = 0;
        }

            ApplyEffects(guest);
        gScript.ChangeFV(frightFill);
    }
}


    void ApplyEffects(GameObject guest)
    {
        SpecialAbility[] effectlist = GetComponents<SpecialAbility>();
        for(int x = 0; x < effectlist.Length; x++)
        {
            effectlist[x].Trigger(guest);
        }
    }

     


    public void SetPlaced()
    {
        placed = true;
    }

    //---------------------------------------------------------------------------------------------------------
    //RayCasting stuff
    //---------------------------------------------------------------------------------------------------------

    bool SightCheck(GameObject guest)
    {
        RaycastHit hit;
        
        //Debug.DrawRay(transform.position, guest.transform.position - transform.position, Color.yellow, 10);
        if (Physics.Raycast(transform.position, guest.transform.position - transform.position, out hit, 100.0f, mask))
        {
            
            Debug.Log(hit.collider.gameObject.name);
            if (hit.collider.gameObject.Equals(guest))
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
        
    }

    //----------------------------------------------------------------------------------------------------------
    //Experimental particle effect stuff down here
    //----------------------------------------------------------------------------------------------------------

 

        /*
        Debug.Log(thisPartSys.trigger.maxColliderCount);

        for(int i = 0; i < listOfWalls.Count; i++)
        {
            thisPartSys.trigger.SetCollider(i, listOfWalls[i].GetComponent<Collider>());
        }
        */
    }
