﻿using UnityEngine;
using System.Collections;

public class AttractionClick : MonoBehaviour
{

	public float WaitTime = 0;
	public bool reseting = false;
	public bool isClick = false;


	void OnMouseOver ()
	{
		if (Input.GetMouseButtonDown (0)) {
			AttractionTrigger ();
		
		}
		if (Input.GetMouseButtonDown (1)) {
			UpgradeMenuShow ();
		}
	}

	void UpgradeMenuShow ()
	{
		for (int x = 0; x < this.transform.childCount; x++) {
			GameObject child = transform.GetChild (x).gameObject;
			if (child.name == "Upgrades" && child.activeInHierarchy) {
				child.SetActive (false);
			} else if (child.name == "Upgrades") {
				child.SetActive (true);
			}
		}
	}

	void AttractionTrigger ()
	{
		if (!reseting) {
			GameObject child;
			
			for (int x = 0; x < this.transform.childCount; x++) {
				GameObject curChild = transform.GetChild (x).gameObject;
				if (curChild.name == "AccurateRooms") {
					child = curChild;
					child.SetActive (true);
					reseting = true;
					StartCoroutine (AttractionReset ());
					isClick = true;
					if (this.gameObject.GetComponent<Animator> () != null) {
						this.gameObject.GetComponent<Animator> ().SetBool ("Clicked", true);
					}
				}
			}

			Component[] effects = gameObject.GetComponents<SpecialAbility> ();
			if (effects.Length > 0) {
				for (int y = 0; y < effects.Length; y++) {
					SpecialAbility special = (SpecialAbility)effects [y];
					//special.trigger ();
				}
			}


			
		}
	}

	IEnumerator AttractionReset ()
	{
		//put back in when ray's material is on it
		//Renderer ourRender = this.gameObject.GetComponent <Renderer> ();
		//ourRender.material = (Material)Resources.Load ("Art/yellow");

		yield return new WaitForSeconds (WaitTime);
		isClick = false;
		if (this.gameObject.GetComponent<Animator> () != null) {
			this.gameObject.GetComponent<Animator> ().SetBool ("Clicked", isClick);
		}
		this.transform.GetChild (0).gameObject.SetActive (false);
		

		yield return new WaitForSeconds (WaitTime);
		reseting = false;
		//ourRender.material = (Material)Resources.Load ("Art/Blue");



	}
}
