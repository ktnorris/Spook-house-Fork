﻿using UnityEngine;
using System.Collections;

public class RoamingTowerRadius : MonoBehaviour
{
	/// <summary>
	/// A boolean that shows whether or not a guest is being trackeds
	/// </summary>
	public bool tracking = false;

	//This registers whenever something of note enters the radius and reacts to it
	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "guest") {
			if (!tracking) {
                if (transform.childCount > 0)
                {
                    if (transform.GetChild(0).tag == "FreakChild")
                    {
                        
                        transform.GetChild(0).GetComponent<ChildGuest>().TrackMechanics(other.gameObject);
                        tracking = true;
                    }
                    else {
                        RoamingTowerAgent roamer = transform.GetChild(0).gameObject.GetComponent<RoamingTowerAgent>();
                        roamer.TrackMechanics(other.gameObject);
                        tracking = true;
                        }   
                    }
                }
			}
		}


	//Generates a random position isnide of the circle that a roaming class object can move towards
	public Vector3 GenerateNewPosition ()
	{
		//generate a random x and y coord here
		float RandX = Random.Range (transform.position.x - transform.localScale.x / 2, transform.position.x + transform.localScale.x / 2);
		float RandZ = Random.Range (transform.position.z - transform.localScale.z / 2, transform.position.z + transform.localScale.z / 2);
		return new Vector3 (RandX, transform.position.y, RandZ);
	}
}
