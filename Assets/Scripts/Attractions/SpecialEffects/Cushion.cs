﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cushion : SpecialAbility {

    public float firePercentage;
    public float[] damgeCapOrig;
    public float[] guestHealth;

    private void Start()
    {
        Init();
    }

    public override void Init()
    {
        damgeCapOrig = gameObject.GetComponent<AttractionBase>().BaseStats.damageCap;
        guestHealth = new float[damgeCapOrig.Length];
    }

    public override void OnHover()
    {
        throw new NotImplementedException();
    }

    public override void Trigger(GameObject guest)
    {
        float randy = UnityEngine.Random.Range(0f, 100f);
        if(randy <= firePercentage)
        {
            for(int x = 0; x < guestHealth.Length; x++)
            {
                guestHealth[x] = guest.GetComponent<BaseGuest>().frightValueMax;
            }

            gameObject.GetComponent<AttractionBase>().BaseStats.damageCap = guestHealth;
        }else
        {
            gameObject.GetComponent<AttractionBase>().BaseStats.damageCap = damgeCapOrig;
        }
    }
}
