﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Automatic : SpecialAbility {

    private float downTime;
    private SphereCollider thisCol;
    private bool placed;


    public override void Init()
    {
        downTime = gameObject.GetComponent<ActiveUpgrade>().ActiveVals.coolDown + gameObject.GetComponent<ActiveUpgrade>().ActiveVals.upTime;
        Debug.Log(downTime);
        CreateCollider();
    }

    private void CreateCollider()
    {
        if (thisCol == null)
        {
            thisCol = gameObject.AddComponent<SphereCollider>();
        }
        thisCol.radius = gameObject.GetComponent<ActiveAttraction>().BaseStats.range;
        thisCol.isTrigger = true;
    }

    public override void Trigger(GameObject guest)
    {
        //throw new NotImplementedException();
    }

    private void OnTriggerStay(Collider other)
    {
        if (thisCol != null && other.tag == "guest")
        {
            if (thisCol.enabled)
            {
                if (LineOfSightCheck(other.gameObject))
                {
                    Debug.Log("Wammy");
                    gameObject.GetComponent<ActiveAttraction>().OnMouseDown();
                    thisCol.enabled = false;
                    StartCoroutine(DownTime());
                }
            }
        }
    }


    IEnumerator DownTime()
    {
        yield return new WaitForSeconds(downTime);
        thisCol.enabled = true;
    }

    public override void OnHover()
    {
        //throw new NotImplementedException();
    }


    protected bool LineOfSightCheck(GameObject guest)
    {
        RaycastHit hit;

        Debug.DrawRay(transform.position, guest.transform.position - transform.position, Color.yellow, 10);
        if (Physics.Raycast(transform.position, guest.transform.position - transform.position, out hit, 100.0f, GetComponent<AttractionBase>().BaseStats.affectMask))
        {

            Debug.Log(hit.collider.gameObject.name);
            if (hit.collider.gameObject.Equals(guest))
            {

                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
