﻿using UnityEngine;
using System.Collections;

public abstract class SpecialAbility : MonoBehaviour {

    public abstract void Init();
    public abstract void Trigger(GameObject guest);
    public abstract void OnHover();
}
