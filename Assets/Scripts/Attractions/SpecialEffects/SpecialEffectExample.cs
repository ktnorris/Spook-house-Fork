﻿using UnityEngine;
using System.Collections;
using System;

public class SpecialEffectExample : SpecialAbility
{

	// Use this for initialization
	//Must remain public and void use this to call whatever effects you need.
	public override void Trigger (GameObject guest)
	{
		Debug.Log ("I have been specially triggered");
	}

    public override void Init()
    {
        throw new NotImplementedException();
    }

    public override void OnHover()
    {
        throw new NotImplementedException();
    }
}
