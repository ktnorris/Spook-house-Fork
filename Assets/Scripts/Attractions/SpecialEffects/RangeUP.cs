﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeUP : SpecialAbility {

    public float rangeChange;

    /// <summary>
    /// Increases the range and then kills itself
    /// </summary>
    public override void Init()
    {
        AttractionBase attraction = gameObject.GetComponent<AttractionBase>();
        attraction.BaseStats.range += rangeChange;
        attraction.Init();
        gameObject.GetComponent<SphereCollider>().radius = attraction.BaseStats.range;
        Destroy(this);
    }

    public override void OnHover()
    {
        throw new NotImplementedException();
    }

    public override void Trigger(GameObject guest)
    {
        //throw new NotImplementedException();
    }


}
