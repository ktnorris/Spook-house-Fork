﻿using UnityEngine;
using System.Collections;
using System;

public class SlowBehavoir: SpecialAbility
{
    /// <summary>
    /// The amount of time someone is slowed
    /// </summary>
    public float slowTime;
    /// <summary>
    /// The factor by which speed is slowed
    /// </summary>
    public float slowDegree;

    public void Start()
    {
        Init();
    }

    public override void Init()
    {
        //throw new NotImplementedException();
        slowTime = 2;
        slowDegree = .5f;
    }

    public override void Trigger(GameObject guest)
    {
        StartCoroutine(SlowGuest(guest.GetComponent<BaseGuest>()));
    }

   IEnumerator SlowGuest(BaseGuest guest)
    {
        //Debug.Log(1 * .5);
        //Debug.Log(guest.moveSpeed * slowDegree);
        guest.SpeedChange(guest.moveSpeed * slowDegree);
        yield return new WaitForSeconds(slowTime);
        guest.SpeedChange(guest.moveSpeed / slowDegree);
    }

    public override void OnHover()
    {
        //throw new NotImplementedException();
    }
}
