﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CritZone : SpecialAbility {

    public float critZoneFloor;
    private SphereCollider triggerZone;
    public float waitTime;
   

    private void Start()
    {
        Init();
    }

    public override void Init()
    {
        triggerZone = gameObject.GetComponent<ActiveAttraction>().ActiveVals.damageCollider;
    }

    public override void OnHover()
    {
        //not used
    }

    public override void Trigger(GameObject guest)
    {
        // not used
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "guest" && triggerZone.enabled)
        {
            StartCoroutine(effectTime(other.gameObject, waitTime, other.gameObject.GetComponent<BaseGuest>().critFloor));
            other.gameObject.GetComponent<BaseGuest>().critFloor = critZoneFloor;
        }
    }

    IEnumerator effectTime(GameObject guest, float waitTime, float origiinalVal)
    {
        yield return new WaitForSeconds(waitTime);
        guest.GetComponent<BaseGuest>().critFloor = origiinalVal;
    }


}
