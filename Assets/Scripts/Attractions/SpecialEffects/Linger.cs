﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Linger : SpecialAbility {

    public float lingerLength;
    public float lingerDamage;

    public override void Init()
    {
        throw new NotImplementedException();
    }

    public override void OnHover()
    {
        throw new NotImplementedException();
    }

    public override void Trigger(GameObject guest)
    {
        StartCoroutine(LingerEffect(0, guest.GetComponent<BaseGuest>()));
    }

    IEnumerator LingerEffect(float curTime, BaseGuest guest)
    {
        if (curTime < lingerLength)
        {
            yield return new WaitForSeconds(1);
            guest.ChangeFV(lingerDamage);
            StartCoroutine(LingerEffect(curTime += 1, guest));
        }
    }
}
