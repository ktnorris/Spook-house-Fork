﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatUp : SpecialAbility {

    public float DamageChange;

    //public float DamageCapChange;

    /// <summary>
    /// Adds the change values onto all of the attraction stats and then kills itself.
    /// </summary>
    public override void Init()
    {
        AttractionBase toEdit = gameObject.GetComponent<AttractionBase>();

        for (int x = 0; x < toEdit.BaseStats.damage.Length; x++)
        {
            toEdit.BaseStats.damage[x] += DamageChange;
            //toEdit.BaseStats.damageCap[x] += DamageCapChange;
        }
        Destroy(this);
    }

    public override void Trigger(GameObject guest)
    {
        throw new NotImplementedException();
    }

    public override void OnHover()
    {
        //throw new NotImplementedException();
    }
}
