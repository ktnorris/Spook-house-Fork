﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoolDownReduction : SpecialAbility {

    public float reductionAmt;

    public override void Init()
    {
        gameObject.GetComponent<ActiveAttraction>().ActiveVals.coolDown -= reductionAmt;
        Destroy(this);
    }

    public override void OnHover()
    {
        throw new NotImplementedException();
    }

    public override void Trigger(GameObject guest)
    {
        throw new NotImplementedException();
    }

}
