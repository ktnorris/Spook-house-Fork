﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range_Light : SpecialAbility {

    Light rangeLight;
    GameObject lightHolder;
    public LayerMask cullMask;
    public Color CircleColor;
    private float angle;

    

    void InitGameobjectLocation(GameObject lightObj)
    {
        lightObj.transform.localPosition = new Vector3(0, 0, 0);
        lightObj.transform.localPosition += new Vector3(0, 1, 0);
        lightObj.transform.Rotate(new Vector3(90, 0, 0));
        RaycastHit ray;

        Physics.Raycast(lightHolder.transform.position, new Vector3(lightHolder.transform.position.x, -100000, lightHolder.transform.position.z), out ray);

        angle = 2 * (Mathf.Atan(GetComponent<AttractionBase>().BaseStats.range / ray.distance) * Mathf.Rad2Deg);
    }

    void InitLightRange()
    {

        rangeLight = lightHolder.AddComponent<Light>();
        rangeLight.type = LightType.Spot;
        rangeLight.color = CircleColor;

        

        rangeLight.range = 20;
        rangeLight.intensity = 2;
        rangeLight.cullingMask = cullMask;
        rangeLight.shadows = LightShadows.Hard;

        rangeLight.spotAngle = angle;

        
    }

    public override void Init()
    {
        if (lightHolder == null)
        {
            lightHolder = new GameObject();
            lightHolder.transform.parent = this.transform;
            InitGameobjectLocation(lightHolder);
        }
    }

    public override void Trigger(GameObject guest)
    {
        
    }

    public override void OnHover()
    {
        rangeLight.enabled = true;
    }

    public void OnMouseEnter()
    {
        if (lightHolder != null)
        {
            InitLightRange();
        }
    }

    public void OnMouseExit()
    {
        if (rangeLight != null)
        {
            Destroy(rangeLight);
        }
    }
}
