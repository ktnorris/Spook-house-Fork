﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RoomPlacement : AttractionPlaceV2 {

    public bool isDoorConnected;
    private NavMeshObstacle[] obstacles;
    List<GameObject> killList;
    public float connectedDoors;
    public float doorCount;

	// Use this for initialization
	protected override void Start () {
        base.Start();
        killList = new List<GameObject>();
        obstacles = GetComponentsInChildren<NavMeshObstacle>();
        foreach(NavMeshObstacle obs in obstacles)
        {
            obs.enabled = false;
        }

        doorCount = GetComponentsInChildren<DoorContact>().Length;
	}
	
	// Update is called once per frame
	protected override void Update () {
        base.Update();
	}

    

    protected override bool CanPlace()
    { 
        if(connectedDoors >= doorCount)
        {
            isDoorConnected = false;
        }
        base.collisionFlag = !isDoorConnected;
        return base.CanPlace(); 
    }

    /// <summary>
    /// Drop the room sets stuff in the room script?
    /// </summary>
    void Drop()
    {
        foreach(GameObject toKill in killList)
        {
            if (toKill != null)
            {
                toKill.GetComponent<BaseGuest>().KillForNoGoodReason();
            }
        }
        gameObject.AddComponent<RoomBase>();
        DoorContact[] doors = GetComponentsInChildren<DoorContact>();
        for (int x = 0; x < doors.Length; x++)
        {
            doors[x].gameObject.AddComponent<DoorWaypoints>();
            Destroy(doors[x]);
        }

        foreach(NavMeshObstacle obs in obstacles)
        {
            obs.enabled = true;
        }
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "guest")
        {
            killList.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "guest")
        {
            killList.Remove(other.gameObject);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Room")
        {
            base.collisionFlag = true;
        }
    }

    protected override void Lock()
    {
        Drop();
        base.Lock();
    }


}
