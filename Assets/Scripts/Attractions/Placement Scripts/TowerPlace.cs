﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerPlace : AttractionPlaceV2 {

    bool OnRoom;
    RoomBase2 room;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Room")
        {
            OnRoom = true;
            room = other.GetComponentInParent<RoomBase2>();
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Room")
        {
            OnRoom = false;
            room = null;
        }
    }

    protected override bool CanPlace()
    {
        base.collisionFlag = !OnRoom;
        return base.CanPlace();
        
    }

    void Drop()
    {
        room.towersInRoom.Add(gameObject);
        GetComponent<AttractionBase>().InitEffects();
    }

    protected override void Lock()
    {

        Drop();
        base.Lock();
    }


}
