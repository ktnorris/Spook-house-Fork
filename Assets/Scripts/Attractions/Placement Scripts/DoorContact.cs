﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorContact : MonoBehaviour {

    public RoomPlacementNotShit thisRoom;
    private bool Colliding;
    public LayerMask roomLayerMask;
    private GameObject exit;

    private void Start()
    {
        thisRoom = GetComponentInParent<RoomPlacementNotShit>();
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "door")
        {




            if (thisRoom.connectedDoors.Count < thisRoom.doorCount)
            {
                //Debug.Log(other.gameObject.name);
                thisRoom.connectedDoors.Add(this.gameObject.transform.parent.gameObject);
                exit = other.gameObject;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "door")
        {
            if (thisRoom.connectedDoors.Count > 0)
            {
                thisRoom.connectedDoors.Remove(this.gameObject.transform.parent.gameObject);
                exit = null;
            }
        }
    }
    

    public void disableExit()
    {
        if(exit != null)
        {
            exit.transform.GetChild(0).gameObject.SetActive(false);
            exit.GetComponentInParent<RoomBase2>().ConnectAnExit(exit);
        }
    }


    
}
