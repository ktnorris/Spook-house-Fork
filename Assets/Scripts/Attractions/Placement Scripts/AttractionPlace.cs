﻿using UnityEngine;
using System.Collections;

public class AttractionPlace : MonoBehaviour
{
	public bool hasMoved;
	public Vector3 screenPos;
	public Vector3 offset;
	public Plane pl;
	public Ray ray;
	public float ent;
	public GameObject grid;
	private TilePlacement tilePlacer;

	void Start ()
	{

		pl = new Plane (Vector3.up, Vector3.zero);
		//grid = GameObject.FindGameObjectWithTag ("Grid");
	}

	void OnMouseDrag ()
	{
		if (!hasMoved) {
			if (this.gameObject.tag != "GuestSpawn") {
				GameObject child = this.transform.GetChild (0).gameObject;
				child.SetActive (true);
			}
		}
			if (!hasMoved) {
				//print ("Pew Pew raycast");
				ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				//print ("ray established");

				ent = 100.0f;
				//print (pl.Raycast (ray, out ent));
				if (pl.Raycast (ray, out ent)) {
					//print ("analyzing hit");
					Vector3 hitPoint = ray.GetPoint (ent);
					//print (hitPoint);
					gameObject.transform.position = hitPoint;
					gameObject.transform.position = Snappem ();
				}
		}
	}

	Vector3 Snappem ()
	{
		//print ("Snappin");
		Transform gridTrans = grid.transform;
		this.transform.parent = gridTrans;
		float X = Mathf.Round (this.transform.position.x);
		//print (X);
		float Z = Mathf.Round (this.transform.position.z);

		return new Vector3 (X, this.transform.position.y, Z);
 
	}

	void OnMouseUp ()
	{
		if (!hasMoved) {

			if (this.gameObject.tag != "GuestSpawn") {

				GameObject child = this.transform.GetChild (0).gameObject;
				child.SetActive (false);
			}
		}
		if (GameObject.Find ("NonGridThings") != null) {

			transform.parent = GameObject.Find ("NonGridThings").transform;
		}
			hasMoved = true;
	}
}