﻿using UnityEngine;
using System.Collections;
using System;

public class AttractionPlaceV2 : MonoBehaviour {

	/// <summary>
	/// Tracks wheter or not the attraction has moved
	/// </summary>
	public bool hasMoved;
	/// <summary>
	/// The screen position.
	/// </summary>
	public Vector3 screenPos;
	/// <summary>
	/// The offset calculated for the grid.
	/// </summary>
	public Vector3 offset;
	/// <summary>
	/// The plane that the raycast strikes.
	/// </summary>
	public Plane pl;
	/// <summary>
	/// The ray we use for raycasting.
	/// </summary>
	public Ray ray;
	/// <summary>
	/// The place where we hit.
	/// </summary>
	public float ent;
	/// <summary>
	/// The grid gameobject used for creating a plane.
	/// </summary>
	public GameObject grid;
    /// <summary>
    /// If the object has already moved this stores the resting location and returns the object there.
    /// </summary>
    private Vector3 restingPoint;
    /// <summary>
    /// if the item is colliding with something it shouldn't
    /// </summary>
    protected bool collisionFlag;

    /// <summary>
    /// The costs of placing down this whatever it is.
    /// </summary>
    public ActiveUpgrade.costs placementCost;

    /// <summary>
    /// The list of renderers attacted to this that we can change the color of.
    /// </summary>
    private Renderer[] rendlist;

    private Color[] rendColorSaved;

    private Atlas atlas;
    

    //--------------------------------------------------------------------------------------------------------------------------------------------
    //Standard Placement Tech
    //--------------------------------------------------------------------------------------------------------------------------------------------

	// Use this for initialization
	protected virtual void Start () {
		grid = GameObject.FindGameObjectWithTag ("Grid");
		pl = new Plane (Vector3.up, grid.transform.position);
        BeginCollisionDetection();
        Debug.Log("I'm setting up this shit");
        GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().SetHeldObject(this.gameObject);
        rendlist = gameObject.GetComponentsInChildren<Renderer>();
        rendColorSaved = new Color[rendlist.Length];
        atlas = GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>();

        for (int x = 0; x<rendlist.Length; x++)
        {
            if (rendlist[x].material.HasProperty("_Color"))
            {
                rendColorSaved[x] = rendlist[x].material.GetColor("_Color");
            }
                
            }
        }
	
	// Update is called once per frame
	protected virtual void Update ()
	{
			ray = Camera.main.ScreenPointToRay (Input.mousePosition);

			ent = 100.0f;
        if (pl.Raycast(ray, out ent))
        {
            Vector3 hitPoint = ray.GetPoint(ent);
            gameObject.transform.position = Snappem(hitPoint);
        }

        //Color Changer here
        bool place = CanPlace();

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (hasMoved)
            {
                if (place)
                {
                    Lock();
                }
            }else
            {
                if (place)
                {
                    PurchaseAndLock();
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (hasMoved)
            {
                ReturnToRest();
            }else
            {
                KillObject();
            }
        }

        if (Input.GetKeyDown(atlas.inputSettings.rotateClockwise))
        {
            AttRotate(1);
        }

        if (Input.GetKeyDown(atlas.inputSettings.rotateCounterClockWise))
        {
            AttRotate(-1);
        }


	}



    protected virtual void AttRotate(float direction)
    {
        this.transform.Rotate(new Vector3(0, direction * 90, 0));
    }

	Vector3 Snappem (Vector3 toRound)
	{
		//print ("Snappin");
		Transform gridTrans = grid.transform;
        //transform.parent = gridTrans;
        

		float X = Mathf.Round((toRound.x - gridTrans.position.x) /(offset.x)) * offset.x;
        //Debug.Log(X);
		//print (X);
		float Z = Mathf.Round((toRound.z - gridTrans.position.z) / offset.z) * offset.z;
        //Debug.Log(Z);

		return  keepOnGrid(new Vector3 ((X) + gridTrans.position.x, grid.transform.position.y, (Z) + gridTrans.position.z));

	}



    /// <summary>
    /// God don't make fun of me for this I don't want to do math
    /// </summary>
    /// <param name="toCheck">The vector we're checking</param>
    /// <returns></returns>
    Vector3 keepOnGrid(Vector3 toCheck)
    {
        if(toCheck.x >= grid.transform.position.x + grid.transform.localScale.x * 5)
        {
            toCheck.x = gameObject.transform.position.x;
        }

        if(toCheck.x < grid.transform.position.x - grid.transform.localScale.x * 5)
        {
            toCheck.x = gameObject.transform.position.x;
        }

        if(toCheck.z > grid.transform.position.z + grid.transform.localScale.z*5)
        {
            toCheck.z = gameObject.transform.position.z;
        }

        if(toCheck.z < grid.transform.position.z - grid.transform.localScale.z*5)
        {
            toCheck.z = gameObject.transform.position.z;
        }

        //Debug.Log(toCheck);
        return toCheck;
    }

    //---------------------------------------------------------------------------------------------------------------------------------
    //Replacement (tower menu) code
    //---------------------------------------------------------------------------------------------------------------------------------

    public virtual void Replacement(Vector3 returnPoint)
    {
        hasMoved = true;
        restingPoint = returnPoint;
    }

    protected virtual void ReturnToRest()
    {
        GetComponent<BoxCollider>().isTrigger = false;
        gameObject.transform.position = restingPoint;
        Lock();
    }

    protected virtual void Lock()
    {
        GetComponent<BoxCollider>().isTrigger = false;
        RemoveFromHeld();
        ClearColor();
        Destroy(this);
    }

    protected virtual void RemoveFromHeld()
    {
        GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().SetHeldObject(null);
    }


    //----------------------------------------------------------------------------------------------------------------------------------
    //Tower purchasing
    //----------------------------------------------------------------------------------------------------------------------------------

    protected virtual void PurchaseAndLock()
    {
        GameScoring score = GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().gScoring;

        if (CanPurchase(score, placementCost))
        {
            score.AddMoney(-placementCost.costM);
            score.AddSoulShards(-placementCost.costS);
            score.AddRepFlat(-placementCost.costR);
        }

        Lock();
    }


    private void KillObject()
    {
        RemoveFromHeld();
        Destroy(gameObject);
    }


    private bool CanPurchase(GameScoring score, ActiveUpgrade.costs placementCost)
    {
        if (placementCost.costM <= score.currentState.Money && placementCost.costS <= score.currentState.SoulShards && placementCost.costR <= score.currentState.Rep)
        {
            return true;
        }else
        {
            return false;
        }
            
    }


    //--------------------------------------------------------------------------------------------------------------------------------------
    //Visual Feedback color changing stuff;
    //--------------------------------------------------------------------------------------------------------------------------------------


    protected virtual bool CanPlace()
    {

        Color toChange;
        bool canPlaceit;
        

        if(!collisionFlag && CanPurchase(GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().gScoring, this.placementCost))
        {
            toChange = Color.green;
            canPlaceit = true;
        }else
        {
            toChange = Color.red;
            canPlaceit = false;
        }
            if (rendlist.Length > 0 && rendlist[1].material.color != toChange)
            {
                for (int x = 0; x < rendlist.Length; x++)
                {
                    if (rendlist[x] != null)
                    {
                        rendlist[x].material.color = toChange;
                    }
                }
            }
        

        return canPlaceit;
    }


    protected void ClearColor()
    {
        for (int x = 0; x < rendlist.Length; x++)
        {
            if (rendlist[x] != null)
            {
                rendlist[x].material.color = rendColorSaved[x];
            }
        }
    }

    //---------------------------------------------------------------------------------------------------------------------------------------
    //Tower collision
    //---------------------------------------------------------------------------------------------------------------------------------------


    void BeginCollisionDetection()
    {
        gameObject.GetComponent<Collider>().isTrigger = true;
    }

    private void OnTriggerStay(Collider other)
    {

        if (other.CompareTag(gameObject.tag) && !other.isTrigger)
        { 
            collisionFlag = true;
        }

        
    }



    private void OnTriggerExit(Collider other)
    {
        if (other.tag == gameObject.tag)
        {
            collisionFlag = false;
        }
        
    }


}
