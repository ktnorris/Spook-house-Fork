﻿using UnityEngine;
using System.Collections;

public class Towers : MonoBehaviour {

    public string towerName;
    public string towerDesc;
    public int towerCost;
    public float dpsKids;
    public float dpsAdults;
    public float dpsElders;


    

	// Use this for initialization
	void Start () {
	
	}




	
	// Update is called once per frame
	void Update () {
	
	}

    public void PianoSet(){

        towerName = "Piano";
        towerDesc = "This is a piano.";
        towerCost = 2300;
        dpsKids = 1.5f;
        dpsAdults = 3.0f;
        dpsElders = 1.5f;
        

    }

    public void PumpkinSet(){

        towerName = "Pumpkin";
        towerDesc = "This is a Pumpkin.";
        towerCost = 2200;
        dpsKids = 3f;
        dpsAdults = 1.5f;
        dpsElders = 1.5f;

    }

    
    public void CoffinSet(){

        towerName = "Coffin";
        towerDesc = "This is a coffin.";
        towerCost = 2100;
        dpsKids = 1.5f;
        dpsAdults = 1.5f;
        dpsElders = 1.5f;

    }


    
}
