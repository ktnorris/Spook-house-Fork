﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//Make sure Canvas > Image is a direct child of the tower

public class CooldownTimer : MonoBehaviour {

    /// <summary>
    /// The image we are using as our circular timer.
    /// </summary>
    public Image circleTimer;

    /// <summary>
    /// The amount of time in our timer (cooldown).
    /// </summary>
    public float timeLeft;

    /// <summary>
    /// The object that the GUI Image faces towards.
    /// </summary>
    public GameObject target;

    /// <summary>
    /// The attraction that this script is attached to.
    /// </summary>
    public GameObject thisTower;

    /// <summary>
    /// The tower's instance of the AttractionMaster script.
    /// </summary>
	public ActiveAttraction attMaster;

    /// <summary>
    /// True when attraction is cooling down, false when attraction is ready to be activated.
    /// </summary>
    public bool cooldownActive = false;

    /// <summary>
    /// The number displayed in the center of the circle timer. Make sure this is the second child of Image.
    /// </summary>
    public Text frontNum;

    /// <summary>
    /// The black outline for the number in the center of the circle timer (to make it easier to see). Make sure this is the first child of Image.
    /// </summary>
    public Text backNum;

    /// <summary>
    /// The float we use to update the GUI Numbers in the circle timer.
    /// </summary>
    public float timerNumsFloat;

    // Use this for initialization
    void Start () {

        circleTimer = GetComponent<Image>();
        thisTower = transform.parent.gameObject.transform.parent.gameObject;//Accesses the parent 2 levels up
        circleTimer.fillAmount = 1f;//Progress bar starts full
        target = GameObject.FindGameObjectWithTag("MainCamera") ;//Accesses the child "target" of the Main Camera, which is an empty game object 20 units behind the camera
		attMaster = thisTower.GetComponent<ActiveAttraction>();
        frontNum = gameObject.transform.GetChild(1).GetComponent<Text>();
        backNum = gameObject.transform.GetChild(0).GetComponent<Text>();
		timerNumsFloat = attMaster.ActiveVals.coolDown - 0.001f;

    }
	
	// Update is called once per frame
	void Update () {

        transform.LookAt(target.gameObject.transform);//causes the Image to always face the camera
        transform.GetChild(0).transform.rotation = Quaternion.LookRotation(gameObject.transform.position - target.gameObject.transform.position);//causes the number to always face the camera
        transform.GetChild(1).transform.rotation = Quaternion.LookRotation(gameObject.transform.position - target.gameObject.transform.position);//causes the number to always face the camera
        frontNum.text = "" + (timerNumsFloat + 1).ToString()[0];
        backNum.text = "" + (timerNumsFloat + 1).ToString()[0];
        if (cooldownActive)
        {
            circleTimer.fillAmount -= 1.0f / attMaster.ActiveVals.coolDown * Time.deltaTime;//reduces the fill amount over time
            timerNumsFloat -= attMaster.ActiveVals.coolDown / attMaster.ActiveVals.coolDown * Time.deltaTime;//reduces the timer number over time

        }
        

    }

    public IEnumerator CooldownStart ()
    {
        Debug.Log("Cooldown Activated");
        timerNumsFloat = attMaster.ActiveVals.coolDown - 0.001f;
        cooldownActive = true;
        circleTimer.gameObject.transform.parent.gameObject.layer = 0;//Sets the canvas' layer to default, making it visible
        yield return new WaitForSeconds(attMaster.ActiveVals.coolDown);
        cooldownActive = false;
        circleTimer.gameObject.transform.parent.gameObject.layer = 9;//sets the canvas' layer back to invisible, making it INvisible
        circleTimer.fillAmount = 1.0f;//Resets the cooldown fill amount
        timerNumsFloat = attMaster.ActiveVals.coolDown - 0.001f;//resets the displayed number

    }
}