﻿using UnityEngine;
using System.Collections;

public class RoamingTowerAgent : MonoBehaviour
{


	/// <summary>
	/// The gameobject representing the roaming radius
	/// </summary>
	public GameObject roamRadius;
	/// <summary>
	/// The roamingTower radius script on the roaming radius
	/// </summary>
	private RoamingTowerRadius roamScript;
	/// <summary>
	/// The minimum possible random wait time
	/// </summary>
	public float waitMin;
	/// <summary>
	/// The maximum possible random wait time
	/// </summary>
	public float waitMax;
	/// <summary>
	/// A 3d vector that represents the destination point
	/// </summary>
	private Vector3 dest;
	/// <summary>
	/// The current random wait time
	/// </summary>
	private float randWait;
	/// <summary>
	/// how long a agent will track a guest for
	/// </summary>
	public float trackDuration;
	/// <summary>
	/// The range at which an agent will be triggered to act
	/// </summary>
	public float effectRange;
	/// <summary>
	/// The nav mesh attached to this agent
	/// </summary>
	private MoveToNavMesh thisNav;

	// Use this for initialization
	void Start ()
	{
		thisNav = gameObject.GetComponent<MoveToNavMesh> ();
//		roamRadius = transform.parent.gameObject;
		roamScript = roamRadius.GetComponent<RoamingTowerRadius> ();
		RoamMechanics ();
	}


	//Creates a random destination and wait time and then has the roam class execute it
	void RoamMechanics ()
	{
		RoamingTowerRadius parScript = roamRadius.GetComponent<RoamingTowerRadius> ();
		dest = parScript.GenerateNewPosition ();
		randWait = Random.Range (waitMin, waitMax);
		StartCoroutine (RoamTime ());
	}

	//When the destination is acutally changed and waited on.
	IEnumerator RoamTime ()
	{
		MoveToNavMesh thisNav = gameObject.GetComponent<MoveToNavMesh> ();
		thisNav.GoalChange (dest);
		yield return new WaitForSeconds (randWait);
		RoamMechanics ();

	}

	//When the trigger radius detects a guest this will be called instead
	//Roam class will track to an area and trigger an effect.
	public void TrackMechanics (GameObject guest)
	{
		StopAllCoroutines ();
		thisNav.SpeedSet (gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().speed * 2);
		thisNav.GoalChange (guest.transform.position);

	}

	public void ForceStop ()
	{
		StopAllCoroutines ();
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "guest") {
			if (roamScript.tracking) {
				//trigger an effect
				Debug.Log ("boooo");
				roamScript.tracking = false;
				if (this.tag == "FreakChild") {
					//trigger freak out effects
					ChildFO childFreakState = gameObject.GetComponent<ChildFO> ();
					childFreakState.ApplyEffects (other.gameObject);
				}
				if (other.gameObject.GetComponent<Guest> ().guestType == 1) {
					thisNav.SpeedSet (gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().speed / 2);
					RoamMechanics ();
				}
			} else {
				Debug.Log ("Huh what was that");
			}
		}
	}





}
