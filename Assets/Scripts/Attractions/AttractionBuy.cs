﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AttractionBuy : MonoBehaviour {

    public GameObject shopUI;
    public Button pumpkin;

    public int ContactNumberMin;
    /// <summary>
    /// Holds the scoreHandler 
    /// </summary>
	private GameScoring scoreHandler;

    /// <summary>
    /// Witholds the ability to place an object unless it is on the grid.
    /// </summary>
    private bool onGrid;

    /// <summary>
    /// Lets us know if the ground is obstructed where we want to build.
    /// </summary>
    private bool obstructed;

    /// <summary>
    /// Tracks if the player needs to 
    /// </summary>
    private bool rotateState = true;

    public bool bought;


    private AttractionMaster attractionStats;
    public AttractionMaster mainScript;

    public bool colLock;

    // Use this for initialization
    void Start() {
        bought = false;
        init();
		scoreHandler = GameObject.FindGameObjectWithTag("Atlas").GetComponent<GameScoring>();
        attractionStats = gameObject.GetComponent<AttractionMaster>();
    }

    void RenableUI() {

        pumpkin.interactable = true;

    }

    void Update()
    {

        //If the user wants to place something
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (!obstructed)
            {
                //can it buy?
                if (CanBuy())
                {
                    if (ContactArray.Count > 0)
                    {
                        //Buy
                        Place();
                        if (gameObject.GetComponent<AttractionMaster>().towerName == "Pumpkin"){
                            SoundManager.Instance.Play2D(SoundManager.Instance.pumpkinplace, gameObject.transform.position, 1f);
                        }
                        else{
                            SoundManager.Instance.Play2D(SoundManager.Instance.hardplace, gameObject.transform.position, 1f);
                        }


                    }
                    else {
                                CannotPlace();
                        }

                }
                else
                {
                    //complain that you don't have enough money
                    CannotPlace();



                }
            }
            else if (obstructed){
                CannotPlace();
            }

        }

        //For Rotation purposes

        if (rotateState)
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                RotateAroundY(90.0f);
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                RotateAroundY(-90.0f);
            }
        }



        //If 
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            RenableUI();
            Reset();

        }
    }


    void RotateAroundY(float angle)
    {
        transform.Rotate(new Vector3(0, angle, 0));
    }

    /// <summary>
    /// A quick truth engine that determines if the player has the money to place the object
    /// </summary>
    /// <returns>A bool representing if the player can place an object</returns>
    bool CanBuy()
    {

        //TODO add in tile referencing here to see if you can build on the spot


        float CostD = attractionStats.costDollars;
        float CostFP = attractionStats.costFP;

		float PoolD = scoreHandler.currentState.Money;
		float PoolFP = scoreHandler.currentState.SoulShards;

        if (CostD <= PoolD && CostFP <= PoolFP)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Stops the tower from moving and uncolors tiles
    /// </summary>
    void Place()
    {
        gameObject.GetComponent<AttractionPlaceV2>().enabled = false;
        TintTiles(true);
        UpdateContactArray();
        FinalBuy();



    }


    void FinalBuy()
    {
		scoreHandler.AddMoney(-attractionStats.costDollars);
		scoreHandler.AddSoulShards(-attractionStats.costFP);

        //GameObject Copy = Instantiate(gameObject, this.transform.position, this.transform.rotation, null) as GameObject;
        mainScript = gameObject.GetComponent<AttractionMaster>();

        TintTiles(false);
        UpdateContactArray();


        gameObject.GetComponent<Collider>().isTrigger = false;
        mainScript.SetAoeRange();

        mainScript.SetPlaced();

        mainScript.AssignPartColliders();

        bought = true;
        TintTiles(true);
        RenableUI();
        Destroy(gameObject.GetComponent<AttractionPlaceV2>());
        Destroy(gameObject.GetComponent<AttractionBuy>());

        //babby = Copy;

        //gameObject.GetComponent<AttractionPlaceV2>().enabled = true;


    }

    /// <summary>
    /// Destroys the dummy tower
    /// </summary>
    void Reset()
    {

        TintTiles(true);
        GameObject bleh = GameObject.FindGameObjectWithTag("sHandler");
        if (bleh.GetComponent<GameManager>().pathState)
        {
            bleh.GetComponent<Pathing>().redrawTiles(true);
        }
        Destroy(this.gameObject);

    }

    /// <summary>
    /// Yells at the player for not being able to place the tower
    /// </summary>
    void CannotPlace()
    {
        //SoundManager.Instance.Play2D(SoundManager.Instance.error, gameObject.transform.position, 0.7f);
    }


    /*
    ------------------------------------------------------------------------------------------------------------------------------------------------------------
    Here I'm going to dive into the tile placement mechanics, this will determine if one is able to physically place the object rather than with resources
    aka money fright points etc.
    */

    /// <summary>
    /// The points that the collider has determined its in cotact with.
    /// </summary>
    private List<GameObject> ContactArray;

    /// <summary>
    /// A quick initialazation for the contact array
    /// </summary>
    void init()
    {
        ContactArray = new List<GameObject>();
        AttractionMaster master = gameObject.GetComponent<AttractionMaster>();
        CreatePlacementCollider(master.AttractionLength, master.AttractionWidth);
    }


    void CreatePlacementCollider(float length, float width)
    {
        BoxCollider placeCollide = gameObject.AddComponent<BoxCollider>();
        placeCollide.size = new Vector3(width - .5f, 2, length - .5f);
        placeCollide.center.Set(0, 1, 0);
        placeCollide.isTrigger = true;

    }

    /// <summary>
    /// Fires whenever a trigger collider finds a tile to add to its contact list
    /// </summary>
    /// <param name="other">The collider of the tiles</param>
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "room")
        {
            GetComponent<AttractionMaster>().roomName = other.gameObject.name;
            GetComponent<AttractionMaster>().room = other.gameObject;
        }

        if (other.gameObject.tag == "Tile")
        {
            ContactArray.Add(other.gameObject);
        }

        if (other.tag == "wall")
        {
            colLock = true;
        }

        if (other.tag == "block")
        {
            colLock = true;
        }


    }

/*
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "wall")
        {
            colLock = true;
        }

        if(other.tag == "block")
        {
            colLock = true;
        }
    }
    */

    /// <summary>
    /// Fires when we need to remove a contact point
    /// </summary>
    /// <param name="other">The contact list</param>
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Tile")
        {

            ContactArray.Remove(other.gameObject);
            other.gameObject.GetComponent<Renderer>().material.color = Color.white;
            other.gameObject.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
            GameObject bleh = GameObject.FindGameObjectWithTag("sHandler");
            if (bleh.GetComponent<GameManager>().pathState)
            {
                bleh.GetComponent<Pathing>().redrawTiles(true);
            }
        }

        if(other.tag == "block")
        {
            Debug.Log("Removing lock");
            colLock = false;
        }

        if(other.tag == "wallCluster")
        {
            colLock = false;
        }
    }

    /// <summary>
    /// Determines if any of the points are obstructed
    /// </summary>
    /// <returns>A bool showing if the tiles have any obstruction in their set</returns>
    bool TruthEngine()
    {
        bool localBool = true;
        if (ContactArray.Count >=  ContactNumberMin)
        {
            
        for (int x = 0; x < ContactArray.Count; x++)
        {
            if (localBool && !ContactArray[x].GetComponent<Tile>().Occupied)
            {
                localBool = true;
            }
            else
            {
                localBool = false;
            }
        }
        return localBool && !colLock;
        }
        else
        {
            return false;
        }
}

    /// <summary>
    /// Tints all the tiles in the set to a color
    /// </summary>
    /// <param name="reset">If true it will reset the tiles to their original color</param>
    public void TintTiles(bool reset)
    {
        List<GameObject> tilesToTint = ContactArray;
        bool obstructionsExist = TruthEngine();
        if (!reset)
        {
			if (obstructionsExist && CanBuy())
            {
                obstructed = false;
                //tint green for area
                for (int x = 0; x < tilesToTint.Count; x++)
                {
                    GameObject curTile = tilesToTint[x];
                    curTile.GetComponent<Renderer>().material.color = Color.green;
					curTile.GetComponent<Renderer> ().material.shader = Shader.Find ("Unlit/Color");
                }
                //tint yellow for range
            }
            else
            {
                obstructed = true;
                //tint red
                for (int x = 0; x < tilesToTint.Count; x++)
                {
                    GameObject curTile = tilesToTint[x];
                    curTile.GetComponent<Renderer>().material.color = Color.red;
					curTile.GetComponent<Renderer> ().material.shader = Shader.Find ("Unlit/Color");
                }
            }
        }
        else
        {
            for (int x = 0; x < tilesToTint.Count; x++)
            {
                GameObject curTile = tilesToTint[x];
                curTile.GetComponent<Renderer>().material.color = Color.white;
				curTile.GetComponent<Renderer> ().material.shader = Shader.Find ("Standard");

            }
        }
    }


    /// <summary>
    /// Changes the occupied values on any tiles that should be changed.
    /// </summary>
    void UpdateContactArray()
    {
        for(int x = 0; x < ContactArray.Count; x++)
        {
            ContactArray[x].GetComponent<Tile>().Occupied = true;
        }
    }






	
}
