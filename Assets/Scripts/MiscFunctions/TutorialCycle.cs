﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialCycle : MonoBehaviour {

    //All the tutorial panels
    public GameObject tut1;
    public GameObject tut2;
    public GameObject tut3;
    public GameObject tut3point5;
    public GameObject tut4;
    public GameObject tut5;
    public GameObject tut6;
    public GameObject tut7; 
    public GameObject tut8;
    public GameObject tut9;
    public GameObject tut10;

   
    public GameObject shopUI;
    public Button shopButton;
    public Button doorButton;

    //Need these to test whether they've been placed
    private GameObject testPumpkin;
    private GameObject testCoffin;

    //Enable/disable these buttons so player doesn't place something they're not supposed to
    public Button pianoBuy;
    public Button pumpkinBuy;

    public GameObject CodeHandler;

    //Stupid door to focus stupid camera on
    
    public GameObject cameraHolder; //stupid camera holder
    public GameObject cameraTarget;

    //Conditions used in tut2 to check whether player has done specified movement commands
    private bool cond1;
    private bool cond2;
    private bool cond3;
    private bool cond4;

    
    //Toggling walls
    public GameObject wallscript;

    //Use this a lot to pull various methods from it
    public GameObject menubuttons;

    private bool spawned;
    public GameObject spawner;

    //Stupid images for friggin spook scare tutorial thing
    public Image img1;
    public Image img2;
  

    public GameObject pumpkinParticle;
    public GameObject pianoParticle;
    public GameObject babbyParticle;

	// Use this for initialization
	void Start () {

    cond1 = false;
    cond2 = false;
    cond3 = false;
    cond4 = false;
    tut1.SetActive(true);
    tut2.SetActive(false);
    tut3.SetActive(false);
    tut4.SetActive(false);
    tut5.SetActive(false);
    tut6.SetActive(false);
    tut7.SetActive(false);
    tut8.SetActive(false);
    tut9.SetActive(false);
    tut10.SetActive(false);

    shopButton.interactable = false;
        pianoParticle.SetActive(false);
        pumpkinParticle.SetActive(false);
        doorButton.enabled = false;
    
	
	}
	
    public void LoadIntoMain(){
        SceneManager.LoadScene("MainScene");
        
    }
	// Update is called once per frame
	void Update () {

        //Movement tutorial
        if (tut2.activeSelf && tut2.GetComponent<TutorialText>().text1.enabled == true){
             if (Input.GetKeyDown(KeyCode.Q)){
                cond1 = true;
             }
            if (Input.GetKeyDown(KeyCode.E)){
                cond2 = true;
            }

            if (cond2 == true && cond1 == true){
                 tut2.GetComponent<TutorialText>().nextButton.interactable = true;
                 

            }

        }

        if (tut2.activeSelf && tut2.GetComponent<TutorialText>().text2.enabled == true){
             if (Input.GetKeyDown(KeyCode.W)){
                cond1 = true;
             }
            if (Input.GetKeyDown(KeyCode.A)){
                cond2 = true;
            }
            if (Input.GetKeyDown(KeyCode.S)){
                cond3 = true;
            }
            if (Input.GetKeyDown(KeyCode.D)){
                cond4 = true;
            }

            if (cond2 == true && cond1 == true && cond3 == true && cond4 == true){
                 tut2.GetComponent<TutorialText>().nextButton.interactable = true;
                

            }

        }

    if (tut2.activeSelf && tut2.GetComponent<TutorialText>().text3.enabled == true){
            
             if (Input.GetAxis ("Mouse ScrollWheel") != 0){
                tut2.GetComponent<TutorialText>().nextButton.interactable = true;
             }         
         
        }



    if (tut3point5.activeSelf)
        {
            if (tut3point5.GetComponent<TutorialText>().text1.enabled == true)
            {
                if (doorButton.enabled != true)
                {
                    doorButton.enabled = true;
                    
                    Next();
                }
            }
            
        }
    

    if (tut4.activeSelf && tut4.GetComponent<TutorialText>().text1.enabled == true){

            if (cond1 && cond2){
            tut4.GetComponent<TutorialText>().nextButton.interactable = true;
            }
    }

        if (tut6.activeSelf)
        {
            if (tut6.GetComponent<TutorialText>().text1.enabled == true)
            {
                if (pumpkinParticle != null && pumpkinParticle.activeSelf != true)
                {
                    pumpkinParticle.SetActive(true);
                    pianoParticle.SetActive(false);
                    pianoBuy.interactable = false;
                    pumpkinBuy.interactable = true;
                }
            }

            if (tut6.GetComponent<TutorialText>().text2.enabled == true)
            {
                if (pumpkinParticle != null && pumpkinParticle.activeSelf == true)
                {
                    pumpkinParticle.SetActive(false);

                }


            }




                if (tut6.GetComponent<TutorialText>().text3.enabled == true) 
            {
                if (pianoParticle != null && pianoParticle.activeSelf != true)
                {
                    pumpkinParticle.SetActive(false);
                    pianoParticle.SetActive(true);
                    pumpkinBuy.interactable = false;
                    pianoBuy.interactable = true;
                }
            }
        }
    //Buying tutorial
    if (tut6.activeSelf && tut6.GetComponent<TutorialText>().text2.enabled == true) {

        if (testPumpkin != null && testPumpkin.GetComponent<AttractionMaster>().placed == true){
                tut6.GetComponent<TutorialText>().nextButton.interactable = true;
                pumpkinBuy.interactable = false;
                
        }

}

 

    if (tut6.activeSelf && tut6.GetComponent<TutorialText>().text3.enabled == true){
        pianoBuy.interactable = true;
        
        

    }


    
if (tut6.activeSelf && tut6.GetComponent<TutorialText>().text4.enabled == true){
        
        if (testPumpkin != null && testPumpkin.GetComponent<AttractionMaster>().placed == true){
                
                tut6.GetComponent<TutorialText>().nextButton.interactable = true;
                 shopButton.enabled = false;
                
                
                
        }

}

//Clicking tutorial
if (tut7.activeSelf && tut7.GetComponent<TutorialText>().text1.enabled == true){
            if (babbyParticle.activeSelf != true)
            {
                babbyParticle.transform.SetParent(testPumpkin.transform);
                babbyParticle.transform.localPosition = new Vector3(0, 0, 0);
                babbyParticle.transform.localRotation = new Quaternion(45, 0, 0,0);
                babbyParticle.SetActive(true);
            }
        if (testPumpkin.GetComponent<AttractionMaster>().CoolingDown == true){
            tut7.GetComponent<TutorialText>().nextButton.interactable = true;
                babbyParticle.SetActive(false);
        }
    }

    if (tut9.activeSelf == true){
            tut9.GetComponent<TutorialText>().nextButton.interactable = true;
        if (tut9.GetComponent<TutorialText>().text1.enabled == true){
        img1.enabled = true;
        img2.enabled = false;
        
        }
        else if (tut9.GetComponent<TutorialText>().text2.enabled == true){
        img2.enabled = true;
     
        img1.enabled = false;
        }
        
	
	}

    if (tut8.activeSelf == true && spawned == false && tut8.GetComponent<TutorialText>().text1.enabled == true){

        spawner.GetComponent<Spawner>().Spawn("Elder");
        spawned = true;
        
    }
 
   }
    
    //These are more or less hard-coded functions that are very similar to existing functions, just with the added functionality of triggering something for the tutorial.
    //DIdn't want to mess in other scripts, so they are centralized here for one-time use.
    public void showShop(){      
        
        shopUI.gameObject.SetActive(true);    
        SoundManager.Instance.Play2D(SoundManager.Instance.buttonclick, gameObject.transform.position, 0.5f);
        tut5.SetActive(false);
        tut6.SetActive (true);
         tut6.GetComponent<TutorialText>().nextButton.interactable = false;
    }

    public void toggleWalls(){

    wallscript.GetComponent<WallHider>().ToggleWalls();
    tut3.GetComponent<TutorialText>().nextButton.interactable = true;
    }

   

    public void timeForward1(){
        cond1 = true;

    }
    public void timeForward2(){
        cond2 = true;
    }

    public void TutorialBuyPumpkin(){
        
       
        GameObject testbutt = menubuttons.GetComponent<MenuButtons>().BuyTowerTutorial();
         if (testbutt != null){
         testPumpkin = testbutt;
         tut6.GetComponent<TutorialText>().CycleText();
         
            }
        else {
            return;
        }

    }

    public void toggleDoors(){
        tut3point5.GetComponent<TutorialText>().nextButton.interactable = true;
        
    }

    //This cycles between the tutorial panels
    public void Next(){
            Debug.Log("send halp");
                cond1 = false;
                 cond2 = false;
                 cond3 = false;
                 cond4 = false;

        if (tut1.activeSelf){
            
            if (tut1.GetComponent<TutorialText>().CycleText() == "nextpanel"){
            tut1.SetActive(false);
            tut2.SetActive (true);
            tut2.GetComponent<TutorialText>().nextButton.interactable = false;
            }

            else {
                return;
            }
        }

        else if (tut2.activeSelf){
            tut2.GetComponent<TutorialText>().nextButton.interactable = false;
            if (tut2.GetComponent<TutorialText>().CycleText() == "nextpanel"){
    
            tut2.SetActive(false);
            tut3.SetActive (true);
            tut3.GetComponent<TutorialText>().nextButton.interactable = false;

            }

           else {
                
                return;
            }
        }

        else if (tut3.activeSelf){
            tut3.GetComponent<TutorialText>().nextButton.interactable = false;
          if (tut3.GetComponent<TutorialText>().CycleText() == "nextpanel"){
    
            tut3.SetActive(false);
            tut3point5.SetActive (true);
            tut3point5.GetComponent<TutorialText>().nextButton.interactable = false;
           if (wallscript.GetComponent<WallHider>().wallsHidden == true){
                wallscript.GetComponent<WallHider>().ToggleWalls();
        }


            }

           else {
                return;
            }


        }

        else if (tut3point5.activeSelf){
            
          if (tut3point5.GetComponent<TutorialText>().CycleText() == "nextpanel"){
    
            tut3point5.SetActive(false);
            
            tut4.SetActive (true);
            tut4.GetComponent<TutorialText>().nextButton.interactable = false;

            }

           else {
                return;
            }


        }

        else if (tut4.activeSelf){
            if (tut4.GetComponent<TutorialText>().CycleText() == "nextpanel"){
    
            tut4.SetActive(false);
            tut5.SetActive (true);
            menubuttons.GetComponent<MenuButtons>().PlayToggle();
            shopButton.interactable = true;
            }

           else {
                return;
            }
        }


        


        else if (tut6.activeSelf){

            tut6.GetComponent<TutorialText>().nextButton.interactable = false;
           
           if (tut6.GetComponent<TutorialText>().CycleText() == "nextpanel"){
    
            tut6.SetActive(false);
            tut7.SetActive (true);
            menubuttons.GetComponent<MenuButtons>().hideShopForce();
            tut7.GetComponent<TutorialText>().nextButton.interactable = false;

            }

           else {
                return;
            }
        }

        else if (tut8.activeSelf){
            
            if (tut8.GetComponent<TutorialText>().CycleText() == "nextpanel"){
    
            tut8.SetActive(false);
            tut9.SetActive (true);
           
            
            

            }

           else {
                return;
            }
        }

      else if (tut7.activeSelf){
            
           if (tut7.GetComponent<TutorialText>().CycleText() == "nextpanel"){
    
            tut7.SetActive(false);
            tut8.SetActive (true);
            CodeHandler.GetComponent<CameraZoom>().SetCamera(5f);
            cameraHolder.transform.position = new Vector3 (0.38f, 0f, 5.23f);
            cameraHolder.transform.rotation = Quaternion.Euler (0, 138.675f, 0f);
            cameraTarget.transform.localPosition = new Vector3(-3f, 0f, 2.45f);
            cameraTarget.transform.localRotation = Quaternion.Euler (29.98f, 44.75f, 0f);

            }

           else {
                return;
            }
        }


    else if (tut9.activeSelf){
           if (tut9.GetComponent<TutorialText>().CycleText() == "nextpanel"){
    
            tut9.SetActive(false);
            tut10.SetActive(true);

            }

           else {
                return;
            }
        }


    

    }
}
