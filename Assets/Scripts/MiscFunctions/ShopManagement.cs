﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopManagement : MonoBehaviour {

    public string itemName;
    private string itemDescription;
    private int itemCost;
    public Text costButton;

    public GameObject buyButton;
  

    public Text desc;
    
	// Use this for initialization
	void Start () {

     if (itemName == "Piano"){
        GameObject piano = Resources.Load("SpawnFolder/Piano") as GameObject;
        
        itemCost = piano.GetComponent<AttractionMaster>().costDollars;
        itemDescription = "Name: Piano\t\tType: Active\nThis haunted piano will chomp its teeth to scare guests when activated, terrifying them.";
    }

    if (itemName == "Pumpkin"){
        GameObject pumpkin = Resources.Load("SpawnFolder/pumpkin") as GameObject;
        itemCost = pumpkin.GetComponent<AttractionMaster>().costDollars;
        itemDescription = "Name: Jack o'Lantern\t\tType:Passive\nThis spooky Jack 'o Lantern is very atmospheric. It will passively scare guests nearby by a small amount.";
        

    }

    if (itemName == "Coffin"){
        GameObject coffin = Resources.Load("SpawnFolder/coffin") as GameObject;
        
        itemCost = coffin.GetComponent<AttractionMaster>().costDollars;
        itemDescription = "Name: Coffin\t\tType: Automatic\nThere's probably something inside this coffin that's making it rattle! It will activate and spook guests who get too close.";

    }

        if (itemName == "Spoopy Piano"){
        GameObject paino = Resources.Load("SpawnFolder/Spoopy Piano") as GameObject;
        
        itemCost = 50;
        itemDescription = "Spooky Piano\nA spooky piano.";
        

    }
     costButton.text = "$" + itemCost.ToString();

    
   
   
    
	
	}
	
    public void TextDelete(){

        desc.text = "";
        
    }
   

	public void TextUpdate(){

   
    desc.text = itemDescription + "\nCost: $" + itemCost.ToString();
    }

    public void UpdateBuy() {
        buyButton.GetComponent<MenuButtons>().ButtonClick();
        if (itemName != null) { 
        buyButton.GetComponent<MenuButtons>().towerType = itemName;
    }

    }


	
}
