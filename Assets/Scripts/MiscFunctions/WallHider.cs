﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WallHider : MonoBehaviour {
	
	//public GameObject wallBox;
	public MeshRenderer[] wallArray;//array where the objects are children of the holder "Inside"
	public bool wallsHidden = false;//changeable boolean
    public GameObject slider;
    

	// Use this for initialization
	void Start () {

		wallArray = GameObject.Find ("Walls").GetComponentsInChildren<MeshRenderer>();//initializes our first array
		//wallBox = GameObject.Find ("WallBox");
	
	}

    public void HideWalls(float alpha)
    {
        slider.GetComponent<Slider>().value = alpha;
    }

	
	
    public void ToggleWalls(){

        wallsHidden = !wallsHidden;//switched boolean from true > false or false > true
        if (wallsHidden)
        {
            HideWalls(0);
        }
        else
        {
            HideWalls(255);
        }
    }
	// Update is called once per frame
	void Update () {
		
		if (Input.GetKeyDown (KeyCode.I)) {//detects for button press
                                           //Debug.Log ("I");
            ToggleWalls();
		}
	}
}
