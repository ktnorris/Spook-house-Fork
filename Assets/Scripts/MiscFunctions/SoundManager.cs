﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 
public class SoundManager : Singleton<SoundManager> {
      public static SoundManager Instance;
     
    public AudioSource m_activeMusic;

    //Music
    public AudioClip Level1Music;

    //Background loop
    public AudioClip windloop;

    //Door SFX
    public AudioClip dooropen;
    public AudioClip doorclose; 

    //Other stuff
    public AudioClip pumpkinplace;
    public AudioClip hardplace;
    public AudioClip elderdie;
    public AudioClip pianoplay;
    public AudioClip coffinshake;
    public AudioClip buttonclick;
    public AudioClip error;
    public AudioClip cashmoney;

    //Ambient SFX
    public AudioClip creakambient;
    public AudioClip laughambient;

    public GameObject cameraListener;

    private bool buttOn;

public class ClipInfo
    {
       //ClipInfo used to maintain default audio source info
       public AudioSource source { get; set; }
       public float defaultVolume { get; set; }
    }
 
  

    public static List<ClipInfo> m_activeAudio;
 
     void Awake(){
    Instance = this;
       m_activeAudio = new List<ClipInfo>();
        Debug.Log("AudioManager Initializing");
        try {
            //change this to the audiolistener object thingy
            transform.parent = cameraListener.transform;
            transform.localPosition = new Vector3(0, 0, 0);
        } catch {
            Debug.Log("Unable to find main camera to put audiomanager");
        }

    PlayMusic (Level1Music, 0.6f);
    PlayLoop(windloop, transform, 0.2f);
    }

    void Update() {
        //calling this function to continually update the list of active sound clips
        updateActiveAudio();
        if (!buttOn){

            StartCoroutine (butts());

        }
    }

    
    public AudioSource PlayMusic(AudioClip music, float volume) {
        m_activeMusic = PlayLoop(music, transform, volume);
        return m_activeMusic;
    }

    public AudioSource Play(AudioClip clip, Vector3 soundOrigin, float volume) {
       //Create an empty game object
       GameObject soundLoc = new GameObject("Audio: " + clip.name);
       soundLoc.transform.position = soundOrigin;
     
       //Create the source
       AudioSource source = soundLoc.AddComponent<AudioSource>();
       setSource(ref source, clip, volume);
       source.Play();
       Destroy(soundLoc, clip.length);
        
       //Set the source as active
       m_activeAudio.Add(new ClipInfo{source = source, defaultVolume = volume});
       return source;
    }

    

    public void setSource(ref AudioSource source, AudioClip clip, float volume) {
       source.rolloffMode = AudioRolloffMode.Logarithmic;
       source.dopplerLevel = 0;
       source.minDistance = 1;
       source.maxDistance = 60;
       source.spatialBlend = 1;
       source.clip = clip;
       source.volume = volume;
    }

    public AudioSource Play2D(AudioClip clip, Vector3 soundOrigin, float volume) {
       //Create an empty game object
       GameObject soundLoc = new GameObject("Audio: " + clip.name);
       soundLoc.transform.position = soundOrigin;
     
       //Create the source
       AudioSource source = soundLoc.AddComponent<AudioSource>();
       setSource(ref source, clip, volume);
        source.spatialBlend = 0;
       source.Play();
       Destroy(soundLoc, clip.length);
        
       //Set the source as active
       m_activeAudio.Add(new ClipInfo{source = source, defaultVolume = volume});
       return source;
    }

    public AudioSource PlayAmbient(AudioClip clip, Vector3 soundOrigin, float volume) {
       //Create an empty game object
       GameObject soundLoc = new GameObject("Audio: " + clip.name);
       soundLoc.transform.position = soundOrigin;
     
       //Create the source
       AudioSource source = soundLoc.AddComponent<AudioSource>();
       setSource(ref source, clip, volume);
        source.spatialBlend = 0;
       source.Play();
       Destroy(soundLoc, clip.length);
        buttOn = false;
        
       //Set the source as active
       m_activeAudio.Add(new ClipInfo{source = source, defaultVolume = volume});
       return source;
    }
    

    public AudioSource PlayLoop(AudioClip loop, Transform emitter, float volume) {
        //Create an empty game object
        GameObject movingSoundLoc = new GameObject("Audio: " + loop.name);
        movingSoundLoc.transform.position = emitter.position;
        movingSoundLoc.transform.parent = emitter;
        //Create the source
        AudioSource source = movingSoundLoc.AddComponent<AudioSource>();
        setSource(ref source, loop, volume);
        source.spatialBlend = 0; //we dont want loops to chane based on camera position
        source.loop = true;
        source.Play();
        //Set the source as active
        m_activeAudio.Add(new ClipInfo{source = source, defaultVolume = volume});
        return source;
    }

    public void stopSound(AudioSource toStop) {
        try {
            Destroy(m_activeAudio.Find(s => s.source == toStop).source.gameObject);
        } catch {
            Debug.Log("Error trying to stop audio source "+toStop);
        }
    }


    private void updateActiveAudio() { 
        var toRemove = new List<ClipInfo>();
        try {
            foreach (var audioClip in m_activeAudio) {
                if (!audioClip.source) {
                    toRemove.Add(audioClip);
                } 
            }
        } catch {
            Debug.Log("Error updating active audio clips");
            return;
        }
        //cleanup
        foreach (var audioClip in toRemove) {
            m_activeAudio.Remove(audioClip);
        }
    }


    IEnumerator butts (){
    
    buttOn = true;
    yield return new WaitForSeconds(Random.Range(10,24));
    int randy = Random.Range(0,2);
    if (randy == 0){
    Debug.Log("AMBIENT NOISES YAY");
    PlayAmbient(creakambient, transform.position, 0.2f);
    buttOn = false;

    }

    else if (randy == 1){
    Debug.Log("AMBIENT NOISES YAY");
    PlayAmbient(laughambient, transform.position, 0.1f);
    buttOn = false;
    

    }

    }

}