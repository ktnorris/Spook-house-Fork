﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialText : MonoBehaviour {

    public Text text1;
    public Text text2;
    public Text text3;
    public Text text4;
    public Text text5;

    public Button nextButton;
 	// Use this for initialization
	void Start () {

    text1.enabled = true;

    if (text2 != null){

    text2.enabled = false;
    }

    if (text3 != null){

    text3.enabled = false;
    }

    if (text4 != null){

    text4.enabled = false;
    }

    if (text5 != null){

    text5.enabled = false;

    }
    
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
 
    public string CycleText(){

        if (text1.enabled == true && text2 != null){
            
            text1.enabled = false;
            text2.enabled = true;  
            return "next";
        }

    else if (text2 != null && text2.enabled == true && text3 != null){
        
            text2.enabled = false;
            text3.enabled = true;   
            return "next";
        }

    else if (text2 != null && text3 != null && text3.enabled == true && text4 != null ){
            text3.enabled = false;
            text4.enabled = true;    
            return "next";
        }

    else if (text3 != null && text4 != null && text5 != null && text4.enabled == true){

            text4.enabled = false;
            text5.enabled = true;    
            return "next";
    }

    
    
    

    else {

        return "nextpanel";

    }


    }
}
