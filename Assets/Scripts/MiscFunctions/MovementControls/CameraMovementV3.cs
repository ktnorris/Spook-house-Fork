﻿using UnityEngine;
using System.Collections;

public class CameraMovementV3 : MonoBehaviour
{
	/// <summary>
    /// The object holding the main camera.
    /// </summary>
	public GameObject camHolder;

    /// <summary>
    /// The speed of the camera. Set in Inspector.
    /// </summary>
	public float moveSpeed;

    /// <summary>
    /// Central point that marks where the camera holder starts in the scene.
    /// </summary>
    public Vector3 camHomeBase;

    /// <summary>
    /// Maximum distance allowed between the camera holder and home base.
    /// </summary>
    public float maxDist;

    // Use this for initialization
    void Start ()
	{
        camHolder = transform.parent.gameObject;
        camHomeBase = camHolder.transform.position;


    }
	
	// Update is called once per frame
	void Update ()
	{

			if (Input.GetKey (KeyCode.W)) {
                    MoveZ(-moveSpeed);
			}

			if (Input.GetKey (KeyCode.S)) {

                    MoveZ(moveSpeed);
			}

			if (Input.GetKey (KeyCode.D)) {

                    MoveX(-moveSpeed);
			}

			if (Input.GetKey (KeyCode.A)) {

                    MoveX (moveSpeed);
		    }

			if (camHolder.transform.localPosition.y < 0 || camHolder.transform.localPosition.y > 0) {
				camHolder.transform.localPosition = new Vector3 (camHolder.transform.localPosition.x, 0, camHolder.transform.localPosition.z);
			}
            /*
			if (camHolder.transform.localPosition.x < -bound) {
				camHolder.transform.localPosition = new Vector3 (-bound, 0, camHolder.transform.localPosition.z);
			}

			if (camHolder.transform.localPosition.x > bound) {
				camHolder.transform.localPosition = new Vector3 (bound, 0, camHolder.transform.localPosition.z);
			}

			if (camHolder.transform.localPosition.z < -bound) {
				camHolder.transform.localPosition = new Vector3 (camHolder.transform.localPosition.x, 0, -bound);
			}

			if (camHolder.transform.localPosition.z > bound) {
				camHolder.transform.localPosition = new Vector3 (camHolder.transform.localPosition.x, 0, bound);
			}
            */
            
	}

	void MoveX (float speed)
	{
        Vector3 tempX = camHolder.transform.position + new Vector3(0.5f * speed * Time.unscaledDeltaTime, 0, 0.5f * speed * Time.unscaledDeltaTime);
        if (Vector3.Distance(tempX, camHomeBase) <= maxDist)
        {
            camHolder.transform.position = tempX;
            //Debug.Log(Vector3.Distance(camHolder.transform.position, camHomeBase));
        }
	}

	void MoveZ (float speed)
	{
        Vector3 tempZ = camHolder.transform.position + new Vector3(-0.5f * speed * Time.unscaledDeltaTime, 0, 0.5f * speed * Time.unscaledDeltaTime);

        if (Vector3.Distance(tempZ, camHomeBase) <= maxDist)
        {
            camHolder.transform.position = tempZ;
        }
    }
}
