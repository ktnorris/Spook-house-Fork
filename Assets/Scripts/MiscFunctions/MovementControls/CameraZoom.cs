﻿using UnityEngine;
using System.Collections;

public class CameraZoom : MonoBehaviour {

    /// <summary>
    /// The scene's main camera.
    /// </summary>
	public Camera mainCam;

    /// <summary>
    /// The child of the main camera that holds the audio listener.
    /// </summary>
    public GameObject audioListener;

    // Use this for initialization
    void Start () {

        mainCam = Camera.main;
        audioListener = GameObject.Find("CameraAudio");
    }
	
	// Update is called once per frame
	void Update () {

        
        if (mainCam.enabled) {
            audioListener.transform.localPosition = new Vector3(0, 0, (9 - mainCam.orthographicSize) * 2);
            if (mainCam.orthographicSize >= 4 && mainCam.orthographicSize <= 9)
            {//Sets "size" (zoom) range between 4 and 9
                mainCam.orthographicSize += Input.GetAxis("Mouse ScrollWheel") * -5f;//changes size based on mouse scrolling

                if (mainCam.orthographicSize < 4)
                {//makes sure size stays in range
                    mainCam.orthographicSize = 4;

                }
                else if (mainCam.orthographicSize > 9)
                {//makes sure size stays in range
                    mainCam.orthographicSize = 9;
                }
            }
			
		}
	}


    public void SetCamera (float zoom){
       
        mainCam.orthographicSize = zoom;

    }
}
