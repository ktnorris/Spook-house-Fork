﻿using UnityEngine;
using System.Collections;

public class TilePlacement : MonoBehaviour
{

	public bool placing;
	public bool reset;
	private GameObject toDisable;
	private Phasing phasscript;

	void Start ()
	{
		phasscript = GameObject.FindGameObjectWithTag ("PathPiece").GetComponent<Phasing> ();
		toDisable = GameObject.Find ("NonGridThings");
	}
	
	// Update is called once per frame
	public void PlaceToggle ()
	{
		if (!phasscript.phasing) {
			if (!reset) {
				if (!placing && !reset) {
					placing = true;
					//toDisable.SetActive (false);
				} else {
					placing = false;
					//toDisable.SetActive (true);
				}
			}
		}
	}

	public void ResetToggle ()
	{
		if (!phasscript.phasing) {
			if (!placing) {
				if (!reset) {
					reset = true;
					//toDisable.SetActive (false);
				} else {
					reset = false;
					//toDisable.SetActive (true);
				}
			}
		}
	}
}
