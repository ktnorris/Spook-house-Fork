﻿using UnityEngine;
using System.Collections;
using Image = UnityEngine.UI.Image;
using Button = UnityEngine.UI.Button;
using SpriteState = UnityEngine.UI.SpriteState;

public class GUIImageToggle : MonoBehaviour {


    private float currState = 0;

	public Sprite[] BaseState;

	public Sprite[] HighlightState;

    public Sprite[] PressedState;

    public Sprite[] DisabledState;

    void Start()
    {
        Toggle(0);
    }

    public void PureToggle()
    {

        currState += 1;
        if(currState <= BaseState.Length - 1)
        {
            Toggle(currState);
        }
        else
        {
            currState = 0;
            Toggle(currState);
        }
    }

	public void Toggle(float state){

		Button button = GetComponent<Button> ();
        
		gameObject.GetComponent<Image> ().sprite = BaseState[(int)state];
		SpriteState spriteState = new SpriteState ();
        if(HighlightState.Length > state)
        {
            spriteState.highlightedSprite = HighlightState[(int)state];
        }

        if (PressedState.Length > state)
        {
            spriteState.pressedSprite = PressedState[(int)state];
        }

        if(DisabledState.Length > state)
        {
            spriteState.disabledSprite = DisabledState[(int)state];
        }
			button.spriteState = spriteState;
	}
}
