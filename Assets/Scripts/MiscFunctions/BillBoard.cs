﻿using UnityEngine;
using System.Collections;

public class BillBoard : MonoBehaviour {

    private Transform lookat;

	// Use this for initialization
	void Start () {
        lookat = Camera.main.transform; 
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(transform.position + lookat.rotation * Vector3.forward,
            lookat.transform.rotation * Vector3.up);

    }
}
