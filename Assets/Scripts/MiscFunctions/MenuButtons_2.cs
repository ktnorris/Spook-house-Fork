﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuButtons_2 : MonoBehaviour {

	public Text moneyText, frightPointText, timeText;
	public GameObject codeHandler, pumpkinPanel, pumpkinIcon, coffinPanel, coffinIcon, pianoPanel, pianoIcon;
	public Spawner spawner;
	public GameObject playPauseButton, FF2Button, ff3button, pauseMenu;
	private GUIImageToggle PPTog, FF2Tog, ff3tog;
	private int timeSpeed; // 0 = paused, 1 = default, 3 = fast, 4 = superfast

	// Use this for initialization
	void Start () {
		codeHandler = GameObject.Find ("CodeHandler");
		spawner = GameObject.Find ("Spawner").GetComponent<Spawner> ();
		pauseMenu.SetActive (false);
		timeSpeed = 1;
		//Time.timeScale = timeSpeed;
		FF2Tog = FF2Button.GetComponent<GUIImageToggle> ();
		ff3tog = ff3button.GetComponent<GUIImageToggle>();
	}
	
	// Update is called once per frame
	void Update () {
		moneyText.text = "" + codeHandler.GetComponent<GameTimeKeeping> ().currentState.Money;
		frightPointText.text = "" + codeHandler.GetComponent<GameTimeKeeping> ().currentState.SoulShards;
		timeText.text = "" + codeHandler.GetComponent<GameTimeKeeping> ().hour;
	}

	public void BuyTower (string towerType)
	{
		spawner.Spawn (towerType);
	}

	public void ShowPauseMenu ()
	{
		pauseMenu.SetActive (true);
		Pause ();
	}

	public void HidePauseMenu ()
	{
		pauseMenu.SetActive (false);
		Resume ();
	}

	public void Pause ()
	{
		Time.timeScale = 0;
	}

	public void Resume ()
	{
		Time.timeScale = 1;
	}

	public void TimeForward (int mult)
	{
		if (mult == 1) {
			timeSpeed = 1;
			Time.timeScale = timeSpeed;
			Debug.Log ("Time scale: " + Time.timeScale);
		} else if (mult == 2) {
			timeSpeed = 2;
			Time.timeScale = timeSpeed;
			Debug.Log ("Time scale: " + Time.timeScale);
			//FF2Tog.Toggle (1);
			//ff3tog.Toggle (0);
		} else if (mult == 3) {
			timeSpeed = 3;
			Time.timeScale = timeSpeed;
			Debug.Log ("Time scale: " + Time.timeScale);
			//FF2Tog.Toggle (0);
			//ff3tog.Toggle (1);
		}
	}

	public void GoToTutorial ()
	{
		SceneManager.LoadScene("MainSceneTutorial");
	}

	public void ExitGame ()
	{
		Application.Quit ();
	}

	public void GoToMainMenu ()
	{
		SceneManager.LoadScene("Main Menu");
	}

	public void RestartLevel ()
	{
		SceneManager.LoadScene("Dakota UI 1_13_2017"); // current scene
	}
}
