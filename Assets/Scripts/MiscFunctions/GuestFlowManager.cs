﻿using UnityEngine;
using System.Collections;

public class GuestFlowManager : MonoBehaviour {

	/// <summary>
	/// An array of the Hour -> Guest spawn ratios. I.E. at hour x, y guests are spawned.
	/// </summary>
	public float[] Hour_Guest;

	/// <summary>
	/// The score handler in this scene instance
	/// </summary>
	private Scoring sHandler;

	// Use this for initialization
	void Start () {
		sHandler = GameObject.FindGameObjectWithTag ("sHandler").GetComponent<Scoring> ();
	}

    /// <summary>
    /// Called at the end of a day cycle updates the Array to match star values.
    /// </summary>
    public void UpdateHour_Guest() {
        float mod = Star_Mod();

        //Debug.Log(mod + "Star Mod");

        if (mod < 1)
        {
            mod += 0.3f;
        }


		for (int x = 0; x < 24; x++) {
			Hour_Guest [x] = (float) Hour_Guest [x] * mod;
		}

		//ClearStars ();
	}


	/// <summary>
	/// Called from UpdateHour_Guest, it determines the modifier to be applied to the Array
	/// </summary>
	float Star_Mod(){

		float toConvert = (float)(sHandler.StarRatingAvg);
		float initMod = (toConvert / 3);
		return initMod;

	}


	/// <summary>
	/// Called from UpdateHour_Guest to clear the cycle's star values.
	/// </summary>
	void ClearStars(){
		sHandler.ResetStars ();

	}
}
