﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ApplicationFunctions : MonoBehaviour
{

	//Use this script for storing miscellaneous stuff that can affect the scene

	public void GQuit ()
	{
		Application.Quit ();
	}


	public void GReset ()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
	}


    public void LevelLoad(int lvl)
    {
        SceneManager.LoadScene(lvl, LoadSceneMode.Single);
    }
}
