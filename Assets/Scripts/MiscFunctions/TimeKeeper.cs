﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using UnityEngine.UI;

public class TimeKeeper : Singleton<TimeKeeper> {
      public static TimeKeeper Instance;


	public int numDays = 1;
	public int hour = 0;
	public Stopwatch time;
	/// <summary>
	/// Timer interval in seconds.
	/// </summary>
	public int timerInterval;

    private int internalTimerInterval;

	/// <summary>
	/// The hour at which a new day begins.
	/// </summary>
	public int cycleEnd;

    public GameObject startTile;

    public GameObject hourAlert;

	// Use this for initialization
	void Start ()
	{
        //finding the StartTile
        startTile = GameObject.Find("StartTile");
        // Set the time interval (in milliseconds)
        internalTimerInterval = timerInterval * 1000;
        // Initializes a new stopwatch and starts it
		//time = new Stopwatch();
        //time.Start();
		StartCoroutine(HourInterval(timerInterval));

        hourAlert.SetActive(false);
	}

    
    void Update(){

        if (hour > 5 && hour < 13){
            if (hourAlert.activeInHierarchy == false){
                hourAlert.SetActive(true);
            }
            hourAlert.transform.GetChild(0).gameObject.SetActive(true) ;
            hourAlert.transform.GetChild(1).gameObject.SetActive(false);

        }
        if (hour == 13){
            hourAlert.SetActive(false);
        }

        if (hour > 20 || hour < 2){
            if (hourAlert.activeInHierarchy == false){
                hourAlert.SetActive(true);
            }
            hourAlert.transform.GetChild(1).gameObject.SetActive(true);
            hourAlert.transform.GetChild(0).gameObject.SetActive(false);   

        }
        if (hour == 2){
            hourAlert.SetActive(false);
        }
    }
    

	/*
	private void OnTimedEvent (System.Object source, System.Timers.ElapsedEventArgs e)
	{//copied from C# API, required
		IncrementHour ();//our method
	}
	*/

	void IncrementHour ()
	{//increases hour by 1


        hour++;

        if (hour == cycleEnd) {
			EndCycle ();
			numDays += 1;
            gameObject.GetComponent<GameManager>().UpdateGameManager(0, 0);
		}

		if ((hour % 24) == 0) {//if hour is >= 24, resets back to 0
			hour = 0;
		}
        
        InitializePhasing();

    }

	public string WhatHour (int anInt)
	{//returns string versions of the current hour

		if (anInt > 23) {
			anInt = anInt % 24;
		}


		if (anInt == 0) {
			return ("12AM");
		} else if (anInt < 12) {
			return (anInt.ToString () + "AM");
		} else if (anInt == 12) {
			return (anInt.ToString () + "PM");
		} else if (anInt > 12) {
			return ((anInt - 12).ToString () + "PM");
		} else {
			return "Error";
		}


	}


	void ShowTime ()
	{

		print(time);

	}

	/*
	// Update is called once per frame
	void Update ()
	{
        if (time.ElapsedMilliseconds >= internalTimerInterval)
        {
            IncrementHour();
            time.Reset();
            time.Start();   
        }
		if (Input.GetKeyDown (KeyCode.H)) {
			WhatHour (hour);
		}
		
	}*/

	IEnumerator HourInterval(float timeGap){
		yield return new WaitForSeconds (timeGap);
		IncrementHour ();
		StartCoroutine (HourInterval(timeGap));
	}


	/// <summary>
	/// Called at the end of the day/night cycle will calculate changes for the next day and apply them.
	/// </summary>
	void EndCycle(){
        print("Ending Cycle");
		gameObject.GetComponent<GuestFlowManager> ().UpdateHour_Guest();
	}

    /// <summary>
    /// Runs a phase from the start tile
    /// is called every hour on the hour
    /// </summary>
    /// <param name="guestCount">The number of guests to be spawned this hour</param>
    public void InitializePhasing()
    {
			Phasing phaseScript = startTile.GetComponent<Phasing> ();
			GuestFlowManager guestFlow = gameObject.GetComponent<GuestFlowManager>();
            //print (hour);
            print(guestFlow);
			int guestCount = Mathf.RoundToInt (guestFlow.Hour_Guest [hour]);
        //print(startTile);
        //print(startTile.GetComponent<Phasing>());
        //print(phaseScript);
            phaseScript.RunPhaseTimed(this.timerInterval, "Linear", guestCount);
			//print ("Start tile inactive");
    }
}
