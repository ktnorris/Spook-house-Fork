﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{

    /// <summary>
    /// To be set in spawn - is the gameobject that we spawn
    /// </summary>
	private GameObject spawnTarget;

	/// <summary>
    /// Spawns a the toSpawn object from the spawnfolder in resources at the position of the spawner
    /// </summary>
    /// <param name="toSpawn">The name of the object in the spawnfolder</param>
    public GameObject Spawn(string toSpawn)
    {
        GameObject spawnTarget = (GameObject)Resources.Load("SpawnFolder/" + toSpawn);
        //Debug.Log(toSpawn);
        //Debug.Log(spawnTarget);
        GameObject butt = Instantiate(spawnTarget, transform.position, spawnTarget.transform.rotation) as GameObject;
        return butt;
    }


}
