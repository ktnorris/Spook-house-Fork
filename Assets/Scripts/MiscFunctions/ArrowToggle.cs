﻿using UnityEngine;
using System.Collections;

public class ArrowToggle : MonoBehaviour {

	private bool doorOpen;

	// Use this for initialization
	void Start () {
		gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		doorOpen = transform.parent.GetComponent<DoorToggle> ().open;
		if (doorOpen) {
			gameObject.SetActive (true);
		} else {
			gameObject.SetActive (false);
		}
	
	}
}
