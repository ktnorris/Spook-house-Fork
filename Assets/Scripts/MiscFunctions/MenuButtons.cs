﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class MenuButtons : MonoBehaviour {

    public GameObject shopUI;
    private bool isUp;

    public Button pumpkinButton;
    private bool towerui;
    public Text clock;
    public TimeKeeper timeKeeper;

	public GameObject PlayPauseButton;
	public GameObject FF2Button;
    public GameObject FF4Button;

	private GUIImageToggle PPTog;
	private GUIImageToggle FF2Tog;
    private GUIImageToggle FF4Tog;

    public Text towerText;

    private int timeSpeed; // 0 = paused, 1 = default, 3 = fast, 4 = superfast

    public string selectedTower;
    
    public GameObject spawner;
    private Towers towervars;
    public GameObject pause;
    
    public string towerType; //type of tower to spawn

	// Use this for initialization
	void Start () {
	    shopUI.gameObject.SetActive(false);
	    timeSpeed = 1;
		PPTog = PlayPauseButton.GetComponent<GUIImageToggle> ();
		FF2Tog = FF2Button.GetComponent<GUIImageToggle> ();
	    FF4Tog = FF4Button.GetComponent<GUIImageToggle>();
		PPTog.Toggle (0);
		FF2Tog.Toggle (0);
	    FF4Tog.Toggle(0);
	    isUp = false;
	    towerui = false;
	    towervars = gameObject.GetComponent<Towers>();
	}
	
	// Update is called once per frame
	void Update () {

    if (clock != null){

    clock.text = timeKeeper.WhatHour(timeKeeper.hour);
    }

		if (isUp) {
			if(Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)){
				//Do some math figure out if it should hide the gui or not
			}
		}

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pausenow();
        }
}


    public void pausenow()
    {
        if (Time.timeScale != 0)
        {
            pause.SetActive(true);
            Time.timeScale = 0;
            gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }
        else
        {
            pause.SetActive(false);
            PlayToggle();
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
        }
    }
/*     public void toggleUI(){
    //Raise and lower the UI at the bottom

    if (isUp == false){
    grassUI.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 80f);
    isUp = true;
    }
    else if (isUp == true){
    grassUI.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
    isUp = false;

    }

} */

    public void NewGame(){

        SceneManager.LoadScene("MainSceneTutorial");

        }

    public void ExitGame(){

        Application.Quit();

    }

    public void ReturnMenu(){
        SceneManager.LoadScene("Main Menu");
    }

    public void RestartLevel(){
        SceneManager.LoadScene("MainScene");

        }
    public void showShop(){
        ButtonClick();
        if (isUp == false){
        shopUI.gameObject.SetActive(true);    
        isUp = true;
    }

    else if (isUp == true){
        shopUI.gameObject.SetActive(false);   
        isUp = false;
    }

}

    public void hideShopForce(){
        shopUI.gameObject.SetActive(false);   
        isUp = false;
        pumpkinButton.interactable = false;

    }


	public void TimeForward(int mult){

            PPTog.Toggle(1);

            if (mult == 2)
            {
                timeSpeed = 2;
                Time.timeScale = timeSpeed * 1.0f;
                FF2Tog.Toggle(1);
                FF4Tog.Toggle(0);
                //change clock fucntionality
                //Debug.Log (timeSpeed);
            }
            if (mult == 4)
            {
                timeSpeed = 4;
                Time.timeScale = timeSpeed * 1.0f;
                FF2Tog.Toggle(0);
                FF4Tog.Toggle(1);
                //Debug.Log(timeSpeed);
        }



	}


    public void PlayToggle(){


            FF2Tog.Toggle(0);
            FF4Tog.Toggle(0);


            ButtonClick();
            timeSpeed = 1;
            Time.timeScale = timeSpeed * 1.0f;
            PPTog.Toggle(0);
            //Debug.Log (timeSpeed);

    }




  

    public void QuitGame(){

            Application.Quit();

    }

   /*  public void ShowOptions(){
        optionsUI.gameObject.SetActive(true);
        //show options
        

        }
    
    public void HideOptions(){
        optionsUI.gameObject.SetActive(false);
    
    } */

   

    public void BuyTower(){
        
         if (towerType != null){
        ButtonClick();
        GameObject butt = spawner.GetComponent<Spawner>().Spawn(towerType);
        butt.GetComponent<AttractionBuy>().shopUI = shopUI;
        butt.GetComponent<AttractionBuy>().pumpkin = pumpkinButton;
        butt.GetComponent<AttractionMaster>().towerName = towerType;
        pumpkinButton.interactable = false;
        showShop();
        
        

    }

}

    public GameObject BuyTowerTutorial(){
        
         if (towerType == "Pumpkin"){
        ButtonClick();
        GameObject butt = spawner.GetComponent<Spawner>().Spawn(towerType);
        butt.GetComponent<AttractionBuy>().shopUI = shopUI;
        butt.GetComponent<AttractionBuy>().pumpkin = pumpkinButton;
        butt.GetComponent<AttractionMaster>().towerName = towerType;
        pumpkinButton.interactable = false;
        shopUI.gameObject.SetActive(false);   
        isUp = false;
        return butt;
        

    }

    else if (towerType == "Spoopy Piano"){
        ButtonClick();
        GameObject butt = spawner.GetComponent<Spawner>().Spawn(towerType);
        butt.GetComponent<AttractionBuy>().shopUI = shopUI;
        butt.GetComponent<AttractionBuy>().pumpkin = pumpkinButton;
        pumpkinButton.interactable = false;
        shopUI.gameObject.SetActive(false);   
        isUp = false;
        return butt;
        

    }

    else{
        return null;
        }
}

    public void ButtonClick(){
        SoundManager.Instance.Play2D(SoundManager.Instance.buttonclick, gameObject.transform.position, 0.5f);
        
    }

    
}
