﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathHandler : MonoBehaviour
{
    public GameObject[] Junctions;

    public int QueryJunctions()
    {
        int flag = 0;
        for(int x = 0; x < Junctions.Length; x++)
        {
            if (Junctions[x].GetComponent<DoorToggleSimple>().open)
            {
                flag += 1;
            }
        }

        return flag;
    }
}
