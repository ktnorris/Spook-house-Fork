﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuIcon : MonoBehaviour {

	public GameObject panel;
	public float secsUntilShowPanel;
	public bool mouseOver, panelShowing;

	// Use this for initialization
	void Start () {
		panel.SetActive (false);
		secsUntilShowPanel = 1.25f;
		mouseOver = false;
	}
	
	// Update is called once per frame
	void Update () {
		//Show the panel after a time delay
		if (mouseOver) {
			secsUntilShowPanel -= Time.deltaTime;
			if (secsUntilShowPanel <= 0) {
				ShowPanel ();
			}
		}
	}

	public void ShowPanel ()
	{
		panel.SetActive (true);
		panelShowing = true;
	}

	public void HidePanel ()
	{
		panel.SetActive (false);
		panelShowing = false;
	}

	//When the mouse enters the icon
	public void MouseEnter ()
	{
		mouseOver = true;
	}

	//When the mouse exits the panel, not the icon
	public void MouseExit ()
	{
		mouseOver = false;
		secsUntilShowPanel = 1.25f;
		HidePanel ();
	}

	//When the mouse exits the icon before the panel shows up
	public void MouseExitIcon ()
	{
		if (!panelShowing) {
			mouseOver = false;
			secsUntilShowPanel = 1.25f;
		}
	}
}
