﻿using UnityEngine;
using System.Collections;

//Attached to ROOMS

public class WallClusterParticleScript : MonoBehaviour {

    /// <summary>
    /// The first room that the wall cluster forms. Assign in inspector.
    /// </summary>
    public GameObject wallCluster1;

    /// <summary>
    /// The second room that the wall cluster forms. Assign in inspector.
    /// </summary>
    public GameObject wallCluster2;

    /// <summary>
    /// The third room that the wall cluster forms. Assign in inspector.
    /// </summary>
    public GameObject wallCluster3;

    /// <summary>
    /// The fourth room that the wall cluster forms. Assign in inspector.
    /// </summary>
    public GameObject wallCluster4;

    /// <summary>
    /// The fifth room that the wall cluster forms. Assign in inspector.
    /// </summary>
    public GameObject wallCluster5;

    /// <summary>
    /// The sixth room that the wall cluster forms. Assign in inspector.
    /// </summary>
    public GameObject wallCluster6;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
