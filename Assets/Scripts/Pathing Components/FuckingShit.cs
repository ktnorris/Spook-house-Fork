﻿using UnityEngine;
using System.Collections;


//This is the original DoorToggle script.
public class FuckingShit : MonoBehaviour {

    public GameObject soundm;

    public bool open;
    public Vector3 RotationPos;
    private UnityEngine.AI.NavMeshObstacle navObstacle;

	// Use this for initialization
	void Start () {
        navObstacle = transform.GetChild(0).GetComponent<UnityEngine.AI.NavMeshObstacle>();
    }
	
    void OnMouseDown()
    {
        if (!open)
        {
            transform.FindChild("Door").Rotate(RotationPos);
            open = true;
            navObstacle.enabled = false;
            SoundManager.Instance.Play(SoundManager.Instance.dooropen, gameObject.transform.position, 1f);
        }
        else
        {
            transform.FindChild("Door").Rotate(new Vector3(0, -RotationPos.y, 0));
            open = false;
            navObstacle.enabled = true;
            SoundManager.Instance.Play(SoundManager.Instance.doorclose, gameObject.transform.position, 1f);
        }
    }
}
