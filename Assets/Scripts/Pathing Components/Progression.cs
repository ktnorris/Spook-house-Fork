﻿using UnityEngine;
using System.Collections;

public class Progression : MonoBehaviour {

    /// <summary>
    /// Whether or not this path is active or inactive
    /// </summary>
    public bool active;

    /// <summary>
    /// Doors to be locked/unlocked
    /// </summary>
    public GameObject[] doorList;

    public float cost;

    /// <summary>
    /// runs at the start the hell you want from me
    /// </summary>
    void Start()
    {
        LockToggle(false);
    }

    /// <summary>
    /// activate/deactivate door toggling.
    /// </summary>
    /// <param name="unlock">Whether or not to unlock it</param>
    void LockToggle(bool unlock)
    { 
        for(int x = 0; x < doorList.Length; x++)
        {
            doorList[x].GetComponent<DoorToggleSimple>().enabled = unlock;
        }
    }


    void OnMouseDown()
    {
        Scoring scoreScript = GameObject.FindGameObjectWithTag("sHandler").GetComponent<Scoring>();
        if(scoreScript.DollaDollaBills > cost)
        {
            scoreScript.AddDollarsandCompute(-cost);
            LockToggle(true);
        }
    }

    void OnMouseOver()
    {
        Scoring scoreScript = GameObject.FindGameObjectWithTag("sHandler").GetComponent<Scoring>();
        if (scoreScript.DollaDollaBills > cost)
        {
            ReColor(Color.green);
        }
        else
        {
            ReColor(Color.red);
        }
    }


    void OnMouseExit()
    {
        ReColor(Color.white);
    }

    void ReColor(Color newColor)
    {
        Renderer[] ourRend = GetComponentsInChildren<Renderer>();
        //Debug.Log("Hit External");
        Color32 c;
        c = newColor;
        //Debug.Log("hit internal");
        for (int x = 0; x < ourRend.Length; x++)
        {
            ourRend[x].material.shader = Shader.Find("Toon/Lit");
            ourRend[x].material.color = c;
        }
    }
}
