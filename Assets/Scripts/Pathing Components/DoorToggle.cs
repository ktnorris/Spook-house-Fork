﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

[Serializable]
public class DoorToggle : MonoBehaviour
{
    public GameObject soundm;
    
    /// <summary>
    /// A bool tracking if the door is open or not
    /// </summary>
	public bool open;
    /// <summary>
    /// How far the door is to be rotated
    /// </summary>
	public Vector3 RotationPos;
    /// <summary>
    /// The navmesh obstacle portion of this door.
    /// </summary>
	private UnityEngine.AI.NavMeshObstacle navObstacle;

    /// <summary>
    /// A reference to the pathing script
    /// </summary>
    private Pathing pathmanager;

    /// <summary>
    /// The List of Entrances to this door.
    /// </summary>
    public List<GameObject> Entrances;

    /// <summary>
    /// The list of potential exits.
    /// </summary>
    public List<GameObject> Exits;

    /// <summary>
    /// The GuiObject to denote cost
    /// </summary>
    public GameObject costText;

    /// <summary>
    /// The GuiObject to denote if the door is locked.
    /// </summary>
    public GameObject lockedText;

    /// <summary>
    /// Whether or not the door can be opened or not
    /// </summary>
    public bool Openable;


    [Serializable]
    public struct TileSet
    {
        public GameObject entrance;
        public List<GameObject> tilePath;
        public float weight;

        public TileSet(GameObject ent, List<GameObject> path, float weight)
        {
            entrance = ent;
            tilePath = path;
            this.weight = weight;

        }
    }

    /// <summary>
    /// The set of tiles that we highlight when displaying a path.
    /// </summary>
    public TileSet[] ourtiles;


    private bool displaying;


    /// <summary>
    /// If this is on the door can never be closed. and doesn't interact with the mouse
    /// </summary>
    public bool permaOpen;

    /// <summary>
    /// Marks the Exit
    /// </summary>
    public bool isExit;

    /// <summary>
    /// The internal dictionary tiles
    /// </summary>
    public Dictionary<GameObject, List<GameObject>> TileSetInternal;

    public float[] costArray;


    private Renderer[] ourRend;

   







	void Start ()
	{
        ourRend = GetComponentsInChildren<Renderer>();
        TileSetInternal = new Dictionary<GameObject, List<GameObject>>();

        pathmanager = GameObject.FindGameObjectWithTag("sHandler").GetComponent<Pathing>();
        for (int x = 0; x < ourtiles.Length; x++)
        {
            //Debug.Log(ourtiles[x].entrance);
           // Debug.Log(ourtiles[x].tilePath);
            TileSetInternal.Add(ourtiles[x].entrance, ourtiles[x].tilePath);
        }

        open = permaOpen;
        if (!permaOpen)
        {
            navObstacle = GetComponentInChildren<UnityEngine.AI.NavMeshObstacle>();
        }

        AccuratePathingWeight();
        //DeclareEntrance();
        StartCoroutine(DeclareDelay());

    }


    private IEnumerator DeclareDelay()
    {
        yield return new WaitForSeconds(1);
        DeclareEntrance();
    }


   


    /// <summary>
    /// 
    /// </summary>
    void OnMouseEnter()
    {
        if (!permaOpen)
        {
            //Debug.Log("Hit External");
            if (GameObject.FindGameObjectWithTag("sHandler").GetComponent<GameManager>().pathState)
        {
                Color32 c;
                if (Openable)
                {
                    c = Color.green;
                }
                else
                {
                    c = Color.red;
                }
            
                //Debug.Log("hit internal");
                for (int x = 0; x < ourRend.Length; x++)
                {
                    ourRend[x].material.shader = Shader.Find("Toon/Lit");
                    ourRend[x].material.color = c;
                }
            }
        }
    }

    /// <summary>
    /// The 
    /// </summary>
    void OnMouseExit()
    {
        if (!permaOpen)
        {
            if (GameObject.FindGameObjectWithTag("sHandler").GetComponent<GameManager>().pathState)
            {
                for (int x = 0; x < ourRend.Length; x++)
                {
                    ourRend[x].material.shader = Shader.Find("Standard");
                    ourRend[x].material.color = Color.white;
                }

            }
        }
    }
    /// <summary>
    /// opens the door during while the pathing state is enabled.
    /// And sets the color of tiles in its list.
    /// </summary>
    void OnMouseDown()
    {
        if (!permaOpen)
        {
            if (GameObject.FindGameObjectWithTag("sHandler").GetComponent<GameManager>().pathState)
            {
                if (!open && PurchaseDoorToggle() && Openable)
                {
                    transform.FindChild("Door").Rotate(RotationPos);
                    open = true;
                    DeclareEntrance();
                    //navObstacle.carving = false;
                    navObstacle.enabled = false;
                    SoundManager.Instance.Play(SoundManager.Instance.dooropen, gameObject.transform.position, 1f);
                   // DisplayImg(costText, false);
                    
                }
                else if (PurchaseDoorToggle() && Openable)
                {
                    transform.FindChild("Door").Rotate(new Vector3(0, -RotationPos.y, 0));
                    open = false;
                    DeclareEntrance();
                    navObstacle.enabled = true;
                    SoundManager.Instance.Play(SoundManager.Instance.doorclose, gameObject.transform.position, 1f);
                    
                }

                if (Openable == false){
                    SoundManager.Instance.Play2D(SoundManager.Instance.error, gameObject.transform.position, 0.7f);
                }
    
                displaying = false;
            }
        }
    }

    /// <summary>
    /// Returns true if you can purchase a door move and removes money for it.
    /// </summary>
    /// <returns></returns>
    bool PurchaseDoorToggle()
    {
        Scoring scoreHandler = GameObject.FindGameObjectWithTag("sHandler").GetComponent<Scoring>();
        
        if(scoreHandler.DollaDollaBills >= pathmanager.openCost && Openable)
        {
            scoreHandler.AddDollarsandCompute(-pathmanager.openCost);
            StartCoroutine(GetComponent<ShowMoney>().TextTrigger(pathmanager.openCost));
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// This method will be called whenever a door can be opened.
    /// </summary>
    public void OpenableState(bool canOpen)
    {
        if (!permaOpen)
        {
            if (GameObject.FindGameObjectWithTag("sHandler").GetComponent<GameManager>().pathState)
            {
                if (canOpen)
                {

                    Openable = true;
                    //DisplayImg(costText, true);


                    //DisplayImg(lockedText, false);

                    Text cost = costText.GetComponentInChildren<Text>();
                    //cost.text = "Cost -" + this.pathmanager.openCost.ToString();


                }
                else
                {

                    Openable = false;
                   // DisplayImg(costText, false);
                    //DisplayImg(lockedText, true);

                }
            }
        }

    }

    /// <summary>
    /// This function will display the object its given.
    /// </summary>
    /// <param name="toDisplay">The object to enable</param>
    /// <param name="display">The object to disable</param>
    /// <returns></returns>
    /*
    GameObject DisplayImg(GameObject toDisplay, bool display)
    {
  
            displaying = true;
            toDisplay.SetActive(display);
            return toDisplay;
    
        
    }*/

    /// <summary>
    /// Declares myself as an entrance if open, or removes myself as entance if closed.
    /// </summary>
    void DeclareEntrance()
    {
        if (Exits.Count > 0)
        {
            if (open)
        {

                this.LockEntrances(true);
                for (int x = 0; x < Exits.Count; x++)
                {
                    DoorToggle curToggle = Exits[x].GetComponent<DoorToggle>();
                    curToggle.Entrances.Add(gameObject);
                    curToggle.OpenableState(true);
                    if (GameObject.FindGameObjectWithTag("sHandler").GetComponent<GameManager>().pathState)
                    {
                        curToggle.UpdateColors(false, false);
                    }
                    curToggle.AccuratePathingWeight();

                    if (curToggle.isExit)
                    {
                        //this.AlertPathingToComplete();
                    }
                }

                
            }
            else
            {

                this.LockEntrances(false);
                for (int x = 0; x < Exits.Count; x++)
                {
                    DoorToggle curToggle = Exits[x].GetComponent<DoorToggle>();

                    if (curToggle.Entrances.Contains(gameObject))
                    {
                        curToggle.Entrances.Remove(gameObject);
                        Pathing pathscript = GameObject.FindGameObjectWithTag("sHandler").GetComponent<Pathing>();

                        if (pathscript.Optimalpath.Contains(gameObject))
                        {
                            //AlertPathingToComplete();
                        }


                       

                    }
                    if (GameObject.FindGameObjectWithTag("sHandler").GetComponent<GameManager>().pathState)
                    {
                        Debug.Log(GameObject.FindGameObjectWithTag("sHandler").GetComponent<GameManager>().pathState + "pathState");
                        curToggle.UpdateColors(true, false);
                        GameObject.FindGameObjectWithTag("sHandler").GetComponent<Pathing>().redrawTiles(true);
                    }
                    curToggle.AccuratePathingWeight();
                    
                    if (curToggle.Entrances.Count < 1)
                    {
                        
                            curToggle.OpenableState(false);
                        
                    }
                    if (curToggle.isExit)
                    {
                        //this.AlertPathingToComplete();
                    }
                }
            }

        }
    }

    /// <summary>
    /// Is called whenever a path is detected to have reached the exit
    /// </summary>
    public void AlertPathingToComplete()
    {
        Pathing pHandler = GameObject.FindGameObjectWithTag("sHandler").GetComponent<Pathing>();
        pHandler.Optimalpath = new List<GameObject>();
        pHandler.pathAggregator(pHandler.exit);
    }

    /// <summary>
    /// Locks all of the entrances so no one can mess with my beautiful paths!
    /// </summary>
    /// <param name="shouldLock"></param>
    public void LockEntrances(bool shouldLock)
    {
        for(int x = 0; x < Entrances.Count; x++)
        {
            Entrances[x].GetComponent<DoorToggle>().OpenableState(!shouldLock);
        }
    }

    /// <summary>
    /// Calculates the set of internal weights for every entrance attached to this exit
    /// </summary>
    public void AccuratePathingWeight()
    {
        this.costArray = new float[Entrances.Count];

        float[] preTotalWeight = CalculatePathCosts();

        for(int x = 0; x < costArray.Length; x++)
        {
            costArray[x] = preTotalWeight[x] + FindMinWeight(Entrances[x].GetComponent<DoorToggle>().RetrieveCostArray());
        }
    }

    /// <summary>
    /// A quick function that iterates through an array of costs and returns the minimum
    /// </summary>
    /// <param name="costs">An array of costs to work through</param>
    /// <returns></returns>
    public float FindMinWeight(float[] costs)
    {
        float curCost = Mathf.Infinity;
        for(int x = 0; x < costs.Length; x++)
        {
            curCost = Mathf.Min(curCost, costs[x]);
        }

        if(curCost == Mathf.Infinity)
        {
            curCost = 0;
        }
        return curCost;
    }

    /// <summary>
    /// Gets the costArray
    /// </summary>
    /// <returns></returns>
    public float[] RetrieveCostArray()
    {
        return this.costArray;
    }


    /// <summary>
    /// Calculates the relatvie cost of the pathing to all of the listed entrances
    /// </summary>
    /// <returns></returns>
    public float[] CalculatePathCosts()
    {
        float[] costs = new float[Entrances.Count];

        for(int x = 0; x < costs.Length; x++)
        {
            costs[x] = Mathf.Abs((transform.position.x - Entrances[x].transform.position.x) + (transform.position.z - Entrances[x].transform.position.z));
        }

        return costs;
    }


    /// <summary>
    /// Calculates the color changes for this instance of doortoggle
    /// </summary>
    /// <param name="reset"></param>
    public void UpdateColors(bool reset, bool optimal)
    {

        if (!reset)
        {
            if (GameObject.FindGameObjectWithTag("sHandler").GetComponent<GameManager>().pathState)
            {
                if (optimal)
                {
                    for (int x = 0; x < Entrances.Count; x++)
                    {
                        ColorPath(Entrances[x], Color.green, Shader.Find("Unlit/Color"));
                    }
                }
                else {
                    for (int x = 0; x < Entrances.Count; x++)
                    {
                        ColorPath(Entrances[x], Color.yellow, Shader.Find("Unlit/Color"));
                    }
                }
            }
        }
        else
        {
            for (int x = 0; x < ourtiles.Length; x++)
            {
                ColorPath(ourtiles[x].entrance, Color.white, Shader.Find("Standard"));
            }
        }
    }


  

 



    /// <summary>
    /// Takes an entrance and color and colors the path assosiated with that gameobject
    /// </summary>
    /// <param name="entrance"></param>
    private void ColorPath(GameObject entrance, Color c, Shader shaderUsed)
    {
        List<GameObject> noColor = new List<GameObject>();
        for(int x = 0; x < Entrances.Count; x++)
        {
            if(Entrances[x] != entrance)
            {
                noColor.Add(Entrances[x]);
            }
        }
        List<GameObject> toColor = this.TileSetInternal[entrance];
        for (int x = 0; x < toColor.Count; x++)
            {
            bool isColorable = true;

            for (int y = 0; y < noColor.Count; y++)
            {
                if (TileSetInternal[noColor[y]].Contains(toColor[x]))
                {
                    isColorable = false;
                }
            }

            if (isColorable)
            {
                toColor[x].GetComponent<Renderer>().material.color = c;
                toColor[x].GetComponent<Renderer>().material.shader = shaderUsed;
            }
        }
    }
    }

