﻿using UnityEngine;
using System.Collections;

public class DoorToggleSimple : MonoBehaviour
{


    //A much simpler door toggle 

    /// <summary>
    /// Tracks whether the door is open or not
    /// </summary>
    public bool open;
    /// <summary>
    /// The position to toggle towards
    /// </summary>
    public Vector3 rotationPos;

    public PathHandler phandle;

    // Use this for initialization
    void Start()
    {
        if (rotationPos == Vector3.zero)
        {
            rotationPos = new Vector3(0, 90, 0);
        }

        phandle = GameObject.FindGameObjectWithTag("pHandler").GetComponent<PathHandler>();
    }

    // Update is called once per frame
    void DoorToggle()
    {
        if (open)
            {
                GetComponentInChildren<UnityEngine.AI.NavMeshObstacle>().transform.Rotate(rotationPos * -1);
                GetComponentInChildren<UnityEngine.AI.NavMeshObstacle>().enabled = true;
                open = false;
            }
            else
            {
                GetComponentInChildren<UnityEngine.AI.NavMeshObstacle>().transform.Rotate(rotationPos);
                GetComponentInChildren<UnityEngine.AI.NavMeshObstacle>().enabled = false;
                open = true;
            }
        }


    void OnMouseDown()
    {
        if(this.enabled)
        {
            if (phandle.QueryJunctions() - 1 >= 1 && open)
            {
                DoorToggle();
            }
            else if(!open)
            {
                DoorToggle();
            }
        }
    }



    /// <summary>
    /// switches the color
    /// </summary>
    void OnMouseEnter()
    {
        if (this.enabled)
        {
            if (phandle.QueryJunctions() - 1 >= 1 && open)
            {
                ReColor(Color.green);
            }
            else if (!open)
            {
                ReColor(Color.green);
            }
            else
            {
                ReColor(Color.red);
            }
        }
    }


    void ReColor(Color newColor)
    {
        Renderer[] ourRend = GetComponentsInChildren<Renderer>();
        //Debug.Log("Hit External");
        Color32 c;
        c = newColor;
        //Debug.Log("hit internal");
        for (int x = 0; x < ourRend.Length; x++)
        {
            ourRend[x].material.shader = Shader.Find("Toon/Lit");
            ourRend[x].material.color = c;
        }
    }


    void OnMouseExit()
    {
        if (this.enabled)
        {
            Renderer[] ourRend = GetComponentsInChildren<Renderer>();
            for (int x = 0; x < ourRend.Length; x++)
            {
                ourRend[x].material.shader = Shader.Find("Standard");
                ourRend[x].material.color = Color.white;
            }

        }
    }
        }
