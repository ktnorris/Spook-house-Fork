﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pathing : MonoBehaviour {

    /// <summary>
    /// The cost of opening a door in dollars
    /// </summary>
    public float openCost;

    /// <summary>
    /// The exit of the house!
    /// </summary>
    public GameObject exit;

    /// <summary>
    /// List of all doors in the scene
    /// </summary>
    public List<GameObject> doorList;

    /// <summary>
    /// The list of entrances that make up the optimal path.
    /// </summary>
    public List<GameObject> Optimalpath;


    private GameObject[] grid;




	// Use this for initialization
	void Start () {
       GameObject[] doorArray = GameObject.FindGameObjectsWithTag("door");

        doorList = new List<GameObject>();
        grid = GameObject.FindGameObjectsWithTag("Tile");

        for(int x = 0; x < doorArray.Length; x++)
        {
            if(doorArray[x].GetComponent<DoorToggle>() != null)
            {
                doorList.Add(doorArray[x]);
            }
        }

    }


    public void redrawTiles(bool pathstate)
    {

        if (pathstate)
        {
            for (int x = 0; x < doorList.Count; x++)
            {
                doorList[x].GetComponent<DoorToggle>().UpdateColors(!pathstate, false);
            }

            if (Optimalpath.Count > 0 && pathstate)
            {
                //DrawOptimalPath(Optimalpath);
            }
        }
        else
        {
            for(int y = 0; y < grid.Length; y++)
            {
                grid[y].GetComponent<Renderer>().material.shader = Shader.Find("Standard");
                grid[y].GetComponent<Renderer>().material.color = Color.white;
            }
        }
    }
    

    public void pathAggregator(GameObject ent)
    { 
//        Debug.Log("Boo");
        DoorToggle thisInstance = ent.GetComponent<DoorToggle>();
        if(thisInstance.Entrances.Count < 1)
        {
            Optimalpath.Add(exit);
            redrawTiles(true);
            return;
        }
        else
        {
            int minCostindex = 0;
            float[] cost = thisInstance.RetrieveCostArray();
            for(int x = 0; x < thisInstance.Entrances.Count; x++)
            {
                if(cost[minCostindex] > cost[x])
                {
                    minCostindex = x;
                }
            }

            Optimalpath.Add(thisInstance.Entrances[minCostindex]);
            pathAggregator(thisInstance.Entrances[minCostindex]);
        }
    }


    private void DrawOptimalPath(List<GameObject> path)
    {
        for(int x = 0; x < path.Count; x++)
        {
            if (gameObject.GetComponent<GameManager>().pathState)
            {
                path[x].GetComponent<DoorToggle>().UpdateColors(false, true);
            }
        }
    }



    
}
