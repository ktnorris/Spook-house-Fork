﻿using UnityEngine;
using System.Collections;

public class BlockBoxes : MonoBehaviour {

	// Use this for initialization
	void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Tile")
        {
            other.gameObject.GetComponent<Tile>().Occupied = true;
        }
    }
}
