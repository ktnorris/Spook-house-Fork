﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContractPanel : MonoBehaviour {

    public Text DescriptionText;
    public Text CharacterText;
    public Image CharacterImg;

    public ContractBase currentContract;

	// Use this for initialization
	void Start () {
		
	}
	
	public void SetInfo(string description, string characterName, Sprite Img)
    {
        DescriptionText.text = description;
        CharacterText.text = characterName;
        CharacterImg.sprite = Img;
    }


    public void Accept()
    {
        currentContract.AcceptContract();
    }

    public void Deny()
    {
        currentContract.DeclineContract();
    }
}
