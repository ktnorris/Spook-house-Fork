﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeButtonHover : MonoBehaviour {

    private TowerPanel tPanel;

    private Text infoPanel;

    private float changeVal;

    private string OriginalString;



    

	// Use this for initialization
	void Start () {
        tPanel = GameObject.FindGameObjectWithTag("TowerPanel").GetComponent<TowerPanel>();
	}

    /// <summary>
    /// This is going to grab the panel that this button should be attached to and sets it for use later.
    /// </summary>
    public void GrabPanelSetVals(float change, Text infoText)
    {
        changeVal = change;
        infoPanel = infoText;
        OriginalString = infoText.text;
    }

    /// <summary>
    /// Shows the change when hovering
    /// </summary>
    public void ShowFutureStat()
    {
        //Debug.Log(changeVal + " Change Value");

        if (infoPanel != null && tPanel.towerTier[0] + tPanel.towerTier[1] + tPanel.towerTier[2] < 3)
        {

            if (changeVal > 0 || changeVal < 0)
            {
                infoPanel.GetComponent<Text>().text = OriginalString + " + " + changeVal.ToString();
                if (changeVal > 0)
                {
                    infoPanel.GetComponent<Text>().color = Color.green;
                }
                else
                {
                    infoPanel.GetComponent<Text>().color = Color.red;
                }
            }
        }
    }

    /// <summary>
    /// Reverts the change after the hover leaves
    /// </summary>
    public void Revert()
    {
        if (infoPanel != null)
        {
            infoPanel.text = OriginalString;
            infoPanel.color = Color.black;
        }
    }

    /// <summary>
    /// What to do while hovering
    /// </summary>
    public void Hovering()
    {
        if (infoPanel != null)
        {
            if (infoPanel.GetComponent<Text>().text != OriginalString)
            {
                OriginalString = infoPanel.GetComponent<Text>().text;
            }

            Revert();
            ShowFutureStat();
        }
    }
}
