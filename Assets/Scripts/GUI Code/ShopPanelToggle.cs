﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopPanelToggle : MonoBehaviour {

    public GameObject[] shopPages;
    public int curPage;

	public void toggle()
    {
        if(curPage+1 == shopPages.Length)
        {
            shopPages[curPage].SetActive(false);
            curPage = 0;
            shopPages[curPage].SetActive(true);
        }else
        {
            shopPages[curPage].SetActive(false);
            curPage++;
            shopPages[curPage].SetActive(true);
        }
    }
}
