﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleanupPanel : MonoBehaviour {

    public GameObject tower;


    public void RemoveBody(float cost)
    {
        if (GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().gScoring.currentState.SoulShards >= cost)
        {
            GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().gScoring.AddSoulShards(-cost);
            Destroy(tower);
        }
    }

    public void EnablePanel(bool show)
    {
        transform.position = Camera.main.WorldToScreenPoint(tower.transform.position);
        transform.parent.GetComponent<Canvas>().enabled = show;
    }
}
