﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomPlacementShuffle : MonoBehaviour {

    public GameObject[] roomDeck;
    public int handSize;

	
	private void displayDeck(GameObject[] roomHand)
    {
        for(int x = 0; x < roomDeck.Length; x++)
        {
            roomDeck[x].SetActive(false);
        }

        for(int y=0; y<roomHand.Length; y++)
        {
            roomHand[y].SetActive(true);
        }
    }


    public void ShuffleDeck()
    {
        GameObject[] hand = new GameObject[handSize];

        for (int x = 0; x < hand.Length; x++)
        {
            int rand = Random.Range(0, roomDeck.Length);
            //Debug.Log(rand);
            hand[x] = roomDeck[rand];
        }

        displayDeck(hand);


    }
}
