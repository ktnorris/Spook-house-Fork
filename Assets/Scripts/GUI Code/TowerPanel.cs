﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class TowerPanel : MonoBehaviour {

    public GameObject tower;

    public float[] towerTier;

    public GameObject[] UpgradeButtons;

   
    /// <summary>
    /// The tiers demonstrations.
    /// </summary>
    [Serializable]
    public struct TierMarkers
    {
        public GameObject tier1;
        public GameObject tier2;
        public GameObject tier3;
    }

    public TierMarkers[] tierTrackers;

    //These are the images to be swapped in and out on the tier markers.
    public Sprite TierMarkerOpen;
    public Sprite TierMarkerTaken;
    public Sprite TierMarkerClosed;

    //----------------------------------------------------------------------------------------------------------------------------------------------------
    //Some Text Boxes for us to edit freely
    //---------------------------------------------------------------------------------------------------------------------------------------------------
    [Serializable]
    public struct infoText
    {
        public Text DamageTextCur;
        public Text DamageCapCur;
        public Text CooldownCur;
        public Text DurationCur;
        public Text RangeCur;
    }

    public infoText towerinfo;
    [Serializable]
    public struct UpgradeText
    {
        public Text Up1;
        public Text Up2;
        public Text Up3;
    }

    public UpgradeText upgradeInfo;
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    //Burn this gui code with fire (does the upgrade display stuff)
    //----------------------------------------------------------------------------------------------------------------------------------------------------


    public void infoPull()
    {
        tower.GetComponent<ActiveUpgrade>().Injector();
        UpdateTiers();
    }

    public void UpdateTiers()
    {

        for (int x = 0; x < tierTrackers.Length; x++)
        {
            tierHelper(tierTrackers[x] , towerTier[x]);
        }
    }

    /// <summary>
    /// A bunch of checks to determine how to display these tiers.
    /// </summary>
    /// <param name="set">The current set of tier markers</param>
    /// <param name="setIdentifier">How many tier markers should be lit up in this branch</param>
    private void tierHelper(TierMarkers set, float setIdentifier)
    {

        float curTier = towerTier[0] + towerTier[1] + towerTier[2];
        //Debug.Log(curTier.ToString() + " - this is the current tier");

        int x = 1;

        switch (x)
        {
            case 1:
                set.tier1.GetComponent<Image>().enabled = true;
                if(setIdentifier >= 1)
                {

                    set.tier1.GetComponent<Image>().sprite = TierMarkerTaken;

                }else if(curTier == 3)
                {
                    set.tier1.GetComponent<Image>().enabled = false;
                }else
                {
                    //Debug.Log("I'm here in case 1");
                    set.tier1.GetComponent<Image>().sprite = TierMarkerOpen;

                }
                goto case 2;
            case 2:
                set.tier2.GetComponent<Image>().enabled = true;
                if (setIdentifier >= 2)
                {

                    set.tier2.GetComponent<Image>().sprite = TierMarkerTaken;

                }
                else if(curTier >= 2)
                {
                    set.tier2.GetComponent<Image>().enabled = false;
                }else
                {
                    //Debug.Log("i'm here in case 2");
                    set.tier2.GetComponent<Image>().sprite = TierMarkerOpen;

                }

                if (curTier > 1 && curTier < 3 && setIdentifier > 0)
                {
                    set.tier2.GetComponent<Image>().enabled = true;
                }
                goto case 3;
            case 3:
                set.tier3.GetComponent<Image>().enabled = true;
                if (setIdentifier == 3)
                {

                    set.tier3.GetComponent<Image>().sprite = TierMarkerTaken;

                }
                else if(curTier >= 1)
                {

                    set.tier3.GetComponent<Image>().enabled = false;
                }else
                {
                    //Debug.Log("I'm here in case 3");
                    set.tier3.GetComponent<Image>().sprite = TierMarkerOpen;

                }

                if (curTier < 2 && setIdentifier > 0)
                {
                    set.tier3.GetComponent<Image>().enabled = true;
                }

                if (curTier < 3 && setIdentifier > 1)
                {
                    set.tier3.GetComponent<Image>().enabled = true;
                }
                break;

        }
    }


    void TierHelperv2(TierMarkers set, float setIdentifier, float leftOverPoints)
    {
        
    }


    //------------------------------------------------------------------------------------------------------------------------------------------------------------
    //Button functionality down here my god 
    //------------------------------------------------------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// The Upgrade buttons that can be clicked
    /// </summary>
    /// <param name="button">The float attached to the button in question</param>
    public void OnButtonClicked(float button)
    {

        
            if (button == 1)
            {
                tower.GetComponent<ActiveUpgrade>().Path = ActiveUpgrade.upPath.Up1;
            }
            else if (button == 2)
            {
                tower.GetComponent<ActiveUpgrade>().Path = ActiveUpgrade.upPath.Up2;
            }
            else if (button == 3)
            {
                tower.GetComponent<ActiveUpgrade>().Path = ActiveUpgrade.upPath.Up3;
            }

            tower.GetComponent<ActiveUpgrade>().Upgrade();
        }

    /// <summary>
    /// Is called by the move button
    /// </summary>
    public void MoveTower()
    {
        hidePanel();
        Vector3 restingPosition = tower.transform.position;
        tower.AddComponent<AttractionPlaceV2>();
        tower.GetComponent<AttractionPlaceV2>().offset = Vector3.one;
        tower.GetComponent<AttractionPlaceV2>().Replacement(restingPosition);
    }

    /// <summary>
    /// Is called by the rotate button
    /// </summary>
    public void RotateTower()
    {
        tower.transform.Rotate(new Vector3(0, 90, 0));
    }

    /// <summary>
    /// Sells the tower back for a fourth of the total money spent on it.
    /// </summary>
    public void SellTower()
    {
        GameScoring scoring = GameObject.FindGameObjectWithTag("Atlas").GetComponent<GameScoring>();

        float total = tower.GetComponent<AttractionBase>().BaseStats.towerCosts.costM;

        for(int x = 0; x < tower.GetComponent<ActiveUpgrade>().upgradeCosts.Length; x++)
        {
            if (towerTier[0] + towerTier[1] + towerTier[2] >= x+1)
            {
                total += tower.GetComponent<ActiveUpgrade>().upgradeCosts[x].costM;
            }
        }

        Debug.Log(total.ToString() + " Sell before reduction");
        total *= .25f;
        Debug.Log(total.ToString() + " Sellback Money");
        scoring.AddMoney(total);

        Destroy(tower);
        hidePanel();
    }

    /// <summary>
    /// Hides the panel to be used by the next tower.
    /// </summary>
    public void hidePanel()
    {
        for (int x = 0; x < tierTrackers.Length; x++)
        {
            tierTrackers[x].tier1.GetComponent<Image>().enabled = false;
            tierTrackers[x].tier2.GetComponent<Image>().enabled = false;
            tierTrackers[x].tier3.GetComponent<Image>().enabled = false;
        }

        for (int y = 0; y < UpgradeButtons.Length; y++)
        {
            UpgradeButtons[y].SetActive(false);
        }

        transform.parent.GetComponent<Canvas>().enabled = false;
    }
}
