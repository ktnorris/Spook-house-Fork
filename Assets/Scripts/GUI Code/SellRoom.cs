﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellRoom : MonoBehaviour {

    public GameObject[] hidelist;

   public void ToggleState()
    {
        GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().SellState = !GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().SellState;
        foreach(GameObject meh in hidelist)
        {
            meh.gameObject.SetActive(!meh.gameObject.activeInHierarchy);
        }
    }
	
}
