﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoredomZ : MonoBehaviour {

	public float startingY, startingX, minX, maxX, maxY, xDirection, leftRightSpeed, upSpeed;

	// Use this for initialization
	void Start () {
		startingY = transform.position.y;
		startingX = transform.position.x;
		minX = startingX - 0.2f;
		maxX = startingX + 0.2f;
		maxY = startingY + 5f;
		xDirection = 0; // 0 = left, 1 = right
		leftRightSpeed = 3;
		upSpeed = 5;
	}
	
	// Update is called once per frame
	void Update () {
		WaverAndRise ();
	}

	public void WaverAndRise ()
	{
		// have the Z rise once it spawns
		if (transform.position.y < maxY) {
			transform.Translate (Vector3.up * Time.deltaTime * upSpeed);
		} else {
			Destroy (gameObject);
		}

		// have the Z waver left to right as it rises
		if (xDirection == 0) {
			if (transform.position.x > minX) {
				transform.Translate (Vector3.left * Time.deltaTime * leftRightSpeed);
			} else {
				xDirection = 1;
			}
		}

		if (xDirection == 1) {
			if (transform.position.x < maxX) {
				transform.Translate (Vector3.right * Time.deltaTime * leftRightSpeed);
			} else {
				xDirection = 0;
			}
		}
	}
}
