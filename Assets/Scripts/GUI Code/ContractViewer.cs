﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

[Serializable]
public class ContractViewer : MonoBehaviour {

    

    public GameObject mainContracts;
    public GameObject sideContracts;
    public GameObject awaitingContracts;

    /// <summary>
    /// Updates the specified group of contracts.
    /// </summary>
    /// <param name="contractList">The new list that we're injecting</param>
    /// <param name="toFill">The list we're updating</param>
    public void UpdateList(List<ContractBase> contractList, GameObject toFill)
    {
        //Debug.Log("I'm getting updated");
        for(int x = 0; x < toFill.transform.childCount; x++)
        {
            if(contractList.Count > x)
            {
                //Debug.Log(contractList.Count);
                //Debug.Log(toFill.transform.GetChild(x));
                toFill.transform.GetChild(x).gameObject.SetActive(true);
                DropdownFill(toFill.transform.GetChild(x).GetComponent<Dropdown>(), contractList[x]);
                toFill.transform.GetChild(x).GetChild(0).gameObject.GetComponent<Text>().text = contractList[x].contractName;
            }

            if(contractList.Count < x)
            {
                toFill.transform.GetChild(x).gameObject.SetActive(false);
            }
        }
    }


    private void DropdownFill(Dropdown toFill, ContractBase contractInfo)
    {
        toFill.options = contractInfo.optionText;
    }

    
}
