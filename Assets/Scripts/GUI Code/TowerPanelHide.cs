﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerPanelHide : MonoBehaviour {
    
    private Atlas.Inputs inputSettings;
    public GameObject TowerPanel;

	// Use this for initialization
	void Start () {
        inputSettings = GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().inputSettings;
	}

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(GetComponent<Canvas>().enabled);
        if (GetComponent<Canvas>().enabled)
        {
            //Debug.Log(inputSettings.cameraLeft);


            if (Input.GetKey(inputSettings.cameraUp) || Input.GetKey(inputSettings.camerDown) || Input.GetKey(inputSettings.cameraRight) || Input.GetKey(inputSettings.cameraLeft))
            {
                TowerPanel.GetComponent<TowerPanel>().hidePanel();
            }

        }
    }
}
