﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomPlacementPanel : MonoBehaviour {

    public Sprite[] roomBlueprints;
    public string[] filePath;

    public GameObject card1;
    public GameObject card2;

    public MenuButtons_2 menu;

    private string call1;
    private string call2;

    public float shuffleCost;

    public void shuffleCards()
    {
        if (canShuffle())
        {
            int randy = (int) Random.Range(0, filePath.Length);
            call1 = filePath[randy];
            card1.GetComponent<Image>().sprite = roomBlueprints[randy];
            randy = (int)Random.Range(0, filePath.Length);
            call2 = filePath[randy];
            card2.GetComponent<Image>().sprite = roomBlueprints[randy];
        }
    }

    public bool canShuffle()
    {
        GameScoring scoring = GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().gScoring;
        if(scoring.currentState.Money >= shuffleCost)
        {
            scoring.AddMoney(-shuffleCost);
            return true;
        }else
        {
            return false;
        }
    }


    public void Card1()
    {
        if (call1 != null)
        {
            menu.BuyTower(call1);
        }
    }

    public void Card2()
    {
        if (call2 != null)
        {
            menu.BuyTower(call2);
        }
    }
	
	
}
