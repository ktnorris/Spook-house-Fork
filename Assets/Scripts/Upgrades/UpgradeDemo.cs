﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpgradeDemo : Upgrade
{

	//Three prefabs that contain the three effects for the upgrades
	//See SpecialEffectTest for reference
    
	public SpecialAbility effectOne;
	public SpecialAbility effectTwo;
	public SpecialAbility effectThree;

	public override void WasClicked ()
	{
    
		
		List<SpecialAbility> effectlist = new List<SpecialAbility> ();
		//Puts the three upgrade effects into a LIST because LIST MASTER RACE
		effectlist.Add (effectOne);
		effectlist.Add (effectTwo);
		effectlist.Add (effectThree);
		GameObject attraction = transform.parent.parent.gameObject;
		if (effectlist.Count > 0) {
			for (int x = 0; x < effectlist.Count; x++) {
				//Prepare the effects to be activated upon upgrade
				SpecialAbility newEffect = effectlist [x];
				SpecialAbility addedEffect = attraction.AddComponent (newEffect.GetType ()) as SpecialAbility;
			
			}

	
		}
	}
}
