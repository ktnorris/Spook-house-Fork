﻿using UnityEngine;
using System.Collections;

public class UpgradeClick : MonoBehaviour
{

	public Component[] scriptCall;

	void OnMouseOver ()
	{
		if (Input.GetMouseButtonDown (0)) {
			Upgrade upgradeScript = (Upgrade)scriptCall [0];
			upgradeScript.WasClicked ();
		}
	}
}
