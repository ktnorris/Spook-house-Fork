﻿using UnityEngine;
using System.Collections;

abstract public class Upgrade : MonoBehaviour
{

	// Use this for initialization
	abstract public void WasClicked ();
}
