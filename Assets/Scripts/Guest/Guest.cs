﻿using UnityEngine;
using System.Collections;

public class Guest : MonoBehaviour
{

	//reminder** the difference between fv and fp
	//fright value constantly rises and falls (towers deal fright value damage)
	//fright points are generated when the guest enters a critical scare or freak out state (very slowly)


	public float frightValue;
	public float frightValueMax;
	public float decayPerSecond;
	public float FrightPoints;
	public bool critFear;
	public bool freakState;
	public bool decaying;
	public Animator animater;
	/// <summary>
	/// The type of the guest, 1 = Child, 2 = Adult, 3 = Elder.
	/// </summary>
	public int guestType;
	public MoveToNavMesh navScript;

	//values for Ray to use
	public float idle;
	public float walking;
	public float run;
    

	// Use this for initialization
	void Start ()
	{
       

		FrightPoints = 0; //Zeroed at initialization

		if (frightValue != 0) {
			frightValue = 0;//Zeroed at initialization
		}

		if (frightValueMax == 0) {
			frightValueMax = 10;//If the fright value max isn't set we set it to 10. Most of the time the max is set in the prefab.
		}

		if (decayPerSecond == 0) {
			decayPerSecond = 1;//If the decay raten is not set we set it to 1 here.
		}

		freakState = false;//Manually sets freak state to false just in case.

		StartCoroutine (DecayDelay ());//Begins the fright value decay loop.

		animater = gameObject.GetComponent<Animator> ();//Locally set the animator for later use



	}

	public void ChangeSpeed (float newSpeed)
	{
		navScript.SpeedSet (newSpeed);
	}

	public void ChangeDest (Transform newGoal)
	{
		navScript.GoalChange (newGoal.position);
	}


	public void ChangeFV (float fright)//this function is called whenever a tower interacts with a guest's fright value
	{

		//ColorChange colorchanger = gameObject.GetComponent<ColorChange> ();//Retrieve's the color change script and stores it locally


		if (frightValue > frightValueMax && !critFear) {//determines the state of the guest i.e. the guest was scared above his max and entered crit fear.
			Debug.Log ("Crit fear reached");
			critFear = true;
			frightValue += fright;
			FrightPoints += 1; //this line and the lines above set all of the values changed by htting crit fear
			//colorchanger.ChangeColor (); //calls the color change to react to the change
		} else if (critFear && frightValue > frightValueMax) {//if the guest is scared and already in the crit fear range then this is triggered
			Debug.Log ("Freaking out!");
			freakState = true;
			FreakEffects ();//guest enters freak out states

		} else if (critFear && frightValue < frightValueMax) {//if they are scared while under crit fright and their fright value is under the fright value max
			if (frightValue + fright > frightValueMax) {//determines if this scare would put them back in the crit fear range
				frightValue += fright; //changes fright value
				FrightPoints += 1; //awards fright point
				//colorchanger.ChangeColor (); //calls for a colorchange
			} else {
				frightValue += fright; //adds in the fright value
				Debug.Log ("reseting crit fear");
				critFear = false; //resets crit fear to false 
				//colorchanger.ChangeColor ();
			}
		} else {//if none of these conditions are met then that means that theres no going over the max value
			frightValue += fright;//adds fright
			//colorchanger.ChangeColor ();//calls color change if applicable
		}
	}


	IEnumerator DecayDelay ()//A continous loop that subtracts from the fright value total.
	{

		yield return new WaitForSeconds (1);
		if (frightValue > 0) {//checks to see if there is any fright value to subtract from
			this.frightValue -= decayPerSecond;//subtracts the decay rate
		}
		decaying = false;
		//print (frightValue);
		StartCoroutine (DecayDelay ());//re-runs the loop

	}


	public void ReportScore ()//called when the guest exits the level to report their stats the entertainment
	{
		Scoring scoreScript = GameObject.FindGameObjectWithTag ("sHandler").GetComponent <Scoring> ();//finds the score script
		EntertainmentMeter emeter = gameObject.GetComponent<EntertainmentMeter> ();//gets the entertainment meter
		int[] scorearray = emeter.MazeExit ();//gets the array of scores from the entertainment meter

		//Debug.Log (scorearray [1] + "value to add to score");
		scoreScript.AddDollarsandCompute (scorearray [1]);//reports the dollar value from the entertainment
		scoreScript.AddFPandCompute (FrightPoints);//Adds the fright points from this guest to the score
		scoreScript.AddStarCount (scorearray [0]);//Adds the star value into the entertainment script.
	}



	public void FreakEffects ()//calculates what kind of guest this particluar instance is and calls freak out scripts.
	{
		Renderer thisRender = gameObject.GetComponent <Renderer> ();
		thisRender.material.color = Color.blue;//sets the color to indicate freaking

		
		if (guestType == 1) {//Checks to see if this guest is a child
			Debug.Log ("I am a child and I am freaking out");
			this.tag = "FreakChild";//sets a tag so that the guest doesn't interact with attraction colliders
			ChildFO freakout = gameObject.GetComponent<ChildFO> ();//gets the freak out script from the guest
			freakout.Trigger ();//triggers the freak out state
		} else if (guestType == 2) {//checks to see if it's adult
			Debug.Log ("I am an adult and I am freaking out");
			//will run stuff here later
		} else if (guestType == 3) {//check to see if this guest is elderly
			this.tag = "DyingElder";//sets the tag to halt attraction interaction
			ElderFO freakOutScript = gameObject.GetComponent<ElderFO> ();//gets freak out script
			freakOutScript.Trigger ();// trigger the freak out effects
			Debug.Log ("I am an elder and I am freaking out");
		} else {
			//for future expansion
		}

	}



	public void FreakReport ()//Sends data to scoring when a guest exits the house and is freaking out
	{
		Scoring scoreScript = (Scoring)GameObject.FindGameObjectWithTag ("sHandler").GetComponent <Scoring> ();//finds the scoring script
		scoreScript.AddDollarsandCompute (0);//sends money NONE
		scoreScript.AddFPandCompute (FrightPoints);//sends over the fright points
		scoreScript.AddStarCount (0);//sends the star count over (0)
	}


   
}
