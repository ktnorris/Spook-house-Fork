﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowMoney : MonoBehaviour {

    /// <summary>
    /// The guest that this script is attached to.
    /// </summary>
    private BaseGuest thisguest;

    /// <summary>
    /// The amount of money to be displayed.
    /// </summary>
    public float moneyAmount;

    /// <summary>
    /// The money to be displayed.
    /// </summary>
    public Text moneyToDisplay;

    /// <summary>
    /// The text's starting color.
    /// </summary>
    Color startColor;

    /// <summary>
    /// The target we want the text to face.
    /// </summary>
    GameObject camTarget;

    /// <summary>
    /// If this boolean is true, changes text color and vertical position over time.
    /// </summary>
    bool goTime = false;

	// Use this for initialization
	public void Start () {

        moneyToDisplay = GetComponentInChildren<Text>();
        startColor = moneyToDisplay.color;
        camTarget = GameObject.FindGameObjectWithTag("MainCamera");

    }

    void TextHandler(float cash)
    {
        goTime = true;
        moneyToDisplay.text = "$" + Mathf.RoundToInt(cash).ToString();//The text field
        
    }

    public IEnumerator TextTrigger(float cash)//Lets us set a time limit for the movement and fade of the money text
    {

        TextReset();
        TextHandler(cash);
        yield return new WaitForSeconds(1.5f);
        goTime = false;
        moneyToDisplay.text = "";
    }

    void TextReset()
    {
        moneyToDisplay.color = startColor;//sets the money text back to the original color
        moneyToDisplay.gameObject.transform.localPosition = new Vector3(0, 0, 0);//sets the money back to the original location
    }
	
	// Update is called once per frame
	void Update () {

        moneyToDisplay.gameObject.transform.LookAt(2 * moneyToDisplay.transform.position - camTarget.transform.position);//causes the Image to always face the camera

        if (goTime)
        {
            moneyToDisplay.color = Color.Lerp(moneyToDisplay.color, new Color(moneyToDisplay.color.r, moneyToDisplay.color.g, moneyToDisplay.color.b, 0), Time.deltaTime * 2);//makes the money text fade out
            moneyToDisplay.gameObject.transform.Translate(Vector3.up * Time.deltaTime * 2);//makes the money text float upwards
        }

    }
}
