﻿using UnityEngine;
using System.Collections;

public class ElderFO : MonoBehaviour
{


	// Update is called once per frame
	public void Trigger ()
	{
		MoveToNavMesh movescript = gameObject.GetComponent<MoveToNavMesh> ();
		movescript.GoalChange (this.transform.position);

		GameObject spawnTarget = (GameObject)Resources.Load ("SpawnFolder/Elder Death Collider");
		GameObject deathcollider = Instantiate (spawnTarget, transform.position, transform.rotation) as GameObject;
		deathcollider.transform.parent = this.transform;
		//GetComponent<NavMeshAgent> ().radius = 0;
		GetComponent<UnityEngine.AI.NavMeshAgent> ().speed = 0;
	
	}
}
