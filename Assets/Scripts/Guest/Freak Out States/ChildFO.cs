﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChildFO : MonoBehaviour
{
	/// <summary>
	/// The navMeshAgent assosiated with this instance of a child
	/// </summary>
	private UnityEngine.AI.NavMeshAgent thisNav;
	/// <summary>
	/// The minimum wait time for changing roaming destinations
	/// </summary>
	public float waitMin;
	/// <summary>
	/// The maximum time between changing roaming positions
	/// </summary>
	public float waitMax;
	/// <summary>
	/// how long a child will track down a guest
	/// </summary>
	public float trackDuration;
	/// <summary>
	/// The range at which an effect will trigger
	/// </summary>
	public float effectRange;
	/// <summary>
	/// The radius that a child can detect and "attack" another guest
	/// </summary>
	public float triggerRadius;
	/// <summary>
	/// The height of the trigger zone that attacks a guest.
	/// </summary>
	public float triggerHeight;


	public void Trigger ()
	{
		//first things first we need to make sure the child stops moving
		thisNav = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ();
		thisNav.SetDestination (this.transform.position);
		//We then switch the tag so that it isn't affected by towers anymore
		gameObject.tag = "FreakChild";
		//We spawn the roaming collider range on top of the guest and set it as the parent of the child
		GameObject roamCollider = Instantiate (Resources.Load ("SpawnFolder/RoamingTowerRadius_NOAttraction"), this.transform.position, this.transform.rotation) as GameObject;
		transform.parent = roamCollider.transform;
		//We then initialize the roaming agent script and add it from this function call
		RoamInit ();
		//We then increase the collider size so that they can hit other guests easier
		CapsuleCollider collider = gameObject.GetComponent<CapsuleCollider> ();
		collider.height = triggerHeight;
		collider.radius = triggerRadius;


	}

	private void RoamInit ()
	{
		gameObject.AddComponent<RoamingTowerAgent> ();
		RoamingTowerAgent roamAgent = gameObject.GetComponent<RoamingTowerAgent> ();
		roamAgent.roamRadius = transform.parent.gameObject;
		roamAgent.waitMin = this.waitMin;
		roamAgent.waitMax = this.waitMax;
		roamAgent.trackDuration = this.trackDuration;
		roamAgent.effectRange = this.effectRange;

	}


	public void ApplyEffects (GameObject guest)
	{
		//first we store what kind of guest this is
		int guestType = guest.GetComponent<Guest> ().guestType;

		//if the guest type is an adult or elder
		if (guestType > 1) {
			//we lose our roam behavoirs
			this.GetComponent<RoamingTowerAgent> ().ForceStop ();
			GameObject roamRadius = GetComponent<RoamingTowerAgent> ().roamRadius;
			Destroy (this.GetComponent<RoamingTowerAgent> ());
			this.transform.parent = null;
			Destroy (roamRadius);

			//we remove their ability to move independently
			//gameObject.GetComponent<MoveToNavMesh> ().enabled = false;
			//thisNav.enabled = false;
			Destroy (gameObject.GetComponent<MoveToNavMesh> ());
			Destroy (thisNav);

			//we set the parent of the transform to the guest we encountered
			transform.parent = guest.transform;

			//now we apply the freak out conditions
			//specifically we stop them from being affected by towers 
			guest.tag = "FreakChild";
		}
			
		//now what happens when its another child instead?
		if (guestType == 1) {
			//We then trigger a freak out from the other child and go back to doing our thing
			//i.e. we keep roaming
			Guest gScript = guest.GetComponent<Guest> ();
            gScript.FrightPoints += 1;
			gScript.FreakEffects ();

		}




	}

}
	
