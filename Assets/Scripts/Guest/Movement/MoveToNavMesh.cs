﻿using UnityEngine;
using System.Collections;

public class MoveToNavMesh : MonoBehaviour
{

	public Transform goal;
	private UnityEngine.AI.NavMeshAgent thisNav;
	public bool DestroyOnExit;

	void Start ()
	{
		//Sets useful variables here
		thisNav = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ();
		//Only things to be applied if the object is a Guest class
		if (gameObject.tag == "guest") {
			GameObject exitTile = GameObject.FindGameObjectWithTag ("Exit");
			goal = exitTile.transform;
			GoalChange (goal.position);
		}
	}

	//Sets a new goal for the nav mesh to find
	public void GoalChange (Vector3 newGoal)
	{
		gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().destination = newGoal;

	}

	//changes the speed of the nav mesh
	public void SpeedSet (float newSpeed)
	{
		thisNav.speed = newSpeed;
	}

	//Mostly just here to delete guests when they hit the exit
	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Exit") {

			//Debug.Log ("HIT");

			if (DestroyOnExit) {
				Guest gscript = this.gameObject.GetComponent<Guest> ();
                if(this.tag == "FreakChild")
                {
                    Guest childScript = null;
                    for (int x = 0; x < transform.childCount; x++)
                    {
                        if (transform.GetChild(x).CompareTag("FreakChild"))
                        {
                            childScript = transform.GetChild(x).GetComponent<Guest>();
                        }
                    }
                    
                    if(childScript != null)
                    {
                        childScript.FreakReport();
                    }
                    
                }
				if (gscript.freakState) {
					gscript.FreakReport ();
				} else {
					gscript.ReportScore ();
				}

				Destroy (gameObject);
			}
		}
	}
}
