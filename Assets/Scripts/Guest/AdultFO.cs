﻿using UnityEngine;
using System.Collections;

public class AdultFO : MonoBehaviour {
    private bool destroy = false;
    private GameObject screamcollider;
	public void Trigger ()
	{
        GameObject spawnTarget = (GameObject)Resources.Load ("SpawnFolder/Adult Scream Collider");
        screamcollider = Instantiate (spawnTarget, transform.position, transform.rotation) as GameObject;
        if (destroy == false){
        StartCoroutine (DestroyScream());
        }

    }

    public IEnumerator DestroyScream(){
    yield return new WaitForSeconds (3);
    Destroy(screamcollider);
    destroy = true;
    }

    //public void Run (){
        //When kid freakout run script done put that in here too 
        
    //}
}
