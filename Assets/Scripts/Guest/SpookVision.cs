﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpookVision : MonoBehaviour
{

    /// <summary>
    /// Bottom most 10th of the sprite.
    /// </summary>
    public GameObject sprite1;

    /// <summary>
    /// 2nd 10th of the sprite.
    /// </summary>
    public GameObject sprite2;

    /// <summary>
    /// 3rd 10th of the sprite.
    /// </summary>
    public GameObject sprite3;

    /// <summary>
    /// 4th 10th of the sprite.
    /// </summary>
    public GameObject sprite4;

    /// <summary>
    /// 5th 10th of the sprite.
    /// </summary>
    public GameObject sprite5;

    /// <summary>
    /// 6th 10th of the sprite.
    /// </summary>
    public GameObject sprite6;

    /// <summary>
    ///7th 10th of the sprite.
    /// </summary>
    public GameObject sprite7;

    /// <summary>
    /// 8th 10th of the sprite.
    /// </summary>
    public GameObject sprite8;

    /// <summary>
    /// 9th 10th of the sprite.
    /// </summary>
    public GameObject sprite9;

    /// <summary>
    /// 10th 10th of the sprite.
    /// </summary>
    public GameObject sprite10;

    public Slider theSlider;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

        if (theSlider.value == 0)
        {
            sprite1.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite2.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite3.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite4.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite5.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite6.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite7.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite8.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite9.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite10.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);

        }

        else if (theSlider.value <= .1)
        {
            sprite1.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

            sprite2.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite3.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite4.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite5.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite6.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite7.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite8.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite9.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite10.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
        }

        else if (theSlider.value <= .2)
        {
            sprite1.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite2.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

            sprite3.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite4.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite5.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite6.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite7.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite8.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite9.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite10.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
        }

        else if (theSlider.value <= .3)
        {
            sprite1.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite2.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite3.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

            sprite4.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite5.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite6.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite7.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite8.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite9.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite10.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
        }

        else if (theSlider.value <= .4)
        {
            sprite1.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite2.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite3.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite4.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

            sprite5.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite6.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite7.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite8.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite9.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite10.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
        }

        else if (theSlider.value <= .5)
        {
            sprite1.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite2.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite3.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite4.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite5.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

            sprite6.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite7.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite8.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite9.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite10.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
        }

        else if (theSlider.value <= .6)
        {
            sprite1.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite2.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite3.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite4.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite5.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite6.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

            sprite7.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite8.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite9.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite10.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
        }

        else if (theSlider.value <= .7)
        {
            sprite1.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite2.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite3.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite4.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite5.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite6.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite7.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

            sprite8.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite9.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite10.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
        }

        else if (theSlider.value <= .8)
        {
            sprite1.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite2.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite3.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite4.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite5.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite6.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite7.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite8.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

            sprite9.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
            sprite10.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.333f);
        }

        else if (theSlider.value > .8)
        {
            sprite1.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite2.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite3.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite4.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite5.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite6.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite7.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite8.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite9.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            sprite10.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

        }
    }
}