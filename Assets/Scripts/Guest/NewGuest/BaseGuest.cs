﻿using UnityEngine;
using System.Collections;

public class BaseGuest : MonoBehaviour {

	/// <summary>
	/// The index of the boredom Z.
	/// Zs come in sets of three,
	/// so if we're on the first
	/// Z this will be zero, if
	/// we're on the second it
	/// will be one, etc.
	/// </summary>
	public int boredomZIndex;

    public float critFloor;

	/// <summary>
	/// The boredom poof particle effect.
	/// </summary>
	public GameObject boredomPoofPE;

	/// <summary>
	/// The "Z" that rises out of guests when bored.
	/// </summary>
	public GameObject boredomZ;

	/// <summary>
	/// The percentage of max fright below which boredom begins
	/// </summary>
	public float boredomThreshold;

    /// <summary>
    /// The current amount of fright on this instance of guest
    /// </summary>
    public float frightValue;

    /// <summary>
    /// The maximum amount of fright a guest can store
    /// </summary>
    public float frightValueMax;

    /// <summary>
    /// The rate that fright decay on a guest per second basis
    /// </summary>
	public float decayRate;

    /// <summary>
    /// The current movement speed attatched to this guest
    /// </summary>
    public float moveSpeed;

    /// <summary>
    /// Whether or not guest is moving
    /// </summary>
    public bool moving;

    /// <summary>
    /// The destination the guest will move toward
    /// </summary>
    public Vector3 destination;

    /// <summary>
    /// Indicates if the guest is in the critical fear state
    /// </summary>
    public bool critState;

    /// <summary>
    /// Indicates if the guest is in a "freak state"
    /// </summary>
    public bool freakState;

	/// <summary>
	/// Indicates if the guest is bored
	/// </summary>
	public bool boredState;

    /// <summary>
    /// The type of the guest 1=child, 2=adult, 3=elder
    /// </summary>
    public int guestType;

    /// <summary>
    /// The number of fright points this guest is holding.
    /// </summary>
    public int frightPoints;

	/// <summary>
	/// If the guest is currently flickering
	/// </summary>
	private bool flicker;

    /// <summary>
    /// The ShowMoney script attached to the guest.
    /// </summary>
    public ShowMoney moneyScript;
    

    /// <summary>
    /// The room that the guest is currently in.
    /// </summary>
    public string roomName;

    /// <summary>
    /// The last room the guest was in.
    /// </summary>
    public string lastRoom;

    /// <summary>
    /// True if the guest is in a room, false if in between rooms.
    /// </summary>
    public bool inRoom;

    /// <summary>
    /// If the guest is in between rooms, this is one of the rooms that the wall cluster is attached to.
    /// </summary>
    public GameObject wallClusterRoom1;

    /// <summary>
    /// If the guest is in between rooms, this is one of the rooms that the wall cluster is attached to.
    /// </summary>
    public GameObject wallClusterRoom2;

    /// <summary>
    /// If the guest is in a doorway, this is the door they are colliding with.
    /// </summary>
    public DoorToggle currentGuestDoor;

	public bool Idling;

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //Misc instantiation and misc functions <- these are the only edits that should be made once everything else is completed.


    protected void Init() {
		GoalChange(FindObjectOfType<GameTimeKeeping>().KillPosition.transform.position);
        StartCoroutine(CheckForIdle(this.transform.position, idleTime));
        StartCoroutine(DecayEngine());
        moneyScript = GetComponent<ShowMoney>();
        roomName = "Foyer";
        inRoom = true;
		frightValue = frightValueMax * 0.5f; // guests start between bored and freaking
		boredomThreshold = 0.2f;
    }

    public void Report()
    {
        int[] emeter = gameObject.GetComponent<EntertainmentMeter>().MazeExit();
        ReportScore(emeter[1], emeter[0]);
        gameObject.GetComponent<EntertainmentMeter>().enabled = false;
    }


    protected void Kill()
    {
        Destroy(gameObject);
    }

    protected void blech() {
        Debug.Log("blerg");
    }


    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //Base Guest functions -> Controls the state of fright on the guest



    /// <summary>
    /// Changes the Fright Value of this instance of guest
    /// </summary>
    /// <param name="frightvalue">Frightvalue - the amount of fright to be changed</param>
    public void ChangeFV(float changeVal)
    {
		if (!freakState) {
			int fState = CheckFrightState (changeVal);
			if (fState == -1) { // bored
				Debug.Log("Bored, leaving");
                ReportScore(0, 0);
				Instantiate (boredomPoofPE, transform.position, Quaternion.identity); // poof particle effect
				Destroy(gameObject); // guest leaves (poofs out of) house when bored
			}

			if (fState == 1) {
				frightValue += changeVal;
			} else if (fState == 2) {
				frightValue += changeVal;



            } else if (fState == 3) {
                GameScoring scoreScript = GameObject.FindGameObjectWithTag("Atlas").GetComponent<Atlas>().gScoring;
                //scoreScript.AddSoulShards(-1);
                scoreScript.AddFreakOut(1);
			}
		}
    }

    /// <summary>
    /// Updates the fright state and outputs a number documenting the new state.
    /// </summary>
    /// <returns>The an Integer representing the fright state.</returns>
    /// <param name="changedFright">The amount of fright that will change on this addition.</param>
    protected int CheckFrightState(float changedFright)
    {

        float newFrightVal = changedFright + frightValue;

		if (newFrightVal <= 0) { // bored
			return -1;
		}
        else if (newFrightVal < (frightValueMax * critFloor))
        {
            critState = false;
            return 1;
        }
        else if (newFrightVal >= (frightValueMax * critFloor) && newFrightVal <= frightValueMax)
        {
            if (critState == false)
            {
                critState = true;
                StartCoroutine(GetComponent<EntertainmentMeter>().InvokeSoulShards(1));
            }
                return 2;
        }
        else if (newFrightVal > frightValueMax)
        {
            freakState = true;
            critState = false;
            return 3;
        }
		else
        {
            return 0;
        }
    }


    /// <summary>
    /// A simple coroutine that subtracts the decay rate every second
    /// </summary>
    /// <returns>The engine.</returns>
    protected virtual IEnumerator DecayEngine() {

        yield return new WaitForSeconds(1);
        if (frightValue - decayRate < 0)
        {
            ChangeFV(-frightValue);
        }
        else {
			if (frightValue <= frightValueMax * boredomThreshold) {
				if (boredState == false) { // if the guest becomes bored this frame start making Zs
					InvokeRepeating ("BoredomZs", 0, 1f);
				}
				boredState = true;
				ChangeFV (-decayRate / 2); // decay at half rate if almost bored
			} else {
				boredState = false;
				CancelInvoke ("BoredomZs");
				ChangeFV (-decayRate);
			}
        }
        StartCoroutine(DecayEngine());
    }

    /// <summary>
    /// Reports the amount of money entered to the scoring script.
    /// </summary>
    /// <param name="money">The amount of money being reported</param>
    public void ReportScore(float money) {

        GameScoring scoreScript = GameObject.FindGameObjectWithTag("Atlas").GetComponent<GameScoring>();//finds the score script
        scoreScript.AddMoney(money);
        moneyScript.moneyAmount = money;
        StartCoroutine(moneyScript.TextTrigger(money));
        //SoundManager.Instance.Play2D(SoundManager.Instance.cashmoney, gameObject.transform.position, 0.3f);
    }

    /// <summary>
    /// Reports the money and star value entered to the scoring script
    /// </summary>
    /// <param name="money">the money amount</param>
    /// <param name="starVal">the star value amount called</param>
    public void ReportScore(float money, int starVal)
    {
        GameScoring scoreScript = GameObject.FindGameObjectWithTag("Atlas").GetComponent<GameScoring>();//finds the score script
        ReportScore(money);
        scoreScript.AddReputation(starVal);
    }

    /// <summary>
    /// Causes the guest shaders to flicker on and off.
    /// </summary>
    /// <returns></returns>
	protected IEnumerator DamageFlicker(){
		if (!flicker) {
			flicker = true;
			Renderer[] toFlick = gameObject.GetComponentsInChildren<Renderer> ();
			DamageFlickerAid (toFlick, "FX/Flare");
			yield return new WaitForSeconds (.1f);
			DamageFlickerAid (toFlick, "Standard");
			yield return new WaitForSeconds (.1f);
			DamageFlickerAid (toFlick, "FX/Flare");
			yield return new WaitForSeconds (.1f);
			DamageFlickerAid (toFlick, "Standard");
			yield return new WaitForSeconds (.1f);
			DamageFlickerAid (toFlick, "FX/Flare");
			yield return new WaitForSeconds (.1f);
			DamageFlickerAid (toFlick, "Standard");
			flicker = false;

		}
	}


	void DamageFlickerAid(Renderer[] toSwitch, string shaderType){
		for (int x = 0; x < toSwitch.Length; x++) {
            if (toSwitch[x].GetComponent<ParticleSystem>() == null)
            {
                toSwitch[x].material.shader = Shader.Find(shaderType);
            }
		}
	}

    public IEnumerator KillTimer(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        KillForNoGoodReason();
    }


    public void KillForNoGoodReason()
    {
           Instantiate(boredomPoofPE, transform.position, Quaternion.identity); // poof particle effect
            Destroy(gameObject); // guest leaves (poofs out of) house when bored
    }


    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //Movment functionality

    /// <summary>
    /// Sets a new destination and triggers the calculation of a new path.
    /// </summary>
    /// <param name="newDest">A new Vector3 that acts as a destination.</param>
    public void GoalChange(Vector3 newDest) {
        this.gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(newDest);
        UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
       // Debug.Log( gameObject.GetComponent<NavMeshAgent>().CalculatePath(newDest, path));
        this.destination = newDest;
    }

    /// <summary>
    /// Changes the speed of the NavMesh
    /// </summary>
    /// <param name="newSpeed">New speed.</param>
    public void SpeedChange(float newSpeed) {
        this.gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().speed = newSpeed;
        moveSpeed = newSpeed;
        if (moveSpeed != 0){
            moving = true; //FLAG might have to change this
        }
    }


    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //Pathing State Machine 

    /*Heres a quick rundown of the current state machine
    All updates should be held here

    1. The machine has three states, two internal one external
        a. "Walking" state - This is when the guest is navigating towards the destination
        b. "Idle" state - This is a debug state, if a Guest ever can no longer move it forces it to recalculate its path.
        c. "Freak" state - This is set externally and forcibly stops the state machine making room for freak out effects to take place.

    2. The machine does not hardcode these states rather it uses methods to make the transition so that theres no accidentally variable setting
        a. CheckForIdle indicates the walking state
        b. StateReset indicates the idle state
        c. neither of these will operate if FreakState is true;
    */

    public float idleTime;


    
    /// <summary>
    /// A coroutine that compares the guest position to its position checkRate seconds ago.
    /// It can "break" and enter an Idle state if the positions are the same.
    /// Or it continues the check with another co-routine.
    /// </summary>
    /// <param name="oldPos">The position we use to compare to the current position.</param>
    /// <param name="checkRate">The number of seconds in-between checks.</param>
    /// <returns></returns>
    IEnumerator CheckForIdle(Vector3 oldPos, float checkRate)
    {
        yield return new WaitForSeconds(checkRate);
        if (Vector3.Distance(oldPos, this.transform.position) <= 1)
        {
            //Breaks state and begins idle state changes
            StateReset();
        }
        else
        {
            //Continues to run the checkforidle co-routine.
            StartCoroutine(CheckForIdle(this.transform.position, checkRate));

        }
    }

    /// <summary>
    /// Resets the guest's destination forcing it to re-calculate its pathing.
    /// Entered by the "Idle" state and returns to the "Walking" state.
    /// </summary>
    void StateReset()
    {
		//Idling = true;
		
		if (gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().pathStatus != UnityEngine.AI.NavMeshPathStatus.PathComplete) {
			//Debug.Log ("RESET");
			UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath ();
			//Debug.Log (gameObject.GetComponent<NavMeshAgent> ().CalculatePath (destination, path));
			this.GoalChange (GameObject.FindGameObjectWithTag("Atlas").GetComponent<GameTimeKeeping>().KillPosition.transform.position);

		}

		StartCoroutine (CheckForIdle (this.transform.position, idleTime));

    }

	public void BoredomZs()
	{
		InvokeRepeating ("BoredomZ", 0, 0.2f);
	}

	public void BoredomZ()
	{
		if (boredomZIndex < 3) {
			// create a "Z" with this object as its parent
			Instantiate (boredomZ, transform.position + (Vector3.up * 2), Quaternion.identity, transform);
			boredomZIndex++;
		} else {
			boredomZIndex = 0;
			CancelInvoke ("BoredomZ");
		}
	}
}
