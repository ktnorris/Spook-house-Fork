﻿using UnityEngine;
using System.Collections;

public class ChildGuest : BaseGuest {

	public bool hasFreaked;

	// Use this for initialization
	void Start () {
		Init ();
		SpeedChange (this.moveSpeed);
		if (frightValueMax == 0) {
			frightValueMax = 8;
		}
		guestType = 1;
	}

	// Update is called once per frame
	void Update () {
		if (freakState && !hasFreaked) {
			FreakoutTrigger ();
			hasFreaked = true;
		}
	}



	//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//Ripped from Roaming Tower Agent

	/// <summary>
	/// The gameobject representing the roaming radius
	/// </summary>
	public GameObject roamRadius;
	/// <summary>
	/// The roamingTower radius script on the roaming radius
	/// </summary>
	private RoamingTowerRadius roamScript;
	/// <summary>
	/// The minimum possible random wait time
	/// </summary>
	public float waitMin;
	/// <summary>
	/// The maximum possible random wait time
	/// </summary>
	public float waitMax;
	/// <summary>
	/// A 3d vector that represents the destination point
	/// </summary>
	private Vector3 dest;
	/// <summary>
	/// The current random wait time
	/// </summary>
	private float randWait;
	/// <summary>
	/// how long a agent will track a guest for
	/// </summary>
	public float trackDuration;
	/// <summary>
	/// The range at which an agent will be triggered to act
	/// </summary>
	public float effectRange;

    /// <summary>
    /// The speed multiplier applied upon freak out
    /// </summary>
    public float moveMultiplier;

	public void FreakoutTrigger() {
		GoalChange(this.transform.position);
		//We then switch the tag so that it isn't affected by towers anymore
		gameObject.tag = "FreakChild";
        SpeedChange(moveSpeed * 3);
		//We spawn the roaming collider range on top of the guest and set it as the parent of the chil
		roamRadius = Instantiate (Resources.Load ("SpawnFolder/RoamingTowerRadius_NOAttraction"), this.transform.position, this.transform.rotation) as GameObject;
		transform.parent = roamRadius.transform;
		roamScript = roamRadius.GetComponent<RoamingTowerRadius> ();
		RoamMechanics ();
	}


	//Creates a random destination and wait time and then has the roam class execute it
	void RoamMechanics ()
	{
		RoamingTowerRadius parScript = roamRadius.GetComponent<RoamingTowerRadius> ();
		dest = parScript.GenerateNewPosition ();
		randWait = Random.Range (waitMin, waitMax);
		StartCoroutine (RoamTime ());
	}

	//When the destination is acutally changed and waited on.
	IEnumerator RoamTime ()
	{
		GoalChange (dest);
		yield return new WaitForSeconds (randWait);
		RoamMechanics ();

	}

	//When the trigger radius detects a guest this will be called instead
	//Roam class will track to an area and trigger an effect.
	public void TrackMechanics (GameObject guest)
	{
        Debug.Log("Tracking begin");
		StopAllCoroutines ();
		SpeedChange (gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().speed * moveMultiplier);
		GoalChange (guest.transform.position);
        StartCoroutine(TrackTime());

	}

    IEnumerator TrackTime()
    {
        Debug.Log("TrackTimeStart");
        yield return new WaitForSeconds(trackDuration);
        roamScript.tracking = false;
        RoamMechanics();
    }

	void OnTriggerEnter (Collider other)
	{
		if (freakState) {
			if (other.tag == "guest") {
				if (roamScript.tracking) {
					//trigger an effect
					Debug.Log ("boooo");
					roamScript.tracking = false;
					ApplyEffects (other.gameObject);
				}
				if (other.gameObject.GetComponent<BaseGuest> ().guestType == 1) {
					SpeedChange (gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().speed / moveMultiplier);
					RoamMechanics ();
				} else {
					Debug.Log ("Huh what was that");
				}
			}
		} else if (other.tag == "Exit") {
            Kill();
		}
	}


	void ApplyEffects(GameObject guest)	{
		//first we store what kind of guest this is
		int guestType = guest.GetComponent<BaseGuest> ().guestType;

		//if the guest type is an adult or elder
		if (guestType > 1) {
			//we lose our roam behavoirs
			this.StopAllCoroutines();
			this.transform.parent = null;
			Destroy (roamRadius);

			//we remove their ability to move independently
			//gameObject.GetComponent<MoveToNavMesh> ().enabled = false;
			//thisNav.enabled = false;
			Destroy (gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>());

			//we set the parent of the transform to the guest we encountered
			transform.parent = guest.transform;

			//now we apply the freak out conditions
			//specifically we stop them from being affected by towers 
			guest.tag = "FreakChild";
            freakState = false;
		}

		//now what happens when its another child instead?
		if (guestType == 1) {
			//We then trigger a freak out from the other child and go back to doing our thing
			//i.e. we keep roaming
			ChildGuest gScript = guest.GetComponent<ChildGuest> ();
			gScript.frightPoints += 1;
			gScript.FreakoutTrigger ();

		}




	}


}


