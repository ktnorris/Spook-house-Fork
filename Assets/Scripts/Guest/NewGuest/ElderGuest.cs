﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class ElderGuest : BaseGuest {

	/// <summary>
	/// The amount of DPS elders do on death
	/// </summary>
	public float[] FreakDPS;

	/// <summary>
	/// The cap of elder damage if there so be one
	/// </summary>
	public float[] FreakCAP;

	/// <summary>
	/// The size of the collider in units
	/// </summary>
	public float range;

    /// <summary>
    /// Delay on the freak effects occuring (will be used for animation to finish up)
    /// </summary>
    public float delay;

    public Animator ourAnim;

	private bool hasFreaked;


    public LayerMask toPass;

	// Use this for initialization
	void Start () {
		base.Init ();
		SpeedChange (this.moveSpeed);
		if (frightValueMax == 0) {
			frightValueMax = 12;
		}
        ourAnim.SetBool("Walking", true);
		guestType = 3;
		boredState = false;
		boredomZIndex = 0;
	}

	// Update is called once per frame
	void Update () {
		if (freakState && !hasFreaked) {
			FreakoutTrigger ();
			hasFreaked = true;
		}
	}

    /// <summary>
    /// All of the scene changes that take place to make the elder into a tower
    /// </summary>
	public void FreakoutTrigger() {
        ourAnim.SetBool("Walking", false);
        ourAnim.SetBool("Freak", true);
		this.tag = "DyingElder";
		GoalChange (this.transform.position);
		SpeedChange (0);
        this.GetComponent<Rigidbody>().useGravity = true;
        StartCoroutine(FODelay());
        //GameObject range = Resources.Load("SpawnFolder/RangeSystem 1") as GameObject;
        //GameObject copy = Instantiate(range);
        //copy.transform.SetParent(transform);
        //copy.transform.localPosition = Vector3.zero;
        //copy.transform.localScale = Vector3.one;
        gameObject.AddComponent<DeadElder>();

    }

    /// <summary>
    /// Initializes the tower stats once the guest "dies"
    /// </summary>
    /// <param name="towerScript">The attracion script to initialize</param>
	private void InitTowerStats(DeadElder towerScript){
        //this.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
        gameObject.GetComponent<BoxCollider>().size = new Vector3(3, 3, 3);
        towerScript.BaseStats.affectMask = toPass;
		towerScript.BaseStats.damage = FreakDPS;
		towerScript.BaseStats.damageCap = FreakCAP;
        towerScript.BaseStats.range = range;
        towerScript.Init();
        Destroy(GetComponent<NavMeshAgent>());
        Destroy(gameObject.GetComponent<EntertainmentMeter>());
        Destroy(this);
        

	}

    /// <summary>
    /// A delay on the tower activation.
    /// </summary>
    /// <returns></returns>
    IEnumerator FODelay()
    {
        yield return new WaitForSeconds(delay);

        StopAllCoroutines();
        InitTowerStats(gameObject.GetComponent<DeadElder>());

        //SoundManager.Instance.Play(SoundManager.Instance.elderdie, gameObject.transform.position, 1f);
    }


    /// <summary>
    /// OnTrigger effects that change the guest state.
    /// </summary>
    /// <param name="other">Any colliders we come into contact with</param>
	public void OnTriggerEnter(Collider other){
        if (!freakState)
        {
            if (other.tag == "Kill")
            {
                Kill();
            }
        }

        if (other.tag == "room")
        {
            lastRoom = roomName;
            roomName = other.gameObject.name;
            currentGuestDoor = null;
            inRoom = true;
        }

        if (other.tag == "wallCluster")
        {
            if (other.gameObject.GetComponent<WallClusterScript>().hasDoor)
            {
                inRoom = false;
                currentGuestDoor = other.gameObject.GetComponent<WallClusterScript>().thisClusterDoor;
                wallClusterRoom1 = other.gameObject.GetComponent<WallClusterScript>().room1;
                wallClusterRoom2 = other.gameObject.GetComponent<WallClusterScript>().room2;
            }
        }
    }

}
