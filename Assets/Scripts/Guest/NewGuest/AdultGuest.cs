﻿using UnityEngine;
using System.Collections;

public class AdultGuest : BaseGuest {

	/// <summary>
	/// The new speed when in flight mode
	/// </summary>
	public float flightSpeed;

	/// <summary>
	/// The size of the collider which can detect towers to damage.
	/// </summary>
	public float towerDetectSize;

	/// <summary>
	/// If the guest is willing to fight or not.
	/// </summary>
	private bool fightState;

	public bool hasFreaked;

	// Use this for initialization
	void Start () {
		Init ();
		SpeedChange (this.moveSpeed);
		if (frightValueMax == 0) {
			frightValueMax = 10;
		}
		guestType = 2;
	}
	
	// Update is called once per frame
	void Update () {
		if (freakState && !hasFreaked) {
			FreakoutTrigger ();
			hasFreaked = true;
		}
	}

	/// <summary>
	/// Called by the update when a change has been detected by the freak out state. Launches either flight or fight reaction
	/// </summary>
	public void FreakoutTrigger() {
		this.tag = "FreakAdult";
		float rand = Random.Range (1, 3);

		if (rand == 1) {
			Flight ();
		} else {
			Fight ();
		}
	}

	/// <summary>
	/// The flight behavoir speeds up the movement of the guest to simulate running.
	/// </summary>
	private void Flight (){
		this.SpeedChange (flightSpeed);
	}

	/// <summary>
	/// This triggers the guest to "fight" or damage a tower.
	/// It spawns a collider, and damages a tower within the collider making them inoperable.
	/// </summary>
	private void Fight(){
		Debug.Log ("Fighting");

		//First we add a collider and initialize it's values.
		BoxCollider towerDetect = gameObject.AddComponent<BoxCollider> ();
		towerDetect.size = new Vector3 (towerDetectSize, towerDetectSize, towerDetectSize);
		towerDetect.isTrigger = true;

		//Next we make it capable of triggering a fight effect from the trigger collider.
		fightState = true;
	}


	protected void OnTriggerEnter(Collider other){
		if (other.tag == "Exit") {
            Kill();
		}

		if (other.GetComponent<AttractionMaster> () != null && fightState) {
			other.GetComponent<AttractionMaster> ().damaged = true;
			fightState = false;
			Flight ();
		}
}
}
