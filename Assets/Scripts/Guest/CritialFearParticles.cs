﻿using UnityEngine;
using System.Collections;

public class CritialFearParticles : MonoBehaviour {

    /// <summary>
    /// The target that we want the particles to end at.
    /// </summary>
    public GameObject particleTarget;

    /// <summary>
    /// The direction that we want the particles to follow.
    /// </summary>
    private Vector3 targetDir;

    /// <summary>
    /// The guest that this particle system is attached to.
    /// </summary>
    public ElderGuest thisGuest;

    /// <summary>
    /// This particle system.
    /// </summary>
    private ParticleSystem thisPartSys;

    /// <summary>
    /// 
    /// </summary>
    private ParticleSystem.Particle[] myParts;


    /// <summary>
    /// The number of live particles.
    /// </summary>
    private int numPartsAlive;

    /// <summary>
    /// Variable that helps control how fast the particles move.
    /// </summary>
    public float myDrift = 0.25f;

    public float dist;

    // Use this for initialization
    void Start () {

        thisGuest = GetComponentInParent<ElderGuest>();
        thisPartSys = GetComponent<ParticleSystem>();
        particleTarget = GameObject.Find("ParticleTarget");

	}
	
	// Update is called once per frame
	void LateUpdate () {

        targetDir = particleTarget.transform.position - thisGuest.gameObject.transform.position;
        dist = Vector3.Distance(thisGuest.gameObject.transform.position, particleTarget.transform.position);

        InitializeIfNeeded();

        numPartsAlive = thisPartSys.GetParticles(myParts);

        for (int i = 0; i < numPartsAlive; i++)
        {
            myParts[i].velocity += targetDir * myDrift;
        }

        thisPartSys.SetParticles(myParts, numPartsAlive);

    }

    void InitializeIfNeeded()
    {
        if (thisPartSys == null)
            thisPartSys = GetComponent<ParticleSystem>();

        if (myParts == null || myParts.Length < thisPartSys.maxParticles)
            myParts = new ParticleSystem.Particle[thisPartSys.maxParticles];
    }
}
