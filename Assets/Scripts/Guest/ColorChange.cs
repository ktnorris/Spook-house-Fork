﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColorChange : MonoBehaviour
{
	/// <summary>
	/// The skull alpha when bored (zero)
	/// </summary>
	public int boredAlpha;

    /// <summary>
    /// The sprite renderer attached to this guest.
    /// </summary>
    public SpriteRenderer thisRend;

    /// <summary>
    /// The Elder Guest script that the skull represents.
    /// </summary>
	public BaseGuest thisGuest;

    /// <summary>
    /// Bored skull. Assigned in inspector.
    /// </summary>
	public Sprite sprite1;

    /// <summary>
    /// Wary skull. Assigned in inspector.
    /// </summary>
	public Sprite sprite2;

    /// <summary>
    /// Startled skull. Assigned in inspector.
    /// </summary>
	public Sprite sprite3;

    /// <summary>
    /// Scared skull. Assigned in inspector.
    /// </summary>
	public Sprite sprite4;

    /// <summary>
    /// Terrified skull. Assigned in inspector.
    /// </summary>
	public Sprite sprite5;

    /// <summary>
    /// Critial animation. Assigned in inspector.
    /// </summary>
	public Animator critAnim;

    /// <summary>
    /// RIP skull. Assigned in inspector.
    /// </summary>
	public Animator ripAnim;

    /// <summary>
    /// A target behind the camera. Looking directly at the camera looks a bit off.
    /// </summary>
    public GameObject camTarget;

    /// <summary>
    /// Float representing the percentage of guest's FV/FVMax. Between 0 and 1.
    /// </summary>
    public float guestFright;

    /// <summary>
    /// The color of the skull.
    /// </summary>
    public Color skullColor;

    private bool fakeBool;

    public bool critState2;

    public bool freakState2;

	// Use this for initialization
	void Start ()
	{
		boredAlpha = 0;
        thisRend = GetComponent<SpriteRenderer>();
        camTarget = GameObject.FindGameObjectWithTag("MainCamera");
        thisGuest = GetComponentInParent<BaseGuest>();
        //critAnim = gameObject.GetComponent<Animator>();
        //ripAnim = gameObject.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
	{
        guestFright = thisGuest.frightValue / thisGuest.frightValueMax;
        thisRend.transform.LookAt(camTarget.transform);
        thisRend.color = skullColor;
        freakState2 = thisGuest.freakState;

        if (guestFright < 0.2f)
        {
            //critAnim.gameObject.SetActive(false);
            thisRend.enabled = true;
            thisRend.sprite = sprite1;
			skullColor = Color.Lerp(new Color((173.0f / 255.0f), (133.0f / 255.0f), 
				(169.0f / 255.0f), 1), new Color((234.0f / 255.0f), 1, 0, 1), guestFright * 5);
            //Debug.Log(skullColor);
        }

        else if (guestFright < 0.4f)
        {
            //critAnim.gameObject.SetActive(false);
            thisRend.enabled = true;
            thisRend.sprite = sprite2;
            skullColor = Color.Lerp(new Color((234.0f / 255.0f), 1, 0, 1), new Color(1, 0, 0, 1), guestFright);
        }

        else if (guestFright < 0.6f)
        {
            //critAnim.gameObject.SetActive(false);
            thisRend.enabled = true;
            thisRend.sprite = sprite3;
            skullColor = Color.Lerp(new Color((234.0f / 255.0f), 1, 0, 1), new Color(1, 0, 0, 1), guestFright);
        }

        else if (guestFright < 0.8f)
        {
           // critAnim.gameObject.SetActive(false);
            thisRend.enabled = true;
            thisRend.sprite = sprite4;
            skullColor = Color.Lerp(new Color((234.0f / 255.0f), 1, 0, 1), new Color(1, 0, 0, 1), guestFright);
        }

        else if (guestFright <= 1.0f && !thisGuest.critState && !thisGuest.freakState)
        {
            //critAnim.gameObject.SetActive(false);
            thisRend.enabled = true;
            thisRend.sprite = sprite5;
            skullColor = Color.Lerp(new Color((234.0f / 255.0f), 1, 0, 1), new Color(1, 0, 0, 1), guestFright);
        }

        else if (thisGuest.critState)
        {
            thisRend.enabled = false;
            critAnim.gameObject.SetActive(true);
            critAnim.SetBool("critState", true);
        }

        if (!thisGuest.critState)
        {
            critAnim.SetBool("critState", false);
        }



        if(freakState2)
        {
            thisRend.enabled = false;
            ripAnim.gameObject.SetActive(true);
            critAnim.gameObject.SetActive(false);
            ripAnim.SetTrigger("Freak");
        }


    }
}