﻿using UnityEngine;
using System.Collections;

public class RangeSysTowerHelper : MonoBehaviour {

    /// <summary>
    /// The attraction that this is attached to.
    /// </summary>
    public AttractionMaster thisTower;

	// Use this for initialization
	void Start () {

        thisTower = GetComponentInParent<AttractionMaster>();
	
	}

    void OnMouseEnter()
    {
        if (thisTower.placed)
        {
            Debug.Log(thisTower.placed);
            if (thisTower.active && !thisTower.auto)
            {
                for (int x = 0; x < thisTower.internalRendList.Length; x++)
                {
                    if (thisTower.internalRendList[x].gameObject.GetComponent<ParticleSystemRenderer>() == null)
                    {
                        thisTower.internalRendList[x].material.shader = Shader.Find("Toon/Lit");
                    }
                }
            }

            thisTower.rangeEffect.Play();
        }
    }


    void OnMouseExit()
    {
        if (thisTower.active && !thisTower.auto)
        {
            for (int x = 0; x < thisTower.internalRendList.Length; x++)
            {
                if (thisTower.internalRendList[x].gameObject.GetComponent<ParticleSystemRenderer>() == null)
                {
                    thisTower.internalRendList[x].material.shader = Shader.Find("Standard");
                }
            }
        }
        thisTower.rangeEffect.Stop();
    }

    // Update is called once per frame
    void Update () {
	
	}
}
