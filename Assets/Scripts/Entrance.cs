﻿using UnityEngine;
using System.Collections;

public class Entrance : MonoBehaviour
{

    public bool ext;



    // Use this for initialization
    void Start()
    {
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "guest" && !ext)
        {
            
                other.GetComponent<EntertainmentMeter>().enabled = true;


        }else if(other.tag == "guest")
        {
            int[] bleh = other.GetComponent<EntertainmentMeter>().MazeExit();
            other.GetComponent<BaseGuest>().ReportScore(bleh[1], bleh[0]);
        }
    }
}
