using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Movement : MonoBehaviour
{

	public bool isMoving;
	private GameObject[] path;
	public int moveDelay = 2;
	public bool onPath;
	public GameObject grid;


	void Start ()
	{
		onPath = false;
		grid = GameObject.FindGameObjectWithTag ("Grid");
		LearnPath ();
	}


	//Update is called once per frame
	void Update ()
	{
	
		if (isMoving) {
			//print (path);
			onPath = true;
			//UpdateAndMove ();
			TimeToActuallyMove ();
			isMoving = false;
			StartCoroutine (MoveDelay ());
		}

		if (IsPathNull ()) {
			ReportAndDelete ();
		}

	
	}

	void LearnPath ()
	{
		path = GameObject.FindGameObjectsWithTag ("PathPiece");
		//path.OrderBy (path => Vector3.Distance (grid.transform.position, path.transform.position));
	}

	//void UpdateAndMove ()
	//{
	//Vector3 nextTile = new Vector3 (0, 0, 0);
	//LinkedList<GameObject> pathCopy = new LinkedList<GameObject> ();
	//pathCopy = path;
	//float thisX = transform.position.x;
	//float thisZ = transform.position.z;
	//foreach (GameObject currentTile in path) {
	//	float tileX = currentTile.transform.position.x;
	//	float tileZ = currentTile.transform.position.z;

	//	if (thisX == tileX && tileZ == thisZ) {
	//		pathCopy.Remove (currentTile);
	//	}
	//
	///		if (Mathf.Abs (thisX - tileX + thisZ - tileZ) < Mathf.Abs (nextTile.x - thisX + nextTile.z - thisZ)) {
	//			nextTile = currentTile.transform.position;
	//		}
	//
	//	}

	//Rigidbody rb = gameObject.GetComponent<Rigidbody> ();
	//path = pathCopy;
	//rb.MovePosition (new Vector3 (nextTile.x, transform.position.y, nextTile.z));
	//print ("Moved" + nextTile.x.ToString ());
	//}

	void TimeToActuallyMove ()
	{
		//print (path.Length);
		GameObject nextTile = null;
		float curDif = 100;
		int index = 0;
		for (int i = 0; i < path.Length; i++) {
			if (path [i] != null) {
				GameObject currentTile = path [i];
				float thisX = transform.position.x;
				float thisZ = transform.position.z;
				float Dif = Mathf.Abs ((thisX - currentTile.transform.position.x) + (thisZ - currentTile.transform.position.z));
				if (Dif < curDif) {
					curDif = Dif;
					index = i;
					nextTile = path [i];
				}
			}
			
			//print (curDif);
		}

		if (nextTile != null) {
			Rigidbody rb = (Rigidbody)gameObject.GetComponent ("Rigidbody");
			path [index] = null;
			float X = nextTile.transform.position.x;
			float Y = transform.position.y;
			float Z = nextTile.transform.position.z;
			//print (rb);
			rb.MovePosition (new Vector3 (X, Y, Z));
		}
	}


	void ReportAndDelete ()
	{
		//This will report the guest's stats to the overall manager and destroy the guest after it has exited the maze
		//Something something reporting
		Destroy (gameObject);
	}

	bool IsPathNull ()
	{
		for (int x = 0; x < path.Length; x++) {
			if (path [x] != null) {
				return false;
			}
		}

		return true;
	}

	IEnumerator MoveDelay ()
	{
		yield return new WaitForSeconds (moveDelay);
		isMoving = true;

	}


}
