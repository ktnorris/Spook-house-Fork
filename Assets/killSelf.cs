﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killSelf : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(DIE());
	}
	
	IEnumerator DIE()
    {
        yield return new WaitForSeconds(3);
        Destroy(this.gameObject);
    }
}
