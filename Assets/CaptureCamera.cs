﻿using UnityEngine;
using System.Collections;

public class CaptureCamera : MonoBehaviour {

    public GameObject GUI;
    public float speed = 10.0F;
    public float rotationSpeed = 100.0F;
    // Update is called once per frame
    void Update() {
        float translation = Input.GetAxis("Vertical") * speed;
        float transX = Input.GetAxis("Horizontal") * speed;
        float rotation = rotationSpeed;

        translation *= Time.deltaTime;
        transX *= Time.deltaTime;
        rotation *= Time.deltaTime;

        ///WASD = directional movment
        if (Input.GetKey(KeyCode.W))
        {

            transform.Translate(0, 0, speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0, 0, speed * Time.deltaTime * -1);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(speed * Time.deltaTime, 0, 0);
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(speed * Time.deltaTime * -1, 0, 0);
        }

        //Space increases height
        if (Input.GetKey(KeyCode.Space))
        {
            if(Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)){
                transform.Translate(0, speed * Time.deltaTime * -1, 0);
            }
            else
            {
                transform.Translate(0, speed * Time.deltaTime, 0);
            }
            
        }
        
        //Arrows control rotation on the y and x axis q and e control z angles
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(0, -rotation, 0);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Rotate(-rotation, 0, 0);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Rotate( rotation, 0, 0);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(0,  rotation, 0);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            transform.Rotate(0, 0, rotation);
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.Rotate(0, 0, -rotation);
        }


        //Control and shift change speed
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            //rotationSpeed *= 2;
            speed *= 2;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            speed /=  2;
        }

        //Z and x change rotation speed
        if (Input.GetKeyDown(KeyCode.Z))
        {
            rotationSpeed *= 2;
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            rotationSpeed /= 2;
        }

        //disables gui camera
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GUI.GetComponent<Camera>().enabled = !GUI.GetComponent<Camera>().enabled;
        }

    }
}
